<?php

return [
	'type' => [
		1 => 'Banner ảnh',
		2 => 'Banner video',
 	],
 	'targetType' => [
 		'news' => 'Banner cho tin tức',
 		'combo' => 'Banner cho combo/tour',
 		'flight' => 'Banner cho vé máy bay',
 		'hotel' => 'Banner cho khách sạn',
 	], 	 
	'website' => [
		'travellink' => "Web Travellink",
		'app_travellink' => "App Travellink",
		'vovtv'      => "Web vovtv Travel",
		'tpbank'     => "Web tpbank",
	],
	"device" => [
		"pc" => "PC",
		"mob" => "Mobile",
	] 
]
?>
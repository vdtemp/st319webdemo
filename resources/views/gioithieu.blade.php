@extends('main')

@section('css-header')
  <link href="{{ asset('assets/css/gioithieu.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <section class="banner">
            <div class="slide_banner">
                @foreach ($banners as $ban)
                    <div class="item">
                        <a href="{{  $ban['ban_target_url'] ?? '' }}"><img class="w-100" src="{{ $ban['ban_picture'] }}" alt="" /></a>
                    </div>
                @endforeach                
            </div>
        </section>
        <section class="section page_intro">
            <div class="container">
                <div class="list_tab">
                    <ul class="nav nav-tabs justify-content-lg-end" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link text-uppercase active" data-toggle="tab" href="#tab_intro" role="tab" aria-selected="false">Introduction</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" data-toggle="tab" href="#tab_history" role="tab" aria-selected="true">History</a>
                        </li>
                    </ul>
                </div>
                <div class="content_tab">
                    <div class="tab-content">
                        <!-- tab intro -->
                        <div id="tab_intro" class="tab-pane fade active show">
                            <div class="tit_cate">
                                <h2 class="title">introduction</h2>
                            </div>
                            <div class="row">
                                <div class="col-lg-10">
                                    <ul class="list_info">
                                        <li class="item_info">
                                            <span>Name</span>
                                            <span>{{ $webInfo['con_company'] }}</span>
                                        </li>
                                        <li class="item_info">
                                            <span>C.E.O</span>
                                            <span>{{ $webInfo['con_ceo_name'] }}</span>
                                        </li>
                                        <li class="item_info">
                                            <span>Business Field</span>
                                            <span>{{ $webInfo['con_business_field'] }}</span>
                                        </li>
                                        <li class="item_info">
                                            <span>Address</span>
                                            <span>{!! $webInfo['con_address'] !!}</span>
                                        </li>
                                        <li class="item_info">
                                            <span>Date of establishment</span>
                                            <span>{{ $webInfo['con_date_establishment'] }}</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-2"> 
                                    <div class="d-lg-flex align-items-end justify-content-between"> 
                                        <div class="slogan text-right">Never<br>give up</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-8"> 
                                <div class="title bottom_number_title" style="">ST.319 Entertainment in Numbers</div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">                                    
                                    <div class="d-lg-flex align-items-end justify-content-between">
                                        <div class="total_number">
                                            <div class="item text-right">
                                                <div class="number bottom_total_number">1,749,915+</div>
                                                <p class="desc">Total number of Youtube subscribers</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                <div class="d-lg-flex align-items-end justify-content-between">       
                                    <div class="total_number">
                                        <div class="item text-right">
                                            <div class="number bottom_total_number">899,114,601+</div>
                                            <p class="desc">Total number of Youtube video views</p>
                                        </div>    
                                    </div>                                    
                                </div>    
                                </div>
                            </div>
                            
                        </div>
                        <!-- tab history -->
                        <div id="tab_history" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="tit_cate">
                                        <h2 class="title">History</h2>
                                        <span>ST.319</span>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="list_item">
                                        <div class="tit_cate" style="border-bottom: none;"><p style="font-size: 24px;" class="title">2022</p></div>
                                        <div class="row">
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_1.png"></div>
                                                    <h3 class="title">tìm (the 1st digital single)</h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_2.png"></div>
                                                    <h3 class="title">stuck (the 2nd digital single)</h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_3.png"></div>
                                                    <h3 class="title">get out</h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_4.png"></div>
                                                    <h3 class="title">Luôn bên anh</h3>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="tit_cate" style="border-bottom: none;"><p style="font-size: 24px;" class="title">2021</p></div>
                                        <div class="row">    
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_5.png"></div>
                                                    <h3 class="title">shine your light </h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_6.png"></div>
                                                    <h3 class="title">sau tất cả (single)</h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_7.png"></div>
                                                    <h3 class="title">lạc nhau có phải muôn đời</h3>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="tit_cate" style="border-bottom: none;"><p style="font-size: 24px;" class="title">2020</p></div>
                                        <div class="row">    
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_8.png"></div>
                                                    <h3 class="title">#babybaby (single)</h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_9.png"></div>
                                                    <h3 class="title">tình yêu chậm trễ</h3>
                                                </a>
                                            </div>
                                            <div class="col-lg-220 col-md-4 col-6 item">
                                                <a href="">
                                                    <div class="img_item"><img class="w-100" src="/assets/img/pro_10.png"></div>
                                                    <h3 class="title">từ thích thích thành thương thương</h3>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
	<script type="text/javascript">
	  $(document).ready(function() {
	      $('.slide_banner').slick({
	          autoplay: false,
	          autoplaySpeed: 3000,
	          dots: true,
	          arrows: false,
	          infinite: true,
	          speed: 300,
	      });
	  })
	</script>
@endsection
@extends('main')

@section('css')
	<link rel="stylesheet" href="{{asset('assets/css/404.css')}}?v={{config('app.vcss')}}"/>
	<style type="text/css">
		.menu{
			display: none;
		}
	</style>
@endsection

@section('content')
<div class="p404">
	<h2>Trang không tồn tại</h2>
	<p>Hoặc bạn không có quyền truy cập vào trang này</p>
	<a href="#" onclick="previousPage()">Quay lại</a>
</div>
@endsection
<header id="header">
    <div class="menu_mb d-block d-lg-none position-fixed w-100 clearfix">
        <div class="top_mb">
            <div class="logo_mb text-center position-fixed">
                <a href="http://st319.vn/"><img class="img_logo_mb" src="/assets/img/logo.png" alt="" /></a>
            </div>
            <div class="right">
                <div class="bt_menu">
                    <button class="nav-toggle">
                        <div class="icon-menu">
                            <span class="line line-1"></span>
                            <span class="line line-2"></span>
                            <span class="line line-3"></span>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix clearfix-90 d-block d-lg-none"></div>
    <section class="header_top sticky-header w-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-3  d-none d-lg-block">
                    <div class="logo_pc">
                        <a href="http://st319.vn/">
                            <img class="mw-100" src="/assets/img/logo_st319_new.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-9">
                    <div class="right_top float-right d-none d-lg-block">
                        <span>Language</span>
                        <select class="language">
                            @foreach ($arrLang as $lKey => $lName)
                            <option value="{{ $lKey }}">{{$lName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="menu_main">
                        <nav class="nav is-fixed" role="navigation">
                            <div class="wrapper wrapper-flush w-100">
                                <div class="nav-container">
                                    <ul class="nav-menu menu clearfix list-inline">
                                        <li class="menu-item">
                                            <a href="{{ route('intro') }}" class="menu-link">About</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="{{ route('news') }}" class="menu-link">News</a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="{{ route('campaign') }}" class="menu-link">Articles</a>
                                        </li>
                                        <li class="menu-item has-dropdown"><a href="{{ route('artist') }}" class="menu-link ">Artist</a>
                                            <ul class="nav-dropdown list-inline menu">
                                                @foreach ($arrArtist as $keyArtist => $Artist)
                                                <li class="menu-item"><a href="{{ route('artistDetail', ['id' => $keyArtist, 'alias' => removeTitle($Artist)]) }}" class="menu-link">{{ $Artist }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li class="menu-item"><a href="{{ route('event') }}" class="menu-link">Events</a></li>
                                        <li class="menu-item"><a href="{{ route('album') }}" class="menu-link">Music</a></li>
                                        <li class="menu-item"><a href="{{ route('store') }}" class="menu-link">Shop</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix clearfix-header"></div>
</header>
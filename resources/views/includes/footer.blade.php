<footer class="bg_gradient">
    <div class="container">
        <div class="d-lg-flex align-items-center justify-content-between pd_50">
            <div class="ft_item d-sm-flex align-items-end">
                <div class="logo_bot">
                    <a href="/"><img src="/assets/img/logo_ft.png"></a>
                </div>
                <div class="text-uppercase copy_right">copyright st.319 entertainment <br> all right reserved</div>
            </div>
            <div class="ft_item">
                <div class="d-flex justify-content-between list_item mb-4">
                    <a class="text-uppercase item" href="{{ route('intro') }}">About us</a>
                    <a class="text-uppercase item" href="{{ route('intro') }}">Contact</a>
                    <div class="link_social d-flex">
                        <a href="{{$webInfo['con_facebook']}}"><img src="/assets/img/ic_fb.png"></a>
                        <a href="{{$webInfo['con_twitter']}}"><img src="/assets/img/ic_ins.png"></a>
                        <a href="{{$webInfo['con_youtube']}}"><img src="/assets/img/ic_yt.png"></a>
                    </div>
                </div>
                <div class="address">{!!$webInfo['con_address']!!}</div>
            </div>
        </div>
    </div>
    <a href="#top" id="go_top"><i class="fa fa-angle-up"></i></a>
</footer>
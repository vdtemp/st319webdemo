@extends('main')

@section('css-header')
    <title>News - {{ $News['new_title'] }}</title>
    <link href="{{ asset('assets/css/tintuc.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_news">
            <div class="container">
                <div class="page_content">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="mr-150 wrap_title d-flex justify-content-between align-items-baseline">
                                <div class="tit_cate">
                                    <h2 class="title">news</h2>
                                </div>
                                <div class="pagination">
                                    <ul class="">
                                        <li><a href="{{ URL::previous() }}"><i class="fa fa-arrow-left mr-2"></i>BACK</a></li>
                                        @if ($prevNews)
                                            <li><a href="{{ route('newsDetail', ['id' => $prevNews['new_id'], 'alias' => removeTitle($prevNews['new_title']) ]) }}"><i class="fa fa-angle-left mr-2"></i>PREV</a></li>
                                        @endif
                                        @if ($nextNews)
                                            <li><a href="{{ route('newsDetail', ['id' => $nextNews['new_id'], 'alias' => removeTitle($nextNews['new_title']) ]) }}">NEXT<i class="fa fa-angle-right ml-2"></i></a></li>
                                        @endif
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="mr-150">
                                <article class="content">
                                    <h4 class="title">{{ $News['new_title'] }}</h4>
                                    <div class="d-lg-flex justify-content-between align-items-baseline mt-2 mb-4">
                                        <div class="date_post">{{ date('h:i d/m/Y', $News['new_date']) }}</div>
                                        <div class="interact">
                                            <ul class="d-flex">
                                                {{-- <li>
                                                    <a href=""><i class="fa fa-thumbs-up"></i></a>
                                                </li> --}}
                                                <li>
                                                    <div  class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                                </li>
                                                <li>
                                                    <a href=""><i class="fa fa-bookmark"></i> Lưu vào Facebook</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="desc"><i>{{ $News['new_teaser'] }}</i></div>
                                    @if ($News['new_link_video'])
                                        <div class="content_detail" style="margin: 20px;">
                                            {!! $News['new_link_video'] !!}
                                        </div>
                                    @endif
                                    <div class="content_detail" >                                        
                                        {!! $News['new_description'] !!}
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="sidebar_right">
                                <div class="tit_cate">
                                    <h2 class="title">you may like</h2>
                                </div>
                                <div class="list_item">
                                    @foreach ($otherNews as $element)
                                        <div class="item">
                                            <div class="img_item">
                                                <a href="{{ route('newsDetail', ['id' => $element['new_id'], 'alias' => removeTitle($element['new_title']) ]) }}">
                                                    <img class="w-100" src="{{$element['new_picture']}}">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <a class="title" href="{{ route('newsDetail', ['id' => $element['new_id'], 'alias' => removeTitle($element['new_title']) ]) }}">{{ $element['new_title'] }}</a>
                                                <div class="date_post">{{ customshowDate($element['new_date']) }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
           $('.slide_banner').slick({
                autoplay: false,
                autoplaySpeed: 3000,
                dots: true,
                arrows: false,
                infinite: true,
                speed: 300,
            });
        })
    </script>
@endsection
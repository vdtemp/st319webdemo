@extends('main')

@section('css-header')
    <title>Store - </title>
    <link href="{{ asset('assets/css/store.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_store">
            <div class="container">
                <div class="tit_cate">
                    <h2 class="title">Store.319</h2>
                </div>
                <div class="progress_bar">
                    <div class="text-center">
                        <img src="/assets/img/loading.png" alt="">
                    </div>
                    <div class="text success">
                        <span>Đặt hàng thành công</span>
                    </div>
                    <div class="back_link">
                        <a class="btn btn_back mr-lg-3" href="{{ route('store') }}"><i class="fa fa-angle-left mr-2"></i>Xem tiếp sản phẩm</a>
                        <a class="btn btn_home ml-lg-3" href="{{ route('home') }}"><i class="fa fa-angle-double-left mr-2"></i>Trở lại trang chủ</a>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        
    </script>
@endsection
@extends('main')

@section('css-header')
    <title>Artist Info - {{$artistInfo['art_name']}}</title>
    <link href="{{ asset('assets/css/nghesi.css', env('APP_HTTPS')) }}" rel="stylesheet" />    
    <link href="{{ asset('assets/css/fancybox.css', env('APP_HTTPS')) }}" rel="stylesheet" />
    <script type="text/javascript" src="{{ asset('assets/js/fancybox.js', env('APP_HTTPS')) }}"></script>
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_artist page_detail">
            <div class="container">
                <div class="list_tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_latest" role="tab" aria-selected="false">latest</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_about" role="tab" aria-selected="true">about</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_garelly" role="tab" aria-selected="true">Gallery</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- tab latest -->
                    <div id="tab_latest" class="tab-pane fade active show">
                        <div class="tit_cate d-flex justify-content-between align-items-center">
                            <span><h3 class="title" style="text-transform: uppercase;">{{ $artistInfo['art_name'] }}</h3></span>
                            <span><h3 class="title">artist</h3></span>
                        </div>
                        <div class="wrap_content" style="background: url({{ $artistInfo['art_background'] }}) no-repeat; background-size: cover; margin-top: 40px; padding: 0 10px;">
                            <div class="content">
                                <div class="lik_item">
                                    {!! $artistInfo['art_link_video'] !!}
                                </div>  
                                {!! $artistInfo['art_description'] !!}
                                <div class="listen_on">
                                    <div class="img_item mb-4">
                                        <img src="{{ $artistInfo['art_avatar'] }}" alt="">
                                    </div>
                                    <select name="" id="selectListen">
                                        <option value="">{{ $artistInfo['art_job'] == 1 ? 'listen on' : 'watch now' }}</option>
                                        <option value="{{ $artistInfo['art_itunes_url'] }}">iTunes</option>
                                        <option value="{{ $artistInfo['art_spotify_url'] }}">{{ $artistInfo['art_job'] == 1 ? 'Spotify' : 'Netflix ' }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- tab about -->
                    <div id="tab_about" class="tab-pane fade">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="img_highlight">
                                    <img class="w-100" src="{{ $artistInfo['art_avatar'] }}" alt="">
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="tit_cate d-flex justify-content-between align-items-center">
                                    <span><h3 class="title" style="text-transform: uppercase;">{{ $artistInfo['art_name'] }}</h3></span>
                                    <span><h3 class="title">artist</h3></span>
                                </div>
                                <div class="mb-4 artist_social">
                                    <ul class="sns ">                                
                                        <li><a href="{{ $artistInfo['art_facebook_link'] }}" class="facebook" target="_blank">facebook</a></li>
                                        <li><a href="{{ $artistInfo['art_instagram_link'] }}" class="instagram" target="_blank">instagram</a></li>
                                        <li><a href="{{ $artistInfo['art_youtube_link'] }}" class="youtube" target="_blank">youtube</a></li> 
                                        <li><a href="{{ $artistInfo['art_twitter_link'] }}" class="twitter" target="_blank">twitter</a></li>
                                        <li><a href="{{ $artistInfo['art_tiktok_link'] }}" class="tiktok" target="_blank">tiktok</a></li>
                                    </ul>
                                </div>
                                <div class="info_artist">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="content">
                                                {!! $artistInfo['art_description'] !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     @foreach ($arrInfoProduct as $key => $grpProduct)
                        <div class="album item_cate">
                            <div class="title"><span>{{ $arrType[$key] ?? 'Video' }}</span></div>
                            <div class="list_item row">
                                @foreach ($grpProduct as $pro)
                                    <div class="item col-lg-220 col-md-3 col-sm-6">
                                        <a href="">
                                            <div class="img_item"><img class="w-100" src="{{ $pro['pro_image'] }}" alt=""></div>
                                            <div class="title" style="text-transform: uppercase;">{{ $pro['pro_name'] }}</div>
                                            <div class="release_year">{{ $pro['pro_date'] }}</div>
                                        </a>
                                    </div>    
                                @endforeach                                
                            </div>
                        </div>
                     @endforeach
                        <div class="album item_cate">
                            <div class="title"><span>{{ $artistInfo['art_job'] == 1 ? 'ACTIVITIES' : 'PHIM ĐÃ THAM GIA' }}</span></div>
                            <div class="list_item">
                                <div class="awards content" style="width: 700px;display: block;margin: 0 auto;">
                                    {!! $artistInfo['art_awards'] !!}
                                </div>                      
                            </div>
                        </div>
                    </div>
                    <!-- tab garelly -->
                    <div id="tab_garelly" class="tab-pane fade">
                        <div class="tit_cate d-flex justify-content-between align-items-center">
                            <span><h3 class="title" style="text-transform: uppercase;">{{ $artistInfo['art_name'] }}</h3></span>
                            <span><h3 class="title">artist</h3></span>
                        </div>
                        <div class="row list_item">
                            <div class="col-lg-6 item">
                            @foreach ($arrImages as $image)
                                @if ($loop->iteration == 2 )                                    
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-6 item"> 
                                @elseif($loop->iteration > 2 && $loop->iteration <= 5) 
                                        </div>
                                    <div class="col-6 item">           
                                @elseif ($loop->iteration == 6)
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-lg-3 col-6 item">
                                @elseif ($loop->iteration > 6)    
                                    </div>
                                    <div class="col-lg-3 col-6 item">
                                @endif
                                <a data-fancybox="garelly" href="{{ $image['arg_image'] }}" class="img_item">
                                    <img class="w-100" src="{{ $image['arg_image'] }}" alt="">
                                </a>
                            @endforeach
                            </div>
                        </div>
                        <div class="pagination justify-content-center mt-5">
                            <ul>
                                {{-- <li><a href="">1</a></li>
                                <li><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href="">4</a></li>
                                <li><a href="">...</a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tab_garelly .item .item_img').fancybox();
            $('.slider_music_videos').slick({
                autoplay: true,
                dots: false,
                arrows: true,
                prevArrow: '<button type="button " data-role="none " class="slick-prev slick-arrow " aria-label="Previous " role="button " style="display: block; "></button>',
                nextArrow: '<button type="button " data-role="none " class="slick-next slick-arrow " aria-label="Next " role="button " style="display: block; "></button>',
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                margin: 20,
                responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });

            $('#selectListen').on('change', function(event) {
                event.preventDefault();
                /* Act on the event */
                let url = $(this).val();
                console.log(url);
                if(url != ''){
                    window.open(url);
                }
            });
        });
    </script>
@endsection
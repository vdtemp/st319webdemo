@extends('main')

@section('css-header')
    <title>Event</title>
    <link href="{{ asset('assets/css/sukien.css', env('APP_HTTPS')) }}" rel="stylesheet" />      
@endsection

@section('content')
    <main>
        <section class="section page_events">
            <div class="bg_gradient">
                <div class="container">
                    <div class="wrap_title d-flex justify-content-between align-items-baseline">
                        <div class="tit_cate p-0">
                            <h2 class="title">events</h2>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{-- <li><a href="">1</a></li>
                                <li><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><a href="">4</a></li>
                                <li><a href="">...</a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                @foreach ($banners as $ban) 
                    <img class="w-100" src="{{ $ban['ban_picture'] }}" alt="" /> 
                @endforeach                 
            </div>
            <div class="content_events">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            {!! $hotEvent['eve_video_url'] !!}
                            <a href="{{ $hotEvent['eve_form_apply'] }}" class="btn btn_download">Form donwload <i class="fa fa-download"></i></a>
                        </div>
                        <div class="col-lg-6 info_event">
                            <h4 class="title mb-4">{{ $hotEvent['eve_title'] }}</h4>
                            <p class="mb-4">{{ $hotEvent['eve_intro'] }}</p>
                            <div class="mb-4">
                                {!! $hotEvent['eve_description'] !!}
                            </div>
                            {{-- <div class="mb-4">
                                <div class="title">Thời gian</div>
                                <p>TP.HCM: 9h00 24/01/2021</p>
                                <p>HANOI: 9H00 31/01/2021</p>
                            </div>
                            <div class="mb-4">
                                <div class="title">Địa điểm</div>
                                <p>TP.HCM: Nhà văn hóa sinh viên - 643 Điện Biên Phủ, phường 1 quận 3</p>
                                <p>HÀ NỘI: Rạp Đại Nam - 89 Phố Huế, Hai Bà Trưng</p>
                            </div> --}}
                            <div class="tag_events">
                                <?
                                $hotEventTags = explode("|", $hotEvent['eve_tags']);
                                foreach ($hotEventTags as $key => $tag) {
                                    echo "<span>#". $tag ."</span>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                @foreach ($listEvent as $event)
                    <div class="item_events">
                        <div class="info">
                            <div class="title text-uppercase">{{$event['eve_title']}}</div>
                            <div class="year mt-4">2019</div>
                            <div class="detail mt-4">
                                <p class="mb-4">{{$event['eve_intro']}}</p>
                                <div class="mb-4">
                                     {!! $event['eve_description'] !!}
                                </div>
                                <div class="tag_events">
                                    <?
                                    $EventTags = explode("|", $event['eve_tags']);
                                    foreach ($EventTags as $key => $tag) {
                                        echo "<span>#". $tag ."</span>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="img_item">
                            <img class="w-100" src="{{$event['eve_banner']}}" alt="">
                        </div>
                    </div> 
                 @endforeach
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.item_events').on('click', function(e) {
                e.preventDefault();
                $(this).toggleClass('is-active');
            });
        });
    </script>
@endsection
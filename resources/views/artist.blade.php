@extends('main')

@section('css-header')
    <title>Artists - </title>
    <link href="{{ asset('assets/css/nghesi.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_artist">
            <div class="container">
                <div class="tit_cate">
                    <span>
                        <h2 class="title">artist</h2>
                    </span>
                </div>
                <div class="page_content">
                    <div class="list_item">
                        @foreach ($listArtist as $artist)
                            <div class="item text-center">
                                <a href="{{ route('artistDetail', ['id' => $artist['art_id'], 'alias' => removeTitle($artist['art_name'])]) }}" class="btn_img">
                                    @if ($loop->iteration % 2 == 0)
                                    <h3 class="name">{{$artist['art_name']}}</h3>
                                    <div class="img_item"><img class="w-100" src="{{$artist['art_small_pic']}}"></div>
                                    @else
                                    <div class="img_item"><img class="w-100" src="{{$artist['art_small_pic']}}"></div>
                                    <h3 class="name">{{$artist['art_name']}}</h3>    
                                    @endif                                    
                                </a>
                            </div>
                        @endforeach                        
                    </div>
                    <div class="list_images">
                        @foreach ($listArtist as $artist)
                            <div data-id="{{$loop->iteration}}" class="img_artist">
                                <img src="{{$artist['art_avatar']}}">
                            </div>
                        @endforeach   
                    </div>
                    <div class="opacity_main"></div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            btn_imgs = $(".btn_img").find('.img_item');
            img_artists = $(".img_artist");
            btn_imgs.each((index, btn_img) => {
                btn_img.addEventListener("click", function() {
                    img_artists.each((k, img_artist) => {
                        if (img_artist.getAttribute('data-id') == (index + 1)) {
                            img_artist.classList.add("show");
                            btn_img.classList.add("is-active");
                            $(".opacity_main").addClass('open_opacity');
                        }
                    })
                })
            });
            $(".opacity_main").on('click', function() {
                img_artists.removeClass('show');
                $(".opacity_main").removeClass('open_opacity');
                $(".btn_img").removeClass('is-active');
            })
        })
    </script>
@endsection
@extends('main')

@section('css-header')
    <title>Campaign - {{ $Campaign['camp_title'] }}</title>
    <link href="{{ asset('assets/css/tintuc.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
        <main>
        <div class="bg_animation"></div>
        <section class="section page_news">
            <div class="container">
                <div class="page_content">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="mr-150 wrap_title d-flex justify-content-between align-items-baseline">
                                <div class="tit_cate">
                                    <h2 class="title">Article</h2>
                                </div>
                                <div class="pagination">
                                    <ul class="">
                                        <li><a href="{{ URL::previous() }}"><i class="fa fa-arrow-left mr-2"></i>BACK</a></li>
                                        @if ($prevCampaign)
                                            <li><a href="{{ route('campaignDetail', ['id' => $prevCampaign['camp_id'], 'alias' => removeTitle($prevCampaign['camp_title']) ]) }}"><i class="fa fa-angle-left mr-2"></i>PREV</a></li>
                                        @endif
                                        @if ($nextCampaign)
                                            <li><a href="{{ route('campaignDetail', ['id' => $nextCampaign['camp_id'], 'alias' => removeTitle($nextCampaign['camp_title']) ]) }}">NEXT<i class="fa fa-angle-right ml-2"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="mr-150">
                                <article class="content">
                                    <h4 class="title">{{ $Campaign['camp_title'] }}</h4>
                                    <div class="d-lg-flex justify-content-between align-items-baseline mt-2 mb-4">
                                        <div class="date_post">{{ date('h:i d/m/Y', $Campaign['camp_created_date']) }}</div>
                                        <div class="interact">
                                            <ul class="d-flex">
                                                {{-- <li>
                                                    <a href=""><i class="fa fa-thumbs-up"></i></a>
                                                </li> --}}
                                                <li>
                                                    <div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                                </li>
                                                <li>
                                                    <a href=""><i class="fa fa-bookmark"></i> Lưu vào Facebook</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="content_detail mb-3 mt-3 text-center">
                                        <img src="{{ $Campaign['camp_picture'] }}" alt="" class="">
                                    </div>
                                    <div class="desc"><i>{{ $Campaign['camp_intro'] }}</i></div>
                                    @if ($Campaign['camp_link_video'])
                                        <div class="content_detail" style="margin: 20px;">
                                            {!! $Campaign['camp_link_video'] !!}
                                        </div>
                                    @endif
                                    <div class="content_detail" >
                                        {!! $Campaign['camp_description'] !!}
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="sidebar_right">
                                <div class="tit_cate">
                                    <h2 class="title">you may like</h2>
                                </div>
                                <div class="list_item">
                                    @foreach ($otherCampaign as $element)
                                        <div class="item">
                                            <div class="img_item">
                                                <a href="{{ route('campaignDetail', ['id' => $element['camp_id'], 'alias' => removeTitle($element['camp_title']) ]) }}">
                                                    <img class="w-100" src="{{$element['camp_picture']}}">
                                                </a>
                                            </div>
                                            <div class="info">
                                                <a class="title" href="{{ route('campaignDetail', ['id' => $element['camp_id'], 'alias' => removeTitle($element['camp_title']) ]) }}">{{ $element['camp_title'] }}</a>
                                                <div class="date_post">{{ customshowDate($element['camp_created_date']) }}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main> 
@endsection

@section('js-footer')
    <script type="text/javascript">
      $(document).ready(function() {
           
      })
    </script>
@endsection
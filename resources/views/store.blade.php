@extends('main')

@section('css-header')
    <title>Store - </title>
    <link href="{{ asset('assets/css/store.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <section class="section cate_store">
            <div class="bg_gradient">
                <div class="container">
                    <div class="wrap_title d-flex justify-content-between align-items-baseline">
                        <div class="tit_cate p-0">
                            <h2 class="title">store.st319</h2>
                        </div>
                         
                    </div>
                </div>
            </div>
            <div class="banner_cate">
                @foreach ($banners as $banner)
                    <img class="w-100" src="{{ $banner['ban_picture'] }}" alt="">    
                    @break
                @endforeach               
            </div>
            @foreach ($listproduct as $key => $list)
                <!-- item cate -->
                <div class="item_cate">
                    <div class="container">
                        <h3 class="tit_item">{{ $listArtist[$key] ?? '' }}</h3>
                        <div class="year">2021</div>
                        <div class="list_item list_product_cate">
                            @foreach ($list as $pro)
                                <div class="item">
                                    <a href="{{ route('storeProduct', ['id' => $pro['pro_id']]) }}">
                                        <div class="img_item">
                                            <img class="w-100" src="{{$pro['pro_image']}}" alt="">
                                        </div>
                                        <h4 class="name text-uppercase">{{$pro['pro_name']}}</h4>
                                        <div class="price">{{ number_format($pro['pro_price']) }}đ</div>
                                    </a>
                                </div>
                            @endforeach                            
                        </div>
                    </div>
                </div>    
            @endforeach
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.list_product_cate').slick({
                autoplay: true,
                dots: false,
                arrows: true,
                prevArrow: '<button type="button " data-role="none " class="slick-prev slick-arrow " aria-label="Previous " role="button " style="display: block; "></button>',
                nextArrow: '<button type="button " data-role="none " class="slick-next slick-arrow " aria-label="Next " role="button " style="display: block; "></button>',
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                margin: 30,
                responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
        })
    </script>
@endsection
@extends('main')

@section('css-header')
    <title>Campaign</title>
    <link href="{{ asset('assets/css/tintuc.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_news">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="wrap_title mr-150 d-flex justify-content-between align-items-baseline">
                            <div class="tit_cate">
                                <h2 class="title">Article</h2>
                            </div>
                            <div class="pagination">
                                @if ($pagination['totalPage'] > 1)
                                    <ul>
                                        <?
                                        for ($i = max($pagination['page'] - 1, 1); $i <= min($pagination['page'] +2, $pagination['totalPage']); $i++) { 
                                            $url = route('campaign', ['page'=>$i]);
                                            echo ' <li><a href="'. $url .'">'.$i.'</a></li>';
                                        }
                                        ?>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page_content">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="mr-150">
                                <div class="row list_hot">
                                    @foreach ($listCampaign as $campaign)
                                        @if ($loop->iteration <= 2)
                                            <div class="col-lg-6">
                                                <div class="news_hot">
                                                    <div class="img_item">
                                                        <a href="{{ route('campaignDetail', ['id' => $campaign['camp_id'], 'alias' => removeTitle($campaign['camp_title'])]) }}"><img style="max-height: 600px" class="w-100" src="{{ $campaign['camp_picture'] }}"></a>
                                                    </div>
                                                    <div class="info">
                                                        <a href="{{ route('campaignDetail', ['id' => $campaign['camp_id'], 'alias' => removeTitle($campaign['camp_title'])]) }}" class="title">{{ $campaign['camp_title'] }}</a>
                                                        <div class="date_post">{{ customshowDate($campaign['camp_created_date']) }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($loop->iteration == 2)
                                                </div>
                                                <div class="list_item">
                                            @endif
                                        @else
                                            <div class="item row">
                                                <div class="col-lg-3">
                                                    <div class="img_item">
                                                        <a href="{{ route('campaignDetail', ['id' => $campaign['camp_id'], 'alias' => removeTitle($campaign['camp_title'])]) }}"><img class="w-100" src="{{ $campaign['camp_picture'] }}"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-9">
                                                    <div class="info">
                                                        <a class="title" href="{{ route('campaignDetail', ['id' => $campaign['camp_id'], 'alias' => removeTitle($campaign['camp_title'])]) }}">{{ $campaign['camp_title'] }}</a>
                                                        <div class="date_post">{{ customshowDate($campaign['camp_created_date']) }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif                                        
                                    @endforeach 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 d-none d-lg-block">
                            <div class="sidebar_right">
                                <div class="fanpage_fb item item_social">
                                    <div class="title bg_gradient">FACEBOOK</div>
                                    <div class="link_item">
                                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fst319entertainment&tabs=timeline&width=370&height=185&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="185"
                                            style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                                    </div>
                                </div>
                                <div class="yt_channel item item_social">
                                    <div class="title bg_gradient">YOUTUBE</div>
                                    <div class="lik_item">
                                        <iframe width="100%" height="185" src="https://www.youtube.com/embed/vpRi8S6uXAg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="tiktok item item_social">
                                    <div class="title bg_gradient">TIKTOK</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>    
@endsection

@section('js-footer')
    <script type="text/javascript">
      $(document).ready(function() {
           
      })
    </script>
@endsection
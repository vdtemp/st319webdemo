@extends('main')

@section('css-header')
    <title>News</title>
    <link href="{{ asset('assets/css/tintuc.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_news">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="wrap_title mr-150 d-flex justify-content-between align-items-baseline">
                            <div class="tit_cate">
                                <h2 class="title">news</h2>
                            </div>
                            <div class="pagination">
                                @if ($pagination['totalPage'] > 1)
                                    <ul>
                                        <?
                                        for ($i = max($pagination['page'] - 1, 1); $i <= min($pagination['page'] +2, $pagination['totalPage']) ; $i++) { 
                                            $url = route('news', ['page'=>$i]);
                                            echo ' <li><a href="'. $url .'">'.$i.'</a></li>';
                                        }
                                        ?>
                                    </ul>
                                @endif                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page_content">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="mr-150">
                                <div class="news_hot d-flex align-items-end">
                                    <div class="img_item">
                                        <a href="{{ route('newsDetail', ['id' => $hotNews['new_id'], 'alias' => removeTitle($hotNews['new_title'])]) }}">
                                            <img alt class="w-100" src="{{ $hotNews['new_picture'] }}">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="date_post">{{ customshowDate($hotNews['new_date']) }}</div>
                                        <a href="{{ route('newsDetail', ['id' => $hotNews['new_id'], 'alias' => removeTitle($hotNews['new_title'])]) }}" class="title">{{ $hotNews['new_title'] }}</a>
                                    </div>
                                </div>
                                <div class="list_item">
                                    @foreach ($listNews as $news)
                                        <div class="item row">
                                            <div class="col-lg-3">
                                                <div class="img_item">
                                                    <a href="{{ route('newsDetail', ['id' => $news['new_id'], 'alias' => removeTitle($news['new_title'])]) }}">
                                                        <img class="w-100" src="{{ $news['new_picture'] }}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-9">
                                                <div class="info">
                                                    <a class="title" href="{{ route('newsDetail', ['id' => $news['new_id'], 'alias' => removeTitle($news['new_title'])]) }}">{{ $news['new_title'] }}</a>
                                                    <div class="date_post">{{ customshowDate($news['new_date']) }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach                                     
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 d-none d-lg-block">
                            <div class="sidebar_right">
                                <div class="fanpage_fb item item_social">
                                    <div class="title bg_gradient">FACEBOOK</div>
                                    <div class="link_item">
                                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fst319entertainment&tabs=timeline&width=370&height=185&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="185"
                                            style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                                    </div>
                                </div>
                                <div class="yt_channel item item_social">
                                    <div class="title bg_gradient">YOUTUBE</div>
                                    <div class="lik_item">
                                        <iframe width="100%" height="185" src="https://www.youtube.com/embed/vpRi8S6uXAg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="tiktok item item_social">
                                    <div class="title bg_gradient">TIKTOK</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
           $('.slide_banner').slick({
                autoplay: false,
                autoplaySpeed: 3000,
                dots: true,
                arrows: false,
                infinite: true,
                speed: 300,
            });
        })
    </script>
@endsection
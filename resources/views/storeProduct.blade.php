@extends('main')

@section('css-header')
    <title>Store - {{ $productInfo['pro_name'] }}</title>
    <link href="{{ asset('assets/css/store.css', env('APP_HTTPS')) }}" rel="stylesheet" /> 
@endsection

@section('content')
<main>
        <div class="bg_animation"></div>
        <section class="section page_store">
            <div class="container">
                <div class="tit_cate">
                    <h2 class="title">store.319</h2>
                </div>
                <div class="content pt-5 pb-5">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="detail_image">
                                <div class="product_detail_image">
                                    <script type="text/javascript" src="/assets/js/modernizr.custom.js"></script>
                                    <link href="/assets/css/glasscase.min.css" rel="stylesheet" />
                                    <script type="text/javascript" src="/assets/js/jquery.glasscase.min.js"></script>
                                    <input type="hidden" id="__VIEWxSTATE" />
                                    <ul id='zoom_detail' class='gc-start'>
                                        @foreach ($productInfo['images'] as $images)
                                            <li><img src="{{$images['proi_image']}}" alt='' /></li>
                                        @endforeach
                                    </ul>
                                    <script>
                                        $("#zoom_detail").glassCase({
                                            'widthDisplay': 450,
                                            'heightDisplay': 480,
                                            'nrThumbsPerRow': 4,
                                            'isSlowZoom': true,
                                            'thumbsPosition': 'bottom',
                                            'colorIcons': '#fe5000',
                                            'colorActiveThumb': '#fe5000'
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 info_right">
                            <h4 class="title text-uppercase">{{ $productInfo['pro_name'] }}</h4>
                            <div class="sku_number text-right">{{ $productInfo['pro_sku'] ?? '' }}</div>
                            <form id="submitOrder" method="POST" action="{{ route('addToCart') }}">
                                @csrf                                
                                <div class="price_quantity">
                                    <div class="price">{{ number_format($productInfo['pro_price']) }} Đ</div>
                                    <div class="quantity">
                                        <div class="d-flex">
                                            <input class="input-value-button" size="1" idata="{{ $productInfo['pro_price'] }}" id="product1" name="count" value="1" min="1" max="99" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" />
                                            <input type="hidden" name="proId" value="{{$productInfo['pro_id']}}">
                                            <div class="value-button increaseValue" onclick="increaseValue('product1')">+</div>
                                            <div class="value-button decreaseValue" onclick="decreaseValue('product1')">-</div>
                                        </div>
                                    </div>
                                </div>
                                <a class="add_to_cart" >Thêm vào giỏ</a>
                            </form>
                            <div class="desc">
                                {!! $productInfo['pro_detail'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section release">
            <div class="container">
                <div class="tit_cate">
                    <h3 class="title">You may also like</h3>
                </div>
                <div class="list_item">
                    <div class="slider_release">
                        @foreach ($otherPro as $element)
                            @if ($element['pro_id'] == $productInfo['pro_id'])
                                @continue
                            @endif
                            <div class="item">
                                <a href="{{ route('storeProduct', ['id' => $element['pro_id']]) }}"><img class="w-100" src="{{$element['pro_image']}}" alt="" /></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
       $(document).ready(function() {
        $('.add_to_cart').on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $("#submitOrder").submit();
        });
            $('.slider_release').slick({
                autoplay: true,
                dots: false,
                arrows: true,
                prevArrow: '<button type="button " data-role="none " class="slick-prev slick-arrow " aria-label="Previous " role="button " style="display: block; "></button>',
                nextArrow: '<button type="button " data-role="none " class="slick-next slick-arrow " aria-label="Next " role="button " style="display: block; "></button>',
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });

            $(".value-button").on('click',function(event) {
                let price = $(".input-value-button").attr('idata');
                let numb  = $(".input-value-button").val();
                $(".price").html(numb * price + ' Đ');
            });
        })
    </script>
@endsection
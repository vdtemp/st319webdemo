@extends('main')

@section('css-header')
    <title>Album</title>
    <link href="{{ asset('assets/css/music-album.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_music_album">
            <div class="container">
                <div class="wrap_head">
                    <div class="list_tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link @if ($id <= 0) active @endif " data-toggle="tab" href="#tab_music" role="tab" aria-selected="false">music</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if ($id > 0) active @endif" data-toggle="tab" href="#tab_album" role="tab" aria-selected="true">album</a>
                            </li>
                        </ul>
                    </div>
                    <div class="custom_select">
                        <select name="" id="artist_choose">
                            <option class="artist_option" value="">artist</option>
                            @foreach ($listArtist as $key => $artist)
                                <option class="artist_option" value="{{ $key }}" @if ($artId == $key) selected @endif >{{$artist}}</option>   
                            @endforeach                            
                        </select>
                    </div>
                </div>
                <div class="tab-content">
                    <!-- tab music -->
                    <div id="tab_music" class="tab-pane fade @if ($id <= 0) active show @endif">
                        @foreach ($listAlbum as $key => $products)
                        <div class="album item_cate">
                            <div class="title"><span>{{ $arrType[$key] }}</span></div>
                            <div class="list_item row">
                                @foreach ($products as $pro_key=> $element) 
                                    <div class="item col-lg-220 col-md-3 col-sm-6">
                                        @switch($element['pro_type'])
                                            @case(1)
                                                <a href="{{ route('album', ['id' => $element['pro_id']]) }}">
                                                @break
                                            @case(2)
                                                <a href="{{ route('albumDetail', ['id' => $element['pro_id']]) }}">
                                                @break
                                            @default
                                                <a href="{{ route('album', ['id' => $element['pro_id']]) }}">
                                        @endswitch
                                            <div class="img_item"><img class="w-100" src="{{ $element['pro_image'] }}" alt=""></div>
                                            <div class="title">{{ $element['pro_name'] }}</div>
                                            <div class="release_year">{{ $element['pro_date'] }}</div>
                                        </a>
                                    </div>
                                @endforeach                                
                            </div>
                        </div>    
                        @endforeach 
                    </div>
                    <!-- tab album -->
                    <div id="tab_album" class="tab-pane fade @if ($id > 0) active show @endif">
                        <div class="content">
                            <div class="info row">
                                @if ($hotAlbum)
                                <div class="col-lg-6">
                                    <div class="img_album">
                                        <img class="w-100" src="{{ $hotAlbum['pro_image'] }}" alt="">
                                    </div>
                                    <div class="name_album">{{ $hotAlbum['pro_name'] }}</div>
                                    <div class="artist_time">
                                        <span class="artist">{{$listArtist[$hotAlbum['pro_artist_id']]}}</span>
                                        <span class="time">{{ $hotAlbum['pro_date'] }}</span>
                                    </div>
                                    <div class="desc">
                                        {!! $hotAlbum['pro_content'] !!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="list_item_album">
                                        @foreach ($hotAlbum['musics'] as $music)
                                        <div class="item_album">
                                            <div class="box_item">
                                                <div class="number">{{ $loop->iteration }}</div>
                                                <div class="name">{{ $music['albv_title'] }}</div>
                                                <div class="listen">listen on
                                                    <a target="_blank" href="{{ $music['albv_spotify'] }}"><img src="/assets/img/ic_spotify2.png" alt=""></a>
                                                    <a target="_blank" href="{{ $music['albv_itunes'] }}"><img src="/assets/img/ic_itunes2.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>   
                                        @endforeach                                         
                                    </div>
                                    <div><a href="{{ route('news') }}">see more</a></div>
                                    <div class="row">
                                        @foreach ($listNews as $news)
                                            <div class="col-6">
                                                <div class="release_content">
                                                    <a href="{{ route('newsDetail', ['id' => $news['new_id'], 'alias' => removeTitle($news['new_title'])]) }}" class="title">{{ $news['new_title'] }}</a>
                                                    <div class="date_post text-right">{{ customshowDate($news['new_date']) }}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>    
                                @endif
                            </div>
                            <div class="release_info item_cate section">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="tit_release">you may also like</div>
                                        <div class="list_item row">
                                            @foreach ($listAlbum[1] as $key => $album)
                                                @if ($loop->iteration == 3)
                                                    @break
                                                @endif
                                                @if ($album['pro_id'] == $hotAlbum['pro_id'])
                                                    @continue
                                                @endif
                                                <div class="item col-sm-4">
                                                    <a href="{{ route('album', ['id' => $album['pro_id']]) }}">
                                                        <div class="img_item"><img class="w-100" src="{{ $album['pro_image'] }}" alt=""></div>
                                                        <div class="title">{{ $album['pro_name'] }}</div>
                                                        <div class="release_year">{{ $album['pro_date'] }}</div>
                                                    </a>
                                                </div>  
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="tit_release">shop now</div>
                                        <div class="list_item row">
                                            @foreach ($sellingAlbum as $SellAlbum) 
                                                <div class="item col-sm-4">
                                                    <a href="">
                                                        <div class="img_item"><img class="w-100" src="{{ $SellAlbum['pro_image'] }}" alt=""></div>
                                                        <div class="title">{{ $SellAlbum['pro_name'] }}</div>
                                                        <div class="price">{{ number_format($SellAlbum['pro_price']) }}</div>
                                                    </a>
                                                </div>    
                                                @if ($loop->iteration == 2)
                                                    @break
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
<script type="text/javascript">
        $(document).ready(function() {
            $('.list_music_video').slick({
                autoplay: true,
                dots: false,
                arrows: true,
                prevArrow: '<button type="button " data-role="none " class="slick-prev slick-arrow " aria-label="Previous " role="button " style="display: block; "></button>',
                nextArrow: '<button type="button " data-role="none " class="slick-next slick-arrow " aria-label="Next " role="button " style="display: block; "></button>',
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                margin: 30,
                responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
        });
        // code custom select
        var x, i, j, l, ll, selElmnt, a, b, c, d;
        /*look for any elements with the class "custom-select":*/
        d = document.getElementsByClassName("artist_option");
        list_option = d.length;
        x = document.getElementsByClassName("custom_select");
        l = x.length;
        for (i = 0; i < l; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            ll = selElmnt.length;
            /*for each element, create a new DIV that will act as the selected item:*/
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /*for each element, create a new DIV that will contain the option list:*/
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < list_option; j++) {
                /*for each option in the original select element,
                create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) {
                    /*when an item is clicked, update the original select box,
                    and the selected item:*/
                    var y, i, k, s, h, sl, yl;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    sl = s.length;
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < sl; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;                            
                            let url = '/album?artId='+ s.value;
                            window.location.href = url;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            yl = y.length;
                            for (k = 0; k < yl; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) {
                /*when the select box is clicked, close any other select boxes,
                and open/close the current select box:*/
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
            except the current select box:*/
            var x, y, i, xl, yl, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            xl = x.length;
            yl = y.length;
            for (i = 0; i < yl; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < xl; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
    </script>
@endsection
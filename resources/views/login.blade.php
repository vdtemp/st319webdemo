<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
   <meta charset="UTF-8"> 
   <!-- <meta http-equiv="X-Frame-Options" content="deny"> -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="{{ asset('assets/css/bootstrap.min.css', env('APP_HTTPS')) }}" rel="stylesheet" />
   <link href="{{ asset('assets/css/font-awesome.css', env('APP_HTTPS')) }}" rel="stylesheet" />
   <link href="{{ asset('assets/css/slick.css', env('APP_HTTPS')) }}" rel="stylesheet" />
   <link href="{{ asset('assets/css/animate.css', env('APP_HTTPS')) }}" rel="stylesheet" />
   <link href="{{ asset('assets/css/css_global.css', env('APP_HTTPS')) }}" rel="stylesheet" />

   <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js', env('APP_HTTPS')) }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js', env('APP_HTTPS')) }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/wow.min.js', env('APP_HTTPS')) }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/slick.min.js', env('APP_HTTPS')) }}"></script>
   <script type="text/javascript" src="{{ asset('assets/js/main.js', env('APP_HTTPS')) }}"></script>
</head>

<body class="body_home">
   <div style="width: 350px; margin: 20px auto;">
      <div style="text-align: center;"><span class="tx-normal"></span><span class="tx-info">ST319</span> <span class="tx-normal"></span></div>
      <br>
      <form action="{{ route('userActLogin') }}" method="post" class="mg-t-20">
         @csrf
         <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username">
         </div><!-- form-group -->
         <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password">
         </div><!-- form-group -->
         <button type="submit" class="btn btn-info btn-block mg-t-20">Sign In</button>
      </form>
   </div>
</body>
</html>
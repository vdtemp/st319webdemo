@extends('main')

@section('css-header')
    <title>Single - {{ $single['pro_name'] }}</title>
    <link href="{{ asset('assets/css/music-album.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_music_album">
            <div class="container">
                <div class="wrap_head">
                    <div class="list_tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tab_music" role="tab" aria-selected="false">music</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_album" role="tab" aria-selected="true">album</a>
                            </li>
                        </ul>
                    </div>
                    <div class="custom_select">
                        <select name="" id="">
                            <option class="artist_option" value="">artist</option>
                             @foreach ($listArtist as $key => $artist)
                                <option class="artist_option" value="{{ $key }}" >{{$artist}}</option>   
                            @endforeach    
                        </select>
                    </div>
                </div>
                <div class="tab-content">
                    <!-- tab music -->
                    <div id="tab_music" class="tab-pane fade active show">
                        <div class="content">
                            <div class="hot_video">
                                {!! $single['pro_link_video'] !!}
                            </div>
                            <!-- list 1 -->
                            <div class="list_item list_music_video mt-5 mb-4">
                                @foreach ($listAlbum[2] as $element)
                                    <div class="item">
                                        <a href="{{ route('albumDetail', ['id' => $element['pro_id']]) }}">
                                            <div class="img_item">
                                                <img class="w-100" src="{{ $element['pro_image'] }}" alt="">
                                            </div>
                                            <h4 class="name">{{ $element['pro_name'] }}</h4>
                                            <div class="channel">ST.319 Entertainment</div>
                                            <div class="views_time">
                                                <span class="views">8,5Tr lượt xem</span>
                                                <span class="time">2 tháng trước</span>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach                                
                            </div>                            
                            <!-- pagination -->
                            {{-- <div class="pagination mt-5 justify-content-center">
                                <ul>
                                    <li><a href="">1</a></li>
                                    <li><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href="">4</a></li>
                                    <li><a href="">...</a></li>
                                </ul>
                            </div> --}}
                        </div>
                    </div>
                    <!-- tab album -->
                    <div id="tab_album" class="tab-pane fade">
                        <div class="content">
                            <div class="info row">
                                @if ($hotAlbum)
                                <div class="col-lg-6">
                                    <div class="img_album">
                                        <img class="w-100" src="{{ $hotAlbum['pro_image'] }}" alt="">
                                    </div>
                                    <div class="name_album">{{ $hotAlbum['pro_name'] }}</div>
                                    <div class="artist_time">
                                        <span class="artist">{{$listArtist[$hotAlbum['pro_artist_id']]}}</span>
                                        <span class="time">{{ $hotAlbum['pro_date'] }}</span>
                                    </div>
                                    <div class="desc">
                                        {!! $hotAlbum['pro_content'] !!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="list_item_album">
                                        @foreach ($hotAlbum['musics'] as $music)
                                        <div class="item_album">
                                            <div class="box_item">
                                                <div class="number">{{ $loop->iteration }}</div>
                                                <div class="name">{{ $music['albv_title'] }}</div>
                                                <div class="listen">listen on
                                                    <a target="_blank" href="{{ $music['albv_spotify'] }}"><img src="/assets/img/ic_spotify2.png" alt=""></a>
                                                    <a target="_blank" href="{{ $music['albv_itunes'] }}"><img src="/assets/img/ic_itunes2.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>   
                                        @endforeach                                         
                                    </div>
                                    <div><a href="{{ route('news') }}">see more</a></div>
                                    <div class="row">
                                        @foreach ($listNews as $news)
                                            <div class="col-6">
                                                <div class="release_content">
                                                    <a href="{{ route('newsDetail', ['id' => $news['new_id'], 'alias' => removeTitle($news['new_title'])]) }}" class="title">{{ $news['new_title'] }}</a>
                                                    <div class="date_post text-right">{{ customshowDate($news['new_date']) }}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>    
                                @endif
                            </div>
                            <div class="release_info item_cate section">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="tit_release">you may also like</div>
                                        <div class="list_item row">
                                            @foreach ($listAlbum[1] as $key => $album)
                                                @if ($loop->iteration == 3)
                                                    @break
                                                @endif
                                                @if ($album['pro_id'] == $hotAlbum['pro_id'])
                                                    @continue
                                                @endif
                                                <div class="item col-sm-4">
                                                    <a href="{{ route('album', ['id' => $album['pro_id']]) }}">
                                                        <div class="img_item"><img class="w-100" src="{{ $album['pro_image'] }}" alt=""></div>
                                                        <div class="title">{{ $album['pro_name'] }}</div>
                                                        <div class="release_year">{{ $album['pro_date'] }}</div>
                                                    </a>
                                                </div>  
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="tit_release">shop now</div>
                                        <div class="list_item row">
                                            @foreach ($sellingAlbum as $SellAlbum) 
                                                <div class="item col-sm-4">
                                                    <a href="">
                                                        <div class="img_item"><img class="w-100" src="{{ $SellAlbum['pro_image'] }}" alt=""></div>
                                                        <div class="title">{{ $SellAlbum['pro_name'] }}</div>
                                                        <div class="price">{{ number_format($SellAlbum['pro_price']) }}</div>
                                                    </a>
                                                </div>    
                                                @if ($loop->iteration == 2)
                                                    @break
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.list_music_video').slick({
                autoplay: true,
                dots: false,
                arrows: true,
                prevArrow: '<button type="button " data-role="none " class="slick-prev slick-arrow " aria-label="Previous " role="button " style="display: block; "></button>',
                nextArrow: '<button type="button " data-role="none " class="slick-next slick-arrow " aria-label="Next " role="button " style="display: block; "></button>',
                infinite: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                margin: 30,
                responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
        });
        // code custom select
        var x, i, j, l, ll, selElmnt, a, b, c, d;
        /*look for any elements with the class "custom-select":*/
        d = document.getElementsByClassName("artist_option");
        list_option = d.length;
        x = document.getElementsByClassName("custom_select");
        l = x.length;
        for (i = 0; i < l; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            ll = selElmnt.length;
            /*for each element, create a new DIV that will act as the selected item:*/
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            /*for each element, create a new DIV that will contain the option list:*/
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < list_option; j++) {
                /*for each option in the original select element,
                create a new DIV that will act as an option item:*/
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) {
                    /*when an item is clicked, update the original select box,
                    and the selected item:*/
                    var y, i, k, s, h, sl, yl;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    sl = s.length;
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < sl; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            yl = y.length;
                            for (k = 0; k < yl; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) {
                /*when the select box is clicked, close any other select boxes,
                and open/close the current select box:*/
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
            except the current select box:*/
            var x, y, i, xl, yl, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            xl = x.length;
            yl = y.length;
            for (i = 0; i < yl; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < xl; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
    </script>
@endsection
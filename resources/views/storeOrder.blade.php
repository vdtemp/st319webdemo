@extends('main')

@section('css-header')
    <title>Store - </title>
    <link href="{{ asset('assets/css/store.css', env('APP_HTTPS')) }}" rel="stylesheet" />
@endsection

@section('content')
    <main>
        <div class="bg_animation"></div>
        <section class="section page_store">
            <form class="container" method="post" action="{{ route('submitOrder') }}">
                @csrf
                <div class="tit_cate">
                    <h2 class="title">store.319</h2>
                </div>
                <div class="item_cart">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="title">
                                    <span class="number">1</span>
                                    <span class="text">Giỏ hàng <br> của bạn</span>
                                </th>
                                <th>
                                    Sản phẩm
                                </th>
                                <th>
                                    Đơn giá
                                </th>
                                <th>
                                    Số lượng
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                                $total_money = 0;
                            ?>
                            @foreach ($products as $pro)
                                <tr>
                                    <td><img class="w-100" src="{{ $pro['pro_image'] }}"></td>
                                    <td>{{ $pro['pro_name'] }}</td>
                                    <td>{{ number_format($pro['pro_price']) }} đ</td>
                                    <td>
                                        <div class="quantity">
                                            <div class="d-flex">
                                                <input type="hidden" name="pro_id[]" value="{{$pro['pro_id']}}" >
                                                <input class="input-value-button inputNumb" size="1" id="product{{ $loop->iteration }}" iprice="{{ $pro['pro_price'] }}" name="numb[]" value="1" min="1" max="99" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" />
                                                <div class="value-button increaseValue" onclick="increaseValue('product{{ $loop->iteration }}')">+</div>
                                                <div class="value-button decreaseValue" onclick="decreaseValue('product{{ $loop->iteration }}')">-</div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?
                                $total_money += $pro['pro_price'] * $order[$pro['pro_id']];
                                ?>
                            @endforeach
                            
                            <tr>
                                <td>
                                    <a class="btn_back" href="{{ route('store') }}"><i class="fa fa-chevron-left mr-1"></i> Tiếp tục chọn sản phẩm</a>
                                </td>
                                <td class="total_price text-center">Tổng tiền</td>
                                <td class="total_number">{{ number_format($total_money) }} đ</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="item_cart">
                    <div class="title">
                        <span class="number">2</span>
                        <span class="text">Thông tin <br> khách hàng</span>
                    </div>
                    <div class="customer_info">
                        <div class="row">
                            <div class="col-lg-6 col-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Họ và tên*">
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email*">
                                </div>
                            </div>
                            <div class="col-lg-6 col-6">
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control" placeholder="Điện thoại*">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" name="address" class="form-control" placeholder="Địa chỉ nhận hàng*">
                                </div>
                            </div>
                            {{-- <div class="col-lg-3 col-6">
                                <div class="form-group">
                                    <select class="form-control" name="city">
                                        <option value="">Tỉnh, thành phố*</option>
                                        <option value="Hà Nội">Hà Nội</option>
                                        <option value="Hà Nội">Hồ Chí Minh</option>
                                        <option value="Hà Nội">Đà Nẵng</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-6">
                                <div class="form-group">
                                    <select class="form-control"  name="district"> 
                                        <option value="">Quận, huyện*</option>
                                        <option value="Hà Nội">Đông Anh</option>
                                        <option value="Hà Nội">Tây Hồ</option>
                                        <option value="Hà Nội">Cầu Giấy</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="note" placeholder="Nội dung*"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="text-center"><button type="submit" class="btn btn-submit mt-4">Hoàn tất đặt hàng</button></div>
                    </div>
                </div>
            </form>
        </section>
    </main>
@endsection

@section('js-footer')
    <script type="text/javascript">
        function countPrice(){
            let priceTotal = 0;
            $(".inputNumb").each(function(index, el) {
                let price = $(this).attr('iprice');
                let numb  = $(this).val();
                priceTotal += price*numb;
            });

            $(".total_number").html(priceTotal + ' đ');
        };
        $(document).ready(function() {
            $(".value-button").on('click', function(event) {
                event.preventDefault();
                /* Act on the event */
                countPrice();
            });
        });
    </script>
@endsection
@extends('main')
@section('content')
	    <main>
        <!-- slider -->
        <div class="bg_animation"></div>
        <section class="banner">
            <div class="slide_banner">
                @foreach ($banners as $ban)
                    <div class="item">
                        <a href="{{  $ban['ban_target_url'] ?? '' }}"><img class="w-100" src="{{ $ban['ban_picture'] }}" alt="" /></a>
                    </div>
                @endforeach
            </div>
        </section>
        <div class="bg_animation"></div>
        <!-- content -->
        <section class="section">
            <div class="container">
                <div class="row">
                    <!-- news -->
                    <div class="item_cate cate_news col-lg-6 wow bounceInLeft" data-wow-duration="6s">
                        <div class="tit_cate">
                            <h2 class="title">news</h2>
                            <a class="view_more" href="{{ route('news') }}">More</a>
                        </div>
                        @foreach ($hotNews as $element)
                        <div class="hot_news row">
                            <div class="col-lg-4">
                                <div class="info mb-2">
                                    <div class="title"><a href="{{ route('newsDetail', ['id' =>$element['new_id'], 'alias' => removeTitle($element['new_title']) ]) }}">{{$element['new_title']}} {{$element['new_teaser']}}</a></div>
                                    <div class="date_post mt-2 mt-lg-5">{{ customshowDate($element['new_date']) }}</div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="img_item"><img class="w-100" src="{{ $element['new_picture'] ?? '/assets/img/news1.png' }}"></div>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                    <!-- campaign -->
                    <div class="item_cate cate_campaign col-lg-3 col-sm-6 wow bounceInUp" data-wow-duration="8s">
                        @foreach ($hotCampaign as $element)
                            <div class="hot_news">
                                <div class="img_item">
                                    <a href="{{ route('campaignDetail', ['id' => $element['camp_id'], 'alias' => removeTitle($element['camp_title'])]) }}"><img class="w-100" src="{{ $element['camp_picture'] }}"></a>
                                </div>
                                <div class="tit_cate bd_bot">
                                    <h2 class="title">campaign</h2>
                                    <a class="view_more" href="{{ route('campaign') }}">More</a>
                                </div>
                                <div class="info d-lg-flex">
                                    <div class="title"><a href="{{ route('campaignDetail', ['id' => $element['camp_id'], 'alias' => removeTitle($element['camp_title'])]) }}">{{ $element['camp_title'] }}</a></div>
                                    <div class="date_post text-right">{{ customshowDate($element['camp_created_date']) }}</div>
                                </div>
                            </div>
                        @endforeach
                        
                    </div>
                    <!-- article -->
                    <div class="item_cate cate_article col-lg-3 col-sm-6 wow bounceInRight" data-wow-duration="10s">
                        @foreach ($hotArticle as $Article)
                            <div class="hot_news">
                            <div class="img_item"><img class="w-100" src="{{$Article['new_picture']}}"></div>
                            <div class="tit_cate bd_bot">
                                <h2 class="title">article</h2>
                                <a class="view_more" href="{{ route('news') }}">More</a>
                            </div>
                            <div class="info">
                                <div class="title"><a href="{{ route('newsDetail', ['id' => $Article['new_id'], 'alias' => removeTitle($Article['new_title'])]) }}">{{$Article['new_title']}}</a></div>
                                <div class="date_post text-right">{{ customshowDate($Article['new_date']) }}</div>
                            </div>
                        </div>
                        @endforeach                        
                    </div>
                </div>
            </div>
        </section>
        <section class="bg_gradient section">
            <div class="container">
                <!-- music video -->
                <div class="item_cate cate_music mb-0 wow bounceInUp" data-wow-duration="2s">
                    <div class="tit_cate">
                        <h2 class="title">music video</h2>
                        <a class="view_more" href="{{ route('album') }}">More</a>
                    </div>
                    <div class="list_item list_top">
                        <?
                        $i = '';
                        $class = 'hot_news';
                        foreach($hotProduct as $key => $product){
                            $i++;
                            if($i == 4) break;
                        ?>
                            <div class=" <?=$class?> wow bounceInUp" data-wow-duration="<?=$i?>s">
                                <div class="img_item">
                                    <a href="{{ route('album', ['id' => $product['pro_id']]) }}"><img class="w-100" src="{{$product['pro_image']}}"></a>
                                </div>
                            </div>
                        <?    
                            /*if($i == 3){
                                echo '</div><div class="list_item list_bot row mt-4">';
                                $class  =  'col-lg-220 col-12 item';
                            }*/
                        }
                        ?>                      
                    </div>                     
                </div>
            </div>
        </section>
    </main>
@endsection

@section('js-footer')
	<script type="text/javascript">
	  $(document).ready(function() {
	      $('.slide_banner').slick({
	          autoplay: true,
	          autoplaySpeed: 3000,
	          dots: true,
	          arrows: false,
	          infinite: true,
	          speed: 300,
	      });
	      $(window).scroll(function() {
	          if ($(window).scrollTop() >= 1500) {
	              $('.bg_gradient').addClass('is-active');
	          }
	      });
	  })
	</script>
@stop
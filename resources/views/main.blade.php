<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  {{-- {!! SEOMeta::generate() !!} --}}
  {{-- {!! OpenGraph::generate() !!} --}}
  <!-- <meta http-equiv="X-Frame-Options" content="deny"> -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link href="{{ asset('assets/css/bootstrap.min.css', env('APP_HTTPS')) }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/font-awesome.css', env('APP_HTTPS')) }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/slick.css', env('APP_HTTPS')) }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/animate.css', env('APP_HTTPS')) }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/css_global.css', env('APP_HTTPS')) }}" rel="stylesheet" />

  <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js', env('APP_HTTPS')) }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js', env('APP_HTTPS')) }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/wow.min.js', env('APP_HTTPS')) }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/slick.min.js', env('APP_HTTPS')) }}"></script>
  <script type="text/javascript" src="{{ asset('assets/js/main.js', env('APP_HTTPS')) }}"></script>
  <script>
      new WOW().init();
  </script>
  @yield('css-header') 
</head>

<body class="body_home">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v12.0&appId=105375333315086&autoLogAppEvents=1" nonce="7e3btyJo"></script>
    @include('includes.header')
    @yield('content')
    @include('includes.footer') 

    @yield('js-footer')
</body>
</html>
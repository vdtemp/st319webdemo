<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('assets/admin/lib/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/bracket.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/admin/css/jquery.growl.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="hold-transition login-page">

	<div class="d-flex align-items-center justify-content-center bg-br-primary ht-100v">
		<div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 bg-white rounded shadow-base">
			<div class="signin-logo tx-center tx-28 tx-bold tx-inverse"><span class="tx-normal"></span> ADMIN <span class="tx-info">ST319</span> <span class="tx-normal"></span></div>
			<div class="tx-center mg-b-20" style="display: none;">The Admin Template For Perfectionist</div>
			<form action="{{route('admin.do_login')}}" method="post" class="mg-t-20">
        		@csrf
        		<!-- @error('username')
		            <div class="alert alert-danger alert-dismissible">
		              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		              <p>Tên đăng nhập không chính xác</p>
		            </div>
		        @enderror
		        @error('password')
		            <div class="alert alert-danger alert-dismissible">
		              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		              <p>Mật khẩu không chính xác</p>
		            </div>
		        @enderror -->
				<div class="form-group">
					<input type="text" name="username"  class="form-control" placeholder="Enter your username">
				</div><!-- form-group -->
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Enter your Password">
					<a href="" class="tx-info tx-12 d-block mg-t-10" style="display: none !important;">Quên mật khẩu?</a>
				</div><!-- form-group -->
				<button type="submit" class="btn btn-info btn-block mg-t-20">Sign In</button>
			</form>
		</div><!-- login-wrapper -->
	</div><!-- d-flex -->

<!-- /.login-box -->
<script language="javascript" src="{{ asset('assets/admin/lib/jquery/jquery.min.js') }}"></script>
<script language="javascript" src="{{ asset('assets/admin/lib/jquery-ui/ui/widgets/datepicker.js') }}"></script>
<script language="javascript" src="{{ asset('assets/admin/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script language="javascript" src="{{ asset('assets/admin/js/jquery.growl.js') }}"></script>


@include("admin.flash-message")

</body>
</html>

@extends('admin.layouts')
@section('CONTAINER')
<style type="text/css">
	.listing-container .fa-check-square, .fa-square{
		cursor: pointer;
	}
	.detailProduct{
		display: grid;
    	grid-template-columns: 80px auto 20%;
    	margin: 10px 0px;

	}
</style>
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.order.index')}}">Đơn hàng</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách Đơn hàng</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-sm-2">
		               <input type="text" name="name" value="{{ $params['name'] ?? '' }}" class="form-control" placeholder="Tên KH">
		            </div>
		            <div class="col-sm-2">
		               <input type="text" name="phone" value="{{ $params['phone'] ?? '' }}" class="form-control" placeholder="SDT">
		            </div>
		            <div class="col-sm-2">
		               <input type="text" name="email" value="{{ $params['email'] ?? '' }}" class="form-control" placeholder="Email">
		            </div>
		            <div class="col-sm-2">
		              <select  class="form-control" name="uso_status" >
		              	<option value="-1">Trạng thái</option>
		              	@foreach($arrStatus as $key => $value)
		              		<option {{ (isset($params['uso_status']) && $params['uso_status'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>
		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		            <tr class="thead-colored thead-light">
				          	<td>Stt</td>
				          	<td>Sản phẩm</td>				          	
				          	<td class="text-center">Tổng tiền</td>
				          	<td class="text-center">TT liên hệ</td> 
				          	<td class="text-center">Ngày đặt</td>
				          	<td class="text-center">Ghi chú</td>
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($UserOrder as $keys => $order)
					      	<tr>
							   	<td>{{ ++$keys + ($page-1)*15 }}</td>
					           <td>
					           		<a href="">Mã đơn: #{{ $order['uso_id'] ?? ''  }}</a> 
					           		<br>				           		
					           		@foreach ($order['detail'] as $detail)
					           			<div class="detailProduct">
					           				<img src="{{ isset($products[$detail['uod_product_id']]) ? $products[$detail['uod_product_id']]['pro_image'] : '' }} " width="70">
					           				<p> {{ isset($products[$detail['uod_product_id']]) ? $products[$detail['uod_product_id']]['pro_name'] : '' }}  x {{$detail['uod_quantity']}}</p>
					           				<p>{{ number_format($detail['uod_total_money']) }}đ</p>
					           			</div>
					           		@endforeach
					           </td>					           
					           <td> 
					           		<p>{{ number_format($order['uso_total_value']) }} đ</p>
		            			</td>
					           <td class="">
					           		<p>Khách hàng: {{ $order['uso_user_name'] }}</p>
					           		<p>Email: {{ $order['uso_user_email'] }}</p>
					           		<p>Điện thoại: {{ $order['uso_user_phone'] }}</p>
					           		<p>Địa chỉ: {{ $order['uso_user_address'] }}</p>
					           </td>					            
					           <td class="text-center">
					           		<p>{{ date('d/m/Y H:i:s', $order['uso_date'])  }}</p>
					           </td>
					           <td class="text-center">
					          		<p>{{ $order['uso_customer_note'] }}</p>
					           </td>
					           <td class="text-center">
					           		<b>{{ $arrStatus[$order['uso_status']] ?? ''  }}</b>
					           </td>
					           <td class="text-center">
					           		{!! Form::open(['url' => '/admin/order/'. $order['uso_id'].'/confirm' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn xác nhận duyệt đơn này?")']); !!}
						          		<button type="submit"  class="btn-primary" style="cursor: pointer;"><i class="fas fa-edit"></i> Duyệt</button>
					          		{!! Form::close() !!}

					           		{!! Form::open(['url' => '/admin/order/'. $order['uso_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Hủy</button>
					          		{!! Form::close() !!}					          		
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            {!! $UserOrder->appends($params)->render() !!}
        </div>
    </section>

</div>
@endsection
@section("JS_FOOTER")
<script type="text/javascript">
	$(function() {	
		 
	});
</script>
@endsection


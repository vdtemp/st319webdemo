@extends('admin.layouts')
@section('CONTAINER')
<style type="text/css">
	.listing-container .fa-check-square, .fa-square{
		cursor: pointer;
	}
</style>
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.news.index')}}">Chiến dịch</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="camp_title" placeholder="Tiêu đề bài viết" type="text" value="{{ isset($params['camp_title']) ? $params['camp_title'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="lang_id" >
		              	<option value="-1">=Ngôn ngữ=</option>
		              	@foreach($arrLang as $key => $value)
		              		<option {{ (isset($params['lang_id']) && $params['lang_id'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
								<div class="col-lg">
		              <select  class="form-control" name="camp_hot" >
		              	<option value="-1">=Hot=</option>
		              	@foreach($arrHot as $key => $value)
		              		<option {{ (isset($params['camp_hot']) && $params['camp_hot'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="camp_status" >
		              	<option value="-1">=Kích hoạt=</option>
		              	@foreach($arrStatus as $key => $value)
		              		<option {{ (isset($params['camp_status']) && $params['camp_status'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tiêu đề</td>
				          	<td class="text-center">Ảnh</td> 
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Nổi bật</td>
				          	<td class="text-center">Thứ tự</td>
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($campaign as $keys => $camp)
					      	<tr>
							   <td>{{ ++$keys + ($page-1)*15 }}</td>
					           <td>
					           		<p>{{ $camp['camp_title'] }}</p>
					           		<p>Người tạo: <b>{{ $arrUser[$camp['admin_id']] ?? 'admin' }}</b></p>
					           </td>
					           <td class="text-center"><img width="100px" src="{{ $camp['camp_picture'] }}"></td>					            
					           <td class="text-center">{{ date('H:i:s d/m/Y', $camp['camp_created_date'])  }}</td>
					           <td class="text-center">
					          		<i class="far {{($camp['camp_hot'] == 1) ? 'fa-check-square' : 'fa-square' }}" idata="{{$camp['camp_id']}}" onclick="updateHot($(this))"></i>
					           </td>
					           <td>
		            			<input class="form-control text-center order_by" idata="{{$camp['camp_id']}}" style="width: 75px; margin: auto;" type="text" value="{{ $camp['camp_order'] }}">
		            			</td>
					           <td class="text-center">
					           		<b>
					            	@if($camp['camp_status'] <= 0)
					            		Đợi duyệt
					            	@elseif($camp['camp_status'] == 2)
					            		Không duyệt
					            	@elseif($camp['camp_status'] == 1)
					            		Đã duyệt
					            	@endif
					            	</b>
					           </td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.campaign.edit', ['id' => $camp['camp_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{!! Form::open(['url' => '/admin/campaign/'. $camp['camp_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}					          		
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            {!! $campaign->appends($params)->render() !!}
        </div>
    </section>

</div>
@endsection

@section("JS_FOOTER")
<script type="text/javascript">
	$(function() {	
		$(".order_by").on('change',function(){
			let id = $(this).attr('idata');
			let val = $(this).val();

			/*Update orrder*/
			$.ajax({
				url: '{{ route('api.change_order.newscategoryChangeOrder') }}',
				type: 'POST',
				dataType: 'JSON',
				data: {id:id, value:val},
				success: function(data){
					if(data.status == 0){
						console.log('success');
					}
				}
			});			
		});
	}); 

	function updateHot(obj){
		let idata = obj.attr('idata');
		$.ajax({
        url: '{{route('api.set_hot.hotNews')}}',
        type: "POST",
        dataType: 'json',
        data: {idata:idata},
        success: function(html){
         	if(obj.hasClass('fa-check-square')){
         		obj.removeClass('fa-check-square').addClass('fa-square')
         	}else if(obj.hasClass('fa-square')){
         		obj.removeClass('fa-square').addClass('fa-check-square')
         	}  
        }
    });
	}; 
</script>
@endsection


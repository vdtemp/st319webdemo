@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.campaign.index')}}">Chiến dịch</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới Chiến dịch</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="{{route('admin.campaign.create')}}" enctype="multipart/form-data">
            	@csrf  
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Ngôn ngữ</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('lang_id', $arrLang, old('lang_id') ? old('lang_id') : null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('camp_title', old('camp_title') ? old('camp_title') : null, ['class' => 'form-control','placeholder' => 'Nhập tiêu đề']) !!}
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Meta Decription</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('camp_meta_desc', old('camp_meta_desc') ? old('camp_meta_desc') : null, ['class' => 'form-control','placeholder' => 'Nhập từ mô tả','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Meta keyword</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::textarea('camp_meta_keyword', old('camp_meta_keyword') ? old('camp_meta_keyword') : null, ['class' => 'form-control','placeholder' => 'Nhập từ khóa','style="height:100px;"']) !!}
	                </div>
                </div>                
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="camp_picture" id="camp_picture" value="{{ old('camp_picture') }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'campaign', 'max_upload' => 10, 'multiple' => true ,'item_upload' => 'camp_picture', 'name_data' => 'picture_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả ngắn</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('camp_intro', old('camp_intro') ? old('camp_intro') : null, ['class' => 'form-control','placeholder' => 'Nhập mô tả','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nội dung (*)</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('camp_description', old('camp_description') ? old('camp_description') : null, ['class' => 'form-control','id' => 'editor1','rows="3"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Thứ tự</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('camp_order', old('camp_order') ? old('camp_order') : null, ['class' => 'form-control','placeholder' => '0']) !!}
	                </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Video</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('camp_link_video', old('camp_link_video') ? old('camp_link_video') : null, ['class' => 'form-control','placeholder' => 'Video youtube','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nổi bật</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('camp_hot', '1', old('camp_hot') ? true : false, ['class' => 'custom-control-input', 'id' => 'camp_hot']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="camp_hot"></label>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('camp_status', '1', old('camp_status') ? true : false, ['class' => 'custom-control-input', 'id' => 'camp_status']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="camp_status"></label>
                        </div>
                    </div>
                </div>                
                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="insert">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Thêm mới</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
    <script src="{{ asset('/assets/ckeditorStandard/ckeditor.js') }}"></script>    
    <script>
    CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
    } );
    </script>
    @include('ckfinder::setup')
    
    <script type="text/javascript">
        var urlUpload = '{{ route("admin.campaign.create.upload")}}';
    </script>
    <script>
    $(function() {
        $('.frm_select_2').select2();
        $(".select_type").on('change', function(){
            let id = $(this).val();
            if(id == 41){
                $("#box_show_services").show();
            }else{
                $("#box_show_services").hide();
            }
        });
    });
    </script>
@endsection
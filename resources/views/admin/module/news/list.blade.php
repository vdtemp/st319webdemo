@extends('admin.layouts')
@section('CONTAINER')
<style type="text/css">
	.listing-container .fa-check-square, .fa-square{
		cursor: pointer;
	}
</style>
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.news.index')}}">Bài viết</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách Bài viết</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="news_title" placeholder="Tiêu đề bài viết" type="text" value="{{ isset($params['news_title']) ? $params['news_title'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="lang_id" >
		              	<option value="-1">=Ngôn ngữ=</option>
		              	@foreach($arrLang as $key => $value)
		              		<option {{ (isset($params['lang_id']) && $params['lang_id'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
								<div class="col-lg">
		              <select  class="form-control" name="news_hot" >
		              	<option value="-1">=Tin Hot=</option>
		              	@foreach($arrHot as $key => $value)
		              		<option {{ (isset($params['news_hot']) && $params['news_hot'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="news_status" >
		              	<option value="-1">=Kích hoạt=</option>
		              	@foreach($arrStatus as $key => $value)
		              		<option {{ (isset($params['news_status']) && $params['news_status'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="new_type" >
		              	<option value="-1">=Loại=</option>
		              	@foreach($arrType as $key => $value)
		              		<option {{ (isset($params['new_type']) && $params['new_type'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>

		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tiêu đề</td>
				          	<td>Loại</td>
				          	<td class="text-center">Ảnh bài viết</td> 
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Nổi bật</td>
				          	<td class="text-center">Thứ tự</td>
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($news as $keys => $new)
					      	<tr>
							   <td>{{ ++$keys + ($page-1)*15 }}</td>
					           <td>
					           		<p>{{ $new['new_title'] }}</p>
					           		<p>Người tạo: <b>{{ $arrUser[$new['admin_id']] ?? 'admin' }}</b></p>
					           		@isset ($arrUser[$new['edit_admin']])
					           			<p>Chỉnh sửa: <b>{{ $arrUser[$new['edit_admin']] ?? null }}</b></p>    
					           		@endisset					           		
					           </td>
					           <td>
					           	{{ $arrType[$new['new_type']] }}
					           </td>
					           <td class="text-center"><img width="100px" src="{{ $new['new_picture'] }}"></td>					            
					           <td class="text-center">{{ date('H:i:s d/m/Y', $new['new_date'])  }}</td>
					           <td class="text-center">
					          		<i class="far {{($new['new_hot'] == 1) ? 'fa-check-square' : 'fa-square' }}" idata="{{$new['new_id']}}" onclick="updateHot($(this))"></i>
					           </td>
					           <td>
		            			<input class="form-control text-center order_by" idata="{{$new['new_id']}}" style="width: 75px; margin: auto;" type="text" value="{{ $new['new_order'] }}">
		            			</td>
					           <td class="text-center">
					           		<b>
					            	@if($new['new_active'] <= 0)
					            		Đợi duyệt
					            	@elseif($new['new_active'] == 2)
					            		Không duyệt
					            	@elseif($new['new_active'] == 1)
					            		Đã duyệt
					            	@endif
					            	</b>
					           </td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.news.edit', ['id' => $new['new_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{{-- <p><a target="_blank" href="/tin-tuc/preview/{{$new['new_id']}}-{{removeTitle($new['new_title'])}}"><i class="fas fa-edit"></i> Preview</a></p> --}}
					           		{!! Form::open(['url' => '/admin/news/'. $new['new_id'].'/delete/'.$page , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}					          		
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            {!! $news->appends($params)->render() !!}
        </div>
    </section>

</div>
@endsection

@section("JS_FOOTER")
<script type="text/javascript">
	$(function() {	
		$(".order_by").on('change',function(){
			let id = $(this).attr('idata');
			let val = $(this).val();

			/*Update orrder*/
			$.ajax({
				url: '{{ route('api.change_order.newscategoryChangeOrder') }}',
				type: 'POST',
				dataType: 'JSON',
				data: {id:id, value:val},
				success: function(data){
					if(data.status == 0){
						console.log('success');
					}
				}
			});			
		});
	}); 

	function updateHot(obj){
		let idata = obj.attr('idata');
		$.ajax({
        url: '{{route('api.set_hot.hotNews')}}',
        type: "POST",
        dataType: 'json',
        data: {idata:idata},
        success: function(html){
         	if(obj.hasClass('fa-check-square')){
         		obj.removeClass('fa-check-square').addClass('fa-square')
         	}else if(obj.hasClass('fa-square')){
         		obj.removeClass('fa-square').addClass('fa-check-square')
         	}  
        }
    });
	}; 
</script>
@endsection


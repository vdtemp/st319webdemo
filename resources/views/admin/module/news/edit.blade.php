@extends('admin.layouts') 
@section('CONTAINER')


<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.news.index')}}">Bài viết</a>
      <span class="breadcrumb-item active">Sửa</span>
    </nav>
</div>
<div class="br-pagetitle">
    <h4>Sửa bài viết:  {{ $news['new_title'] }}</h4>
</div>

<div class="br-pagebody">
    <section class="listing-container">
        <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="/admin/news/{{ $news['new_id'] }}/edit" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Ngôn ngữ</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('lang_id', $arrLang, old('lang_id') ? old('lang_id') : $news['lang_id'] ?? null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Loại</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('new_type', $arrType, old('new_type') ? old('new_type') : $news['new_type'] ?? null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('new_title', old('new_title') ? old('new_title') : $news['new_title'] ?? null, ['class' => 'form-control','placeholder' => 'Nhập tiêu đề bài viết']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Meta Decription</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('new_meta_desc', old('new_meta_desc') ? old('new_meta_desc') : $news['new_meta_desc'] ?? null, ['class' => 'form-control','placeholder' => 'Nhập từ mô tả','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Meta keyword</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('new_meta_keyword', old('new_meta_keyword') ? old('new_meta_keyword') : $news['new_meta_keyword'] ?? null, ['class' => 'form-control','placeholder' => 'Nhập từ khóa','style="height:100px;"']) !!}
                    </div>
                </div>                
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="new_picture" id="new_picture" value="{{ old('new_picture') ? old('new_picture') : $news['new_picture'] ?? null  }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'news', 'max_upload' => 10, 'multiple' => true ,'item_upload' => 'new_picture', 'name_data' => 'picture_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả ngắn</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('new_teaser', old('new_teaser') ? old('new_teaser') : $news['new_teaser'] ?? null, ['class' => 'form-control','placeholder' => 'Nhập từ mô tả','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nội dung (*)</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('new_description', old('new_description') ? old('new_description') : $news['new_description'] ?? null, ['class' => 'form-control','id' => 'editor1','rows="3"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Thứ tự</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('new_order', old('new_order') ? old('new_order') : $news['new_order'] ?? null, ['class' => 'form-control','placeholder' => '0']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Video</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('new_link_video', old('new_link_video') ? old('new_link_video') : $news['new_link_video'] ?? null, ['class' => 'form-control','placeholder' => 'Video youtube','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nổi bật</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('new_hot', '1', $news['new_hot'] ? true : false, ['class' => 'custom-control-input', 'id' => 'new_hot']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="new_hot"></label>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('new_active', '1', $news['new_active'] ? true : false, ['class' => 'custom-control-input', 'id' => 'new_active']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="new_active"></label>
                        </div>
                    </div>
                </div>  
                
                <!-- /.card-body -->
                <div class="row mg-t-20">
                    <div class="col-sm-2 mg-b-10"></div>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="update">
                        <button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
                        <button type="submit" class="btn btn-primary">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
<script src="{{ asset('/assets/ckeditorStandard/ckeditor.js') }}"></script>    
<script>
CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
} );
</script>
@include('ckfinder::setup')

<script type="text/javascript">
    var urlUpload = '{{ route("admin.news.create.upload")}}';
</script>
<script>
$(function() {
    $('.frm_select_2').select2();
    $(".select_type").on('change', function(){
        let id = $(this).val();
        if(id == 41){
            $("#box_show_services").show();
        }else{
            $("#box_show_services").hide();
        }
    }); 
});
</script>
@endsection
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12"> 
      <span class="breadcrumb-item active">Cấu hình chung</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Cấu hình chung</h4>
</div>
<div class="br-pagebody">
  <section class="listing-container">
        <div class="br-section-wrapper container-fluid">
      @include("admin.error")
            <form role="form" method="post" action="{{route('admin.configuration.update')}}" enctype="multipart/form-data">
              @csrf
                <div class="row">
                    <label class="col-sm-2 form-control-label">Tiêu đề trang(Meta Title)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_site_title', old('con_site_title') ? old('con_site_title') : ($configuration['con_site_title'] ?? null), ['class' => 'form-control','placeholder' => 'Meta Title']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Mô tả (MetaDescription)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('con_meta_description', old('con_meta_description') ? old('con_meta_description') : ($configuration['con_meta_description'] ?? null), ['class' => 'form-control','placeholder' => 'Meta Description', 'style' => 'height:75px']) !!}
                    </div>
                </div>                
                <div class="row">
                    <label class="col-sm-2 form-control-label">Từ khóa (Meta Keywords)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('con_meta_keywords', old('con_meta_keywords') ? old('con_meta_keywords') : ($configuration['con_meta_keywords'] ?? null), ['class' => 'form-control','placeholder' => 'Meta Description', 'style' => 'height:75px']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">CEO</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_ceo_name', old('con_ceo_name') ? old('con_ceo_name') : ($configuration['con_ceo_name'] ?? null), ['class' => 'form-control','placeholder' => 'CEO']) !!}
                    </div>
                </div>                
                <div class="row">
                    <label class="col-sm-2 form-control-label">Công ty</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_company', old('con_company') ? old('con_company') : ($configuration['con_company'] ?? null), ['class' => 'form-control','placeholder' => 'Tên công ty']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Lĩnh vực</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_business_field', old('con_business_field') ? old('con_business_field') : ($configuration['con_business_field'] ?? null), ['class' => 'form-control','placeholder' => '']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Ngày bắt đầu KD</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_date_establishment', old('con_date_establishment') ? old('con_date_establishment') : ($configuration['con_date_establishment'] ?? null), ['class' => 'form-control','placeholder' => 'Ngày bắt đầu KD']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Email đại diện</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_admin_email', old('con_admin_email') ? old('con_admin_email') : ($configuration['con_admin_email'] ?? null), ['class' => 'form-control','placeholder' => 'Email đại diện']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Hotline</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('con_hotline', old('con_hotline') ? old('con_hotline') : ($configuration['con_hotline'] ?? null), ['class' => 'form-control','placeholder' => 'Hotline']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Địa chỉ</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('con_address', old('con_address') ? old('con_address') : ($configuration['con_address'] ?? null), ['class' => 'form-control','placeholder' => 'Địa chỉ', 'style' => 'height:75px']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">FacebookPage</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('con_facebook', old('con_facebook') ? old('con_facebook') : ($configuration['con_facebook'] ?? null), ['class' => 'form-control','placeholder' => 'FacebookPage', 'style' => 'height:75px']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Youtube</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('con_youtube', old('con_youtube') ? old('con_youtube') : ($configuration['con_youtube'] ?? null), ['class' => 'form-control','placeholder' => 'Youtube', 'style' => 'height:75px']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Instagram</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('con_twitter', old('con_twitter') ? old('con_twitter') : ($configuration['con_twitter'] ?? null), ['class' => 'form-control','placeholder' => 'Tiktok', 'style' => 'height:75px']) !!}
                    </div>
                </div>

                <div class="row mg-t-20">
                  <div class="col-sm-2 mg-b-10"></div>
                  <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="execute">
                    <button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
                      <button type="submit" class="btn btn-primary">Cập nhật</button>
                  </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('JS_FOOTER')
<script type="text/javascript">
 
</script>
@endsection

@extends('admin.layouts')
@section('CONTAINER')
<div class="content-wrapper">
    <div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.staff.index')}}">User</a>
      <span class="breadcrumb-item active">Sửa</span>
    </nav>
</div>
<div class="br-pagetitle">
    <h4>Sửa user</h4>
</div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                	@include("admin.error")

                    <!-- jquery validation -->
                    <div class="card ">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="/admin/staff/{{ $admin_user['adm_id'] }}/edit" enctype="multipart/form-data">
                        	@csrf
                          
                            <div class="card-body">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label for="InputEmail">Email</label>
                                        {!! Form::text('adm_email', $admin_user['adm_email'] ? $admin_user['adm_email'] :  null, ['class' => 'form-control','placeholder' => 'Nhập email']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="InputUsername">Số điện thoại</label>
                                        {!! Form::text('adm_phone', $admin_user['adm_phone'] ? $admin_user['adm_phone'] :  null, ['class' => 'form-control','placeholder' => 'Nhập phone']) !!}
                                    </div>
                                 <!--    <div class="form-group">
    				                  	<label>Bộ phận</label>
    				                  	<select class="form-control" style="width: 100%;" name="adm_department">
    				                  		<option value="0">- Chọn -</option>
    				                  	  	<option value="1">IT</option>
    				                  	  	<option value="2">Kinh Doanh</option>
    				                  	  	<option value="3">Nhân sự</option>
    				                  	</select>
    				                </div> -->
    				                <div class="form-group">
    				                    <label for="exampleInputFile">Ảnh đại diện</label>
    				                    <div class="input-group">
    				                      <div class="custom-file">
    				                      	{!! Form::file('adm_avatar', ['class' => 'custom-file-input','id'=>'exampleInputFile']) !!}
    				                        <label class="custom-file-label" for="exampleInputFile">Chọn ảnh</label>
    				                      </div>
    				                      <div class="input-group-append">
    				                        <span class="input-group-text" id="">Upload</span>
    				                      </div>
    				                    </div>
    				                </div>
                                    <div class="form-group">
                                        <img src="/{{ App\Helper\ImageManager::getThumb($admin_user['adm_avatar'],'user_admin') }}">
                                    </div>
                                    
                                </div>

                                <div >
                                    <label class="col-sm-2 form-control-label">Phân quyền</label>
                                    <div >
                                        <label class="form-control-label" style="float: left;width: 100px;margin-left: 16px;">Check all</label>
                                        <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                                            <div class="custom-control custom-checkbox">
                                                {!! Form::checkbox('checkItAll', '1', false, ['class' => 'custom-control-input', 'id' => 'checkItAll', 'onclick' => 'checkAllRules()']) !!}
                                                <label class="col-sm-4 form-control-label custom-control-label" for="checkItAll"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                                        <table class="table table-striped">
                                            @foreach($allRole as $key => $value)
                                                <tr>
                                                    <td width="" nowrap="nowrap"><b>{{ $value['info']['label'] }}: </b></td>
                                                    <td valign="center">
                                                        @foreach($value['sub'] as $key_sub => $value_sub)
                                                            <div class="custom-control custom-checkbox" style="display: inline-block;vertical-align: middle; margin-right: 10px;">
                                                                @if(isset($admin_user['adm_role'][$key]) && isset($admin_user['adm_role'][$key][$key_sub]) && $admin_user['adm_role'][$key][$key_sub] == 1)
                                                                    <input class="custom-control-input ruleInput" type="checkbox" checked="checked" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
                                                                @else
                                                                    <input class="custom-control-input ruleInput" type="checkbox" name="{{$key}}_{{$key_sub}}" value="1" id="{{$key}}_{{$key_sub}}">
                                                                @endif
                                                                <label for="{{$key}}_{{$key_sub}}" class="custom-control-label" style=" font-weight: 400 !important; font-size: 14px;">{{ $value_sub['label'] }}</label>
                                                            </div>
                                                        @endforeach
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="">
                                    <label class="col-sm-2 form-control-label">Active</label>
                                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                                        <div class="custom-control custom-checkbox">
                                            {!! Form::checkbox('adm_active', '1', $admin_user['adm_active'] ? true : false, ['class' => 'custom-control-input', 'id' => 'adm_active']) !!}
                                            <label class="col-sm-4 form-control-label custom-control-label" for="adm_active">Kích hoạt</label>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Lưu</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('JS_FOOTER')
<script type="text/javascript"> 
    function checkAllRules(){         
        if($('#checkItAll').is(':checked') == true){
            $('.ruleInput').prop('checked', true)
        }else{
            $('.ruleInput').prop('checked', false)
        }
    }
</script>
@endsection
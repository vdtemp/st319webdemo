@extends('admin.layouts')
@section('CONTAINER')
<style type="text/css">
	.listing-container .fa-check-square, .fa-square{
		cursor: pointer;
	}
</style>
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.artist.index')}}">Nghệ sĩ</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách Nghệ sĩ</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="art_name" placeholder="Tên" type="text" value="{{ isset($params['art_name']) ? $params['art_name'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="lang_id" >
		              	<option value="-1">Ngôn ngữ</option>
		              	@foreach($arrLang as $key => $value)
		              		<option {{ (isset($params['lang_id']) && $params['lang_id'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="art_job" >
		              	<option value="-1">Nghề nghiệp</option>
		              	@foreach($arrJob as $key => $value)
		              		<option {{ (isset($params['art_job']) && $params['art_job'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
								<div class="col-lg">
		              <select  class="form-control" name="art_hot" >
		              	<option value="-1">-Chọn-</option>
		              	@foreach($arrHot as $key => $value)
		              		<option {{ (isset($params['art_hot']) && $params['art_hot'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="art_status" >
		              	<option value="-1">=Kích hoạt=</option>
		              	@foreach($arrStatus as $key => $value)
		              		<option {{ (isset($params['art_status']) && $params['art_status'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tên</td>
				          	<td class="text-center">Avatar</td> 
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Nổi bật</td> 
								 <td class="text-center">Sắp xếp</td> 
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($Artists as $keys => $Artist)
					      	<tr>
							   <td>{{ ++$keys + ($page-1)*15 }}</td>
					           <td>
					           		<p><b>{{ $Artist['art_name'] }}</b></p>
					           		<p>Người tạo: <b>{{ $arrUser[$Artist['admin_id']] ?? 'admin' }}</b></p>
					           		@isset ($arrUser[$Artist['edit_admin']])
					           			<p>Chỉnh sửa: <b>{{ $arrUser[$Artist['edit_admin']] ?? null }}</b></p>    
					           		@endisset					           		
					           </td>
					           <td class="text-center"><img width="100px" src="{{ $Artist['art_avatar'] }}"></td>					            
					           <td class="text-center">{{ date('H:i:s d/m/Y', $Artist['art_create_date'])  }}</td>
					           <td class="text-center">
					          		<i class="far {{($Artist['art_hot'] == 1) ? 'fa-check-square' : 'fa-square' }}" idata="{{$Artist['art_id']}}" onclick="updateHot($(this))"></i>
					           </td>
								  <td class="text-center">
								  		<input class="form-control text-center order_by" idata="{{$Artist['art_id']}}" style="width: 75px; margin: auto;" type="text" value="{{ $Artist['art_order'] }}">
								  </td>
					           <td class="text-center">
					           		<b>
					            	@if($Artist['art_active'] <= 0)
					            		Đợi duyệt
					            	@elseif($Artist['art_active'] == 2)
					            		Không duyệt
					            	@elseif($Artist['art_active'] == 1)
					            		Đã duyệt
					            	@endif
					            	</b>
					           </td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.artist.edit', ['id' => $Artist['art_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{!! Form::open(['url' => '/admin/artist/'. $Artist['art_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}					          		
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            {!! $Artists->appends($params)->render() !!}
        </div>
    </section>

</div>
@endsection

@section("JS_FOOTER")
<script type="text/javascript">
	$(function() {	 
		$(".order_by").on('change',function(){
			let id = $(this).attr('idata');
			let val = $(this).val();

			/*Update orrder*/
			$.ajax({
				url: '{{ route('api.change_order.artistChangeOrder') }}',
				type: 'POST',
				dataType: 'JSON',
				data: {id:id, value:val},
				success: function(data){
					if(data.status == 0){
						console.log('success');
					}
				}
			});			
		});
	}); 

	function updateHot(obj){
		let idata = obj.attr('idata');
		$.ajax({
        url: '{{route('api.set_hot.hotArtist')}}',
        type: "POST",
        dataType: 'json',
        data: {idata:idata},
        success: function(html){
         	if(obj.hasClass('fa-check-square')){
         		obj.removeClass('fa-check-square').addClass('fa-square')
         	}else if(obj.hasClass('fa-square')){
         		obj.removeClass('fa-square').addClass('fa-check-square')
         	}  
        }
    });
	}; 
</script>
@endsection


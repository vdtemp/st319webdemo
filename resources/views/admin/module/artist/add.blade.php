@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.artist.index')}}">Nghệ sĩ</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới nghệ sĩ</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="{{route('admin.artist.create')}}" enctype="multipart/form-data">
            	@csrf  
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Ngôn ngữ</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('lang_id', $arrLang, old('lang_id') ? old('lang_id') : null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Lĩnh vực</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('art_job', $arrJob, old('art_job') ? old('art_job') : null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Nghệ danh (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('art_name', old('art_name') ? old('art_name') : null, ['class' => 'form-control','placeholder' => 'Nhập tên nghệ sĩ']) !!}
	                </div>
                </div> 
                 <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Năm sinh</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_birthday', old('art_birthday') ? old('art_birthday') : null, ['class' => 'form-control','placeholder' => 'Nhập năm sinh']) !!}
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Debut</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_year_debut', old('art_year_debut') ? old('art_year_debut') : null, ['class' => 'form-control','placeholder' => 'Nhập năm debut']) !!}
                    </div>
                </div>             
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Spotify</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_spotify_url', old('art_spotify_url') ? old('art_spotify_url') : null, ['class' => 'form-control','placeholder' => 'Spotify']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Itunes</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_itunes_url', old('art_itunes_url') ? old('art_itunes_url') : null, ['class' => 'form-control','placeholder' => 'Itunes']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="art_avatar" id="art_avatar" value="{{ old('art_avatar') }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'artist', 'max_upload' => 25, 'multiple' => true ,'item_upload' => 'art_avatar', 'name_data' => 'picture_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả ngắn</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('art_intro', old('art_intro') ? old('art_intro') : null, ['class' => 'form-control','placeholder' => 'Nhập mô tả','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Giới thiệu (*)</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('art_description', old('art_description') ? old('art_description') : null, ['class' => 'form-control','id' => 'editor1','rows="3"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Giải thưởng (*)</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('art_awards', old('art_awards') ? old('art_awards') : null, ['class' => 'form-control','id' => 'editor2','rows="3"']) !!}
                    </div>
                </div>
                {{-- <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Thứ tự</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('art_order', old('art_order') ? old('art_order') : null, ['class' => 'form-control','placeholder' => '0']) !!}
	                </div>
                </div> --}}
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Video</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('art_link_video', old('art_link_video') ? old('art_link_video') : null, ['class' => 'form-control','placeholder' => 'Video youtube','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Background</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="art_background" id="art_background" value="{{ old('art_background') }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_background', 'type_upload' => 'artist', 'max_upload' => 1, 'multiple' => false ,'item_upload' => 'art_background', 'name_data' => 'art_background_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Small picture</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="art_small_pic" id="art_small_pic" value="{{ old('art_small_pic') }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_small_pic', 'type_upload' => 'artist', 'max_upload' => 1, 'multiple' => false ,'item_upload' => 'art_small_pic', 'name_data' => 'art_small_pic_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Facebook</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_facebook_link', old('art_facebook_link') ? old('art_facebook_link') : null, ['class' => 'form-control','placeholder' => 'Facebook']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Instagram</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_instagram_link', old('art_instagram_link') ? old('art_instagram_link') : null, ['class' => 'form-control','placeholder' => 'Instagram']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Youtube</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_youtube_link', old('art_youtube_link') ? old('art_youtube_link') : null, ['class' => 'form-control','placeholder' => 'Youtube']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Twitter</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_twitter_link', old('art_twitter_link') ? old('art_twitter_link') : null, ['class' => 'form-control','placeholder' => 'Twitter']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiktok</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('art_tiktok_link', old('art_tiktok_link') ? old('art_tiktok_link') : null, ['class' => 'form-control','placeholder' => 'Tiktok']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nổi bật</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('art_hot', '1', old('art_hot') ? true : false, ['class' => 'custom-control-input', 'id' => 'art_hot']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="art_hot"></label>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('art_active', '1', old('art_active') ? true : false, ['class' => 'custom-control-input', 'id' => 'art_active']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="art_active"></label>
                        </div>
                    </div>
                </div>                
                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="insert">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Thêm mới</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
    <script src="{{ asset('/assets/ckeditorStandard/ckeditor.js') }}"></script>    
    <script>
    CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
    } );
    CKEDITOR.replace( 'editor2', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
    } );
    </script>
    @include('ckfinder::setup')
    
    <script type="text/javascript">
        var urlUpload = '{{ route("admin.artist.create.upload")}}';
    </script>
    <script>
    $(function() {
        $('.frm_select_2').select2();
        $(".select_type").on('change', function(){
            let id = $(this).val();
            if(id == 41){
                $("#box_show_services").show();
            }else{
                $("#box_show_services").hide();
            }
        });
    });
    </script>
@endsection
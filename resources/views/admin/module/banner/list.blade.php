@extends('admin.layouts')
@section('CONTAINER')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12"> 
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách Banner</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="name" placeholder="Tiêu đề" type="text" value="{{ isset($params['name']) ? $params['name'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="ban_status" >
		              	<option value="-1">--Trạng thái--</option>
		              	@foreach ($arrStatus as $skey => $status)
		              		<option value="{{$skey}}" {{ $params['ban_status'] == $skey ? 'selected' : '' }}>{{$status}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="ban_type" >
		              	<option value="-1">--Loại--</option>
		              	@foreach ($arrType as $tkey => $type)
		              		<option value="{{$tkey}}" {{ $params['ban_type'] == $tkey ? 'selected' : '' }}>{{$type}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="ban_position" >
		              	<option value="">--Vị trí--</option>
		              	@foreach ($arrPosition as $pkey => $pos)
		              		<option value="{{$pkey}}" {{ $params['ban_position'] == $pkey ? 'selected' : '' }}>{{$pos}}</option>
		              	@endforeach
		              </select>
		            </div>
		           {{--  <div class="col-lg">
		              <input class="form-control" name="dateCreate" placeholder="Ngày tạo" type="text" value="">
		            </div> --}}
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tiêu đề</td>
				          	<td class="text-center">Ảnh/Video</td> 
				          	<td class="text-center">Vị trí</td>
				          	<td class="text-center">Loại</td> 
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Thứ tự</td>	
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($banners as $key => $banner)
					      	<tr>
							     <td>{{ ++$key }}</td>
					           <td>{{ $banner['ban_name'] }}</td>
					           <td class="text-center">
					           	<img src="{{ $banner['ban_picture'] }}" width="150">
					           </td> 
					           <td class="text-center">{{ $banner['ban_position'] }}</td>	
					           <td class="text-center">{{ $arrType[$banner['ban_type']] ?? '' }}</td> 
					           <td class="text-center">{{ date('d/m/Y H:i:s', $banner['ban_date_create'] ?? time() )  }}</td>
					           <td>
					           	<input class="form-control text-center ban_order" idata="{{ $banner['ban_id'] }}" style="width: 75px; margin: auto;" type="text" value="{{ $banner['ban_order'] }}">
					           </td>
					           <td class="text-center">
					            	{!! Form::select('ban_active', $arrStatus, old('ban_active') ? old('ban_active') : isset($banner) ? $banner['ban_active'] : null, ['class="form-control"', 'onchange="changeStatus('.$banner['ban_id'].', $(this) )"']); !!}
					           </td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.banner.edit', ['id' => $banner['ban_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{!! Form::open(['url' => '/admin/banner/'. $banner['ban_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            
        </div>
    </section>

</div>
@endsection

@section('JS_FOOTER')
	<script type="text/javascript">
		$(function() {	
			$(".ban_order").on('change',function(){
				let id = $(this).attr('idata');
				let val = $(this).val();

				/*Update orrder*/
				$.ajax({
					url: '{{ route('api.change_order.bannerChangeOrder') }}',
					type: 'POST',
					dataType: 'JSON',
					data: {id:id, value:val},
					success: function(data){
						if(data.status == 0){
							alert('Update thành công.');
						}
					}
				});			
			});
		}); 
		function changeStatus(id, obj) {
			let status = parseInt(obj.val());
			$.ajax({
				url: '{{ route('api.banners.changestatus') }}',
				type: 'POST',
				dataType: 'JSON',
				data: {id:id, status:status},
				success: function(data){
					if(data.status == 0){
						alert('Update thành công.');
					}
				}
			});
			
		}
	</script>
@endsection
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.positionBanner.index')}}">Danh sách</a>
      <span class="breadcrumb-item active">Thêm mới</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới vị trí banner</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="{{ env('APP_PREFIX') ?? '/'}}admin/positionBanner/create" enctype="multipart/form-data">
            	@csrf
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên vị trí</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
	                    {!! Form::text('banp_name', old('banp_name') ? old('banp_name') : null, ['class' => 'form-control','placeholder' => 'Nhập tên vị trí']) !!}
	                </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Mã vị trí</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('banp_position', old('banp_position') ? old('banp_position') : null, ['class' => 'form-control','placeholder' => 'Nhập mã vị trí']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('banp_description', old('banp_description') ? old('banp_description') : null, ['class' => 'form-control','placeholder' => 'Mô tả']) !!}
                    </div>
                </div> 
                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="insert">
	                	<button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
	                    <button type="submit" class="btn btn-primary">Thêm mới</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript">
        var urlUpload = '{{ route("admin.banner.create.upload")}}';
    </script>
    <script>
    $(function() {
        $('input[name="daterange"]').daterangepicker({
            timePicker: false,
            // startDate: moment().startOf('hour'),
            // endDate: moment().startOf('hour').add(24, 'hour'),
            locale: {
              format: 'DD/M/Y'
            }
        }, function(start, end, label) {
          
        });
    }); 
    </script>
@endsection
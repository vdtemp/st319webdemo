@extends('admin.layouts') 
@section('CONTAINER')


<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12"> 
      <span class="breadcrumb-item active">Sửa</span>
    </nav>
</div>
<div class="br-pagetitle">
    <h4>Sửa Banner</h4>
</div>

<div class="br-pagebody">
    <section class="listing-container">
        <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="POST" action="{{ route('admin.banner.edit.update',['id' => $banner['ban_id']]) }}" enctype="multipart/form-data">
                @csrf  
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Vị trí</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('ban_position', $arrPosition, old('ban_position') ? old('ban_position') : isset($banner) ? $banner['ban_position'] : null, ['class="form-control"']); !!}
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Loại</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('ban_type', $arrType, old('ban_type') ? old('ban_type') : isset($banner) ? $banner['ban_type'] : null, ['class="form-control"']); !!}
                    </div>
                </div>  
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tiêu đề (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('ban_name', old('ban_name') ? old('ban_name') : isset($banner) ? $banner['ban_name'] : null, ['class' => 'form-control','placeholder' => 'Nhập tiêu đề']) !!}
                    </div>
                </div>  
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Url target</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('ban_target_url', old('ban_target_url') ? old('ban_target_url') : isset($banner) ? $banner['ban_target_url'] : null, ['class' => 'form-control','placeholder' => 'Url target']) !!}
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile"></label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <img width="100%" src="{{$banner['ban_picture']}}" >
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">File</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="ban_picture" id="ban_picture" value="{{ old('ban_picture') }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'banners', 'max_upload' => 10, 'multiple' => true ,'item_upload' => 'ban_picture', 'name_data' => 'picture_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Order</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('ban_order', old('ban_order') ? old('ban_order') : isset($banner) ? $banner['ban_order'] : null, ['class' => 'form-control','placeholder' => '0']) !!}
                    </div>
                </div>  
                <div class="row">
                    <label class="col-sm-2 form-control-label">Active</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('ban_active', '1', isset($banner) ? $banner['ban_active'] : false, ['class' => 'custom-control-input', 'id' => 'ban_active']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="ban_active"></label>
                        </div>
                    </div>
                </div>  
                <!-- /.card-body -->
                <div class="row mg-t-20">
                    <div class="col-sm-2 mg-b-10"></div>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="update">
                        <button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> 
    <script type="text/javascript">
        var urlUpload = '{{ route("admin.banner.create.upload")}}';
    </script> 
@endsection
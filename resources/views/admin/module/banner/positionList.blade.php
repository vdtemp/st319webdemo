@extends('admin.layouts')
@section('CONTAINER')
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách vị trí banner</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="title" placeholder="Tiêu đề" type="text" value="{{ isset($params['title']) ? $params['title'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <input class="form-control" name="position" placeholder="Mã" type="text" value="{{ isset($params['title']) ? $params['title'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <input class="form-control" name="date" placeholder="Ngày tạo" type="text" value="{{ isset($params['title']) ? $params['title'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <input class="form-control" name="status" placeholder="Trạng thái" type="text" value="{{ isset($params['title']) ? $params['title'] : '' }}">
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Tìm kiếm</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tiêu đề</td>
				          	<td class="text-center">Mã</td>
				          	<td class="text-center">Mô tả</td>
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($Positions as $key => $Position)
					      	<tr>
							     <td>{{ ++$key }}</td>
					           <td>{{ $Position['banp_name'] }}</td>
					           <td class="text-center">
					           	 	{{ $Position['banp_position'] }}
					           </td>
					           <td class="text-center">{{ $Position['banp_description']  }}</td>
					           <td class="text-center">{{ date('d/m/Y', $Position['banp_date_create']) }}</td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.positionBanner.edit', ['id' => $Position['banp_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{!! Form::open(['url' => '/admin/positionBanner/'. $Position['banp_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            
        </div>
    </section>

</div>
@endsection

@section('JS_HEADER_MORE')

@endsection
@extends('admin.layouts')
@section('CONTAINER')

<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.product.index')}}">Album</a>
      <span class="breadcrumb-item active">Thêm nội dung album</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Thêm mới nội dung album</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
	    <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="{{route('admin.product.addMusic')}}" enctype="multipart/form-data">
            	@csrf   
                <div class="row">
                    <label class="col-sm-1 form-control-label" for="InputParent">Album</label>
                    <div class="col-sm-3 mg-b-10 mg-sm-b-10">
                        {!! Form::select('albv_album_id', $albums, old('albv_album_id') ? old('albv_album_id') : null, ['class="form-control frm_select_2"']); !!}
                    </div>
                    <div class="col-sm-3 mg-b-10 mg-sm-b-10">
                        <div class="btn btn-primary" id="addSong">Thêm bài hát</div>
                    </div>
                </div> 
                <div id="listSongToAdd">
                    <div class="row" id="box_add_song">
                        <label class="col-sm-1 form-control-label" for="InputUsername">Bài hát</label>
                        <div class="col-sm-3 mg-b-10 mg-sm-b-10">
    	                    {!! Form::text('albv_title[]', old('albv_title[]') ? old('albv_title[]') : null, ['class' => 'form-control','placeholder' => 'Nhập tên bài hát']) !!}
    	                </div>
                        <div class="col-sm-3 mg-b-10 mg-sm-b-10">
                            {!! Form::text('albv_itunes[]', old('albv_itunes[]') ? old('albv_itunes[]') : null, ['class' => 'form-control','placeholder' => 'Link itunes']) !!}
                        </div>
                        <div class="col-sm-3 mg-b-10 mg-sm-b-10">
                            {!! Form::text('albv_spotify[]', old('albv_spotify[]') ? old('albv_spotify[]') : null, ['class' => 'form-control','placeholder' => 'Link spotify']) !!}
                        </div>
                    </div> 
                </div>
                                
                <!-- /.card-body -->
                <div class="row mg-t-20">
                	<div class="col-sm-2 mg-b-10"></div>
                	<div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="addMusic">
	                	<button type="reset" class="btn btn-warning mg-r-20">Hủy</button>
	                    <button type="submit" class="btn btn-primary">Xác nhận</button>
	                </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
    <script src="{{ asset('/assets/ckeditorStandard/ckeditor.js') }}"></script>    
    <script>
    CKEDITOR.replace( 'editor1', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
    } );
    </script>
    @include('ckfinder::setup')
    
    <script type="text/javascript">
        var urlUpload = '{{ route("admin.product.create.upload")}}';
    </script>
    <script>
    $(function() {
        $('.frm_select_2').select2();
        $(".select_type").on('change', function(){
            let id = $(this).val();
            if(id == 41){
                $("#box_show_services").show();
            }else{
                $("#box_show_services").hide();
            }
        });

        $("#addSong").on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            let box_add_song = $('#box_add_song').clone();
            $("#listSongToAdd").append(box_add_song);
        });
    });
    </script>
@endsection
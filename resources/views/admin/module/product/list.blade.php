@extends('admin.layouts')
@section('CONTAINER')
<style type="text/css">
	.listing-container .fa-check-square, .fa-square{
		cursor: pointer;
	}
</style>
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.product.index')}}">Sản phẩm</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách Sản phẩm</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="pro_name" placeholder="Tên SP" type="text" value="{{ isset($params['pro_name']) ? $params['pro_name'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="pro_artist_id" >
		              	<option value="-1">Nghệ sĩ</option>
		              	@foreach($arrArtist as $key => $value)
		              		<option {{ (isset($params['pro_artist_id']) && $params['pro_artist_id'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="lang_id" >
		              	<option value="-1">Ngôn ngữ</option>
		              	@foreach($arrLang as $key => $value)
		              		<option {{ (isset($params['lang_id']) && $params['lang_id'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
								<div class="col-lg">
		              <select  class="form-control" name="pro_hot" >
		              	<option value="-1">Nổi bật</option>
		              	@foreach($arrHot as $key => $value)
		              		<option {{ (isset($params['pro_hot']) && $params['pro_hot'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="pro_status" >
		              	<option value="-1">Kích hoạt</option>
		              	@foreach($arrStatus as $key => $value)
		              		<option {{ (isset($params['pro_status']) && $params['pro_status'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>
		  	<div class="bd rounded table-responsive">
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tên SP</td>				          	
				          	<td class="text-center">Nghệ sĩ</td>
				          	<td class="text-center">Ảnh SP</td> 
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Nổi bật</td>
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($products as $keys => $product)
					      	<tr>
							   <td>{{ ++$keys + ($page-1)*15 }}</td>
					           <td>
					           		<p>{{ $product['pro_name'] }}</p>
					           		<p>Người tạo: <b>{{ $arrUser[$product['admin_id']] ?? 'admin' }}</b></p>
					           		<p>Thể loại:  <b>{{ $arrType[$product['pro_type']] ?? 'admin' }}</b></p>				           		
					           </td>					           
					           <td> 
					           		<a href="">{{ $arrArtist[$product['pro_artist_id']] ?? ''  }}</a>
		            			</td>
					           <td class="text-center"><img width="100px" src="{{ $product['pro_image'] }}"></td>					            
					           <td class="text-center">{{ date('H:i:s d/m/Y', $product['pro_created_date'])  }}</td>
					           <td class="text-center">
					          		<i class="far {{($product['pro_hot'] == 1) ? 'fa-check-square' : 'fa-square' }}" idata="{{$product['pro_id']}}" onclick="updateHot($(this))"></i>
					           </td>
					           <td class="text-center">
					           		<b>
					            	@if($product['pro_status'] <= 0)
					            		Đợi duyệt
					            	@elseif($product['pro_status'] == 2)
					            		Không duyệt
					            	@elseif($product['pro_status'] == 1)
					            		Đã duyệt
					            	@endif
					            	</b>
					           </td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.product.edit', ['id' => $product['pro_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{!! Form::open(['url' => '/admin/product/'. $product['pro_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}					          		
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            {!! $products->appends($params)->render() !!}
        </div>
    </section>

</div>
@endsection

@section("JS_FOOTER")
<script type="text/javascript">
	$(function() {	
		$(".order_by").on('change',function(){
			let id = $(this).attr('idata');
			let val = $(this).val();

			/*Update orrder*/
			$.ajax({
				url: '{{ route('api.change_order.newscategoryChangeOrder') }}',
				type: 'POST',
				dataType: 'JSON',
				data: {id:id, value:val},
				success: function(data){
					if(data.status == 0){
						console.log('success');
					}
				}
			});			
		});
	}); 

	function updateHot(obj){
		let idata = obj.attr('idata');
		$.ajax({
        url: '{{route('api.set_hot.hotNews')}}',
        type: "POST",
        dataType: 'json',
        data: {idata:idata},
        success: function(html){
         	if(obj.hasClass('fa-check-square')){
         		obj.removeClass('fa-check-square').addClass('fa-square')
         	}else if(obj.hasClass('fa-square')){
         		obj.removeClass('fa-square').addClass('fa-check-square')
         	}  
        }
    });
	}; 
</script>
@endsection


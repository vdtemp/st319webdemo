@extends('admin.layouts') 
@section('CONTAINER')


<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.product.index')}}">Sản phẩm</a>
      <span class="breadcrumb-item active">Sửa</span>
    </nav>
</div>
<div class="br-pagetitle">
    <h4>Sửa sản phẩm:  {{ $product['pro_name'] }}</h4>
</div>

<div class="br-pagebody">
    <section class="listing-container">
        <div class="br-section-wrapper container-fluid">
            @include("admin.error")
            <form role="form" method="post" action="/admin/product/{{ $product['pro_id'] }}/edit" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Ngôn ngữ</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('lang_id', $arrLang, old('lang_id') ? old('lang_id') : $product['lang_id'] ?? null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>   
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Thể loại</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('pro_type', $arrType, old('pro_type') ? old('pro_type') : $product['pro_type'] ?? null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputParent">Nghệ sĩ</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::select('pro_artist_id', $arrArtist, old('pro_artist_id') ? old('pro_artist_id') : $product['pro_artist_id'] ?? null, ['class="form-control frm_select_2"']); !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Tên sản phẩm (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('pro_name', old('pro_name') ? old('pro_name') : $product['pro_name'] ?? null, ['class' => 'form-control','placeholder' => 'Nhập tên sản phẩm']) !!}
                    </div>
                </div>         
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Năm xuất bản</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('pro_date', old('pro_date') ? old('pro_date') : $product['pro_date'] ?? null, ['class' => 'form-control']) !!}
                    </div>
                </div>                    
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="exampleInputFile">Ảnh đại diện (*)</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="pro_image" id="pro_image" value="{{ old('pro_image') ? old('pro_image') : $product['pro_image'] ?? null  }}">
                        @component('admin.upload', ['id_upload' => 'gallery_upload_file', 'type_upload' => 'product', 'max_upload' => 10, 'multiple' => true ,'item_upload' => 'pro_image', 'name_data' => 'picture_data'])
                        @endcomponent
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Link video</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('pro_link_video', old('pro_link_video') ? old('pro_link_video') : $product['pro_link_video'] ?? null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Mô tả ngắn</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('pro_content', old('pro_content') ? old('pro_content') : $product['pro_content'] ?? null, ['class' => 'form-control','placeholder' => 'Nhập từ mô tả','style="height:100px;"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nội dung (*)</label>
                    <div class="col-sm-10 mg-b-10 mg-sm-b-10">
                        {!! Form::textarea('pro_detail', old('pro_detail') ? old('pro_detail') : $product['pro_detail'] ?? null, ['class' => 'form-control','id' => 'editor1','rows="3"']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Giá bán</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('pro_price', old('pro_price') ? old('pro_price') : $product['pro_price'] ?? null, ['class' => 'form-control','placeholder' => '0']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label" for="InputUsername">Số lượng</label>
                     <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        {!! Form::text('pro_amount', old('pro_amount') ? old('pro_amount') : $product['pro_amount'] ?? null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Sale</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('pro_sale', '1', old('pro_sale') ? true : $product['pro_sale'] ?? false, ['class' => 'custom-control-input', 'id' => 'pro_sale']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="pro_sale"></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 form-control-label">Nổi bật</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('pro_hot', '1', $product['pro_hot'] ? true : false, ['class' => 'custom-control-input', 'id' => 'pro_hot']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="pro_hot"></label>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <label class="col-sm-2 form-control-label">Kích hoạt</label>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <div class="custom-control custom-checkbox">
                            {!! Form::checkbox('pro_status', '1', $product['pro_status'] ? true : false, ['class' => 'custom-control-input', 'id' => 'pro_status']) !!}
                            <label class="col-sm-4 form-control-label custom-control-label" for="pro_status"></label>
                        </div>
                    </div>
                </div>  
                
                <!-- /.card-body -->
                <div class="row mg-t-20">
                    <div class="col-sm-2 mg-b-10"></div>
                    <div class="col-sm-5 mg-b-10 mg-sm-b-10">
                        <input type="hidden" name="action" value="update">
                        <button type="reset" class="btn btn-warning mg-r-20">Làm lại</button>
                        <button type="submit" class="btn btn-primary">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

@endsection

@section('JS_FOOTER')
<script src="{{ asset('/assets/ckeditorStandard/ckeditor.js') }}"></script>    
<script>
CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
} );
</script>
@include('ckfinder::setup')

<script type="text/javascript">
    var urlUpload = '{{ route("admin.product.create.upload")}}';
</script>
<script>
$(function() {
    $('.frm_select_2').select2();
    $(".select_type").on('change', function(){
        let id = $(this).val();
        if(id == 41){
            $("#box_show_services").show();
        }else{
            $("#box_show_services").hide();
        }
    }); 
});
</script>
@endsection
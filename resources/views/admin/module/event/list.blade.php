@extends('admin.layouts')
@section('CONTAINER')
<style type="text/css">
	.listing-container .fa-check-square, .fa-square{
		cursor: pointer;
	}
</style>
<div class="br-pageheader">
    <nav class="breadcrumb pd-0 mg-0 tx-12">
      <a class="breadcrumb-item" href="{{route('admin.news.index')}}">Sự kiện</a>
      <span class="breadcrumb-item active">Danh sách</span>
    </nav>
</div>
<div class="br-pagetitle">
	<h4>Danh sách</h4>
</div>

<div class="br-pagebody">
	<section class="listing-container">
        <div class="br-section-wrapper container-fluid">
			<h6 class="br-section-label">Tìm kiếm</h6>
  	  		<form method="GET" action="{{ $page_filter_url }}">
  	  			<div class="row">
		            <div class="col-lg">
		              <input class="form-control" name="eve_title" placeholder="Tiêu đề" type="text" value="{{ isset($params['eve_title']) ? $params['eve_title'] : '' }}">
		            </div>
		            <div class="col-lg">
		              <select  class="form-control" name="lang_id" >
		              	<option value="-1">=Ngôn ngữ=</option>
		              	@foreach($arrLang as $key => $value)
		              		<option {{ (isset($params['lang_id']) && $params['lang_id'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div> 
		            <div class="col-lg">
		              <select  class="form-control" name="eve_status" >
		              	<option value="-1">=Trạng thái=</option>
		              	@foreach($arrStatus as $key => $value)
		              		<option {{ (isset($params['eve_status']) && $params['eve_status'] == $key) ? 'selected' : '' }}  value="{{$key}}">{{$value}}</option>
		              	@endforeach
		              </select>
		            </div>
		            <div class="col-lg-2 mg-t-10 mg-lg-t-0">
		            	<button class="btn btn-primary btn-block mg-b-10"><i class="fa fa-send mg-r-10"></i>Search</button>
		            </div>
		        </div>
  	    	</form>


		  	<div class="bd rounded table-responsive">	
		  	  	<table class="table table-bordered table-striped mg-b-0">
		  	  		<tbody class="">
		                <tr class="thead-colored thead-light">
				          	<td>ID</td>
				          	<td>Tiêu đề</td>
				          	<td class="text-center">Ảnh</td> 
				          	<td class="text-center">Ngày tạo</td>
				          	<td class="text-center">Thứ tự</td>
				          	<td class="text-center">Trạng thái</td>
				          	<td class="text-center">Action</td>
				        </tr>
				      	@foreach ($Events as $keys => $event)
					      	<tr>
							   <td>{{ ++$keys + ($page-1)*15 }}</td>
					           <td>
					           		<p>{{ $event['eve_title'] }}</p>
					           		<p>Người tạo: <b>{{ $arrUser[$event['admin_id']] ?? 'admin' }}</b></p>
					           </td>
					           <td class="text-center"><img width="100px" src="{{ $event['eve_banner'] }}"></td>					            
					           <td class="text-center">{{ date('H:i:s d/m/Y', $event['eve_created_date'])  }}</td>					           
					           <td>
		            			<input class="form-control text-center order_by" idata="{{$event['eve_id']}}" style="width: 75px; margin: auto;" type="text" value="{{ $event['eve_order'] }}">
		            			</td>
					           <td class="text-center">
					           		<b>
					            	@if($event['eve_status'] <= 0)
					            		Đợi duyệt
					            	@elseif($event['eve_status'] == 2)
					            		Không duyệt
					            	@elseif($event['eve_status'] == 1)
					            		Đã duyệt
					            	@endif
					            	</b>
					           </td>
					           <td class="text-center">
					           		<p><a href="{{ route('admin.event.edit', ['id' => $event['eve_id']])  }}"><i class="fas fa-edit"></i> Sửa</a></p>
					           		{!! Form::open(['url' => '/admin/event/'. $event['eve_id'].'/delete' , 'method' => 'GET', 'onsubmit' => 'return confirm("Bạn có chắc chắn muốn xóa bản ghi này?")']); !!}
						          		<button type="submit" class="button-delete" style="cursor: pointer;"><i class="fa fa-times-circle"></i> Xóa</button>
					          		{!! Form::close() !!}					          		
					           </td>
							</tr>
				      	@endforeach
		      		</tbody>
		  	  	</table>
            </div>
            <br/>
            {!! $Events->appends($params)->render() !!}
        </div>
    </section>

</div>
@endsection

@section("JS_FOOTER")
<script type="text/javascript">
	$(function() {	
		$(".order_by").on('change',function(){
			let id = $(this).attr('idata');
			let val = $(this).val();

			/*Update orrder*/
			$.ajax({
				url: '{{ route('api.change_order.eventChangeOrder') }}',
				type: 'POST',
				dataType: 'JSON',
				data: {id:id, value:val},
				success: function(data){
					if(data.status == 0){
						console.log('success');
					}
				}
			});			
		});
	});
</script>
@endsection


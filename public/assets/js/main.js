$(function() {
    var html = $('html, body'),
        navContainer = $('.nav-container'),
        navToggle = $('.nav-toggle'),
        navDropdownToggle = $('.has-dropdown');
    navToggle.on('click', function(e) {
        var $this = $(this);
        e.preventDefault();
        $this.toggleClass('is-active');
        navContainer.toggleClass('is-visible');
        html.toggleClass('nav-open')
    });
    navDropdownToggle.on('click', function() {
        var $this = $(this);
        $this.toggleClass('is-active').children('ul').toggleClass('is-visible')
    });
    navDropdownToggle.on('click', '*', function(e) { e.stopPropagation() })
})

if (window.innerWidth < 480) {
    $(window).scroll(function() {
        if ($(window).scrollTop() >= 90) {
            $('.sticky-header').addClass('clearfix');
        } else {
            $('.sticky-header').removeClass('clearfix');
        }
    });
}
function increaseValue(id) {
    var value = +document.getElementById(id).value || 0;
    value++;
    document.getElementById(id).value = value;
}

function decreaseValue(id) {
    var value = +document.getElementById(id).value || 0;
    value--;
    if (value < 1) {
        value = '1';
    }
    document.getElementById(id).value = value;
}
$('.input-value-button').change(function() {
    if ((this.value) < 1 || (this.value == '')) {
        $(this).val(1);
    }
});
$(document).ready(function() {
    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
    if (window.innerWidth > 991) {
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 90) {
                $('.sticky-header').addClass('fixed');
                $('.clearfix-header').addClass('fixed');
            } else {
                $('.sticky-header').removeClass('fixed');
                $('.clearfix-header').removeClass('fixed');
            }
        });
    }
    $(window).scroll(function() {
        if ($(window).scrollTop() >= 200) {
            $('#go_top').show();
        } else {
            $('#go_top').hide();
        }
    });
});
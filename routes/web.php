<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@login')->name('userLogin');
Route::post('/userActLogin', 'LoginController@actLogin')->name('userActLogin');

Route::middleware(['auth.user'])->group(function () {
  Route::get('/', 'HomeController@index')->name('home');

  Route::get('/intro', 'HomeController@introduction')->name('intro');

  Route::get('/article', 'CampaignController@index')->name('campaign');
  Route::get('/article/{id}-{alias?}', 'CampaignController@detail')->name('campaignDetail');

  Route::get('/artist', 'ArtistController@index')->name('artist');
  Route::get('/artist/{id}-{alias?}', 'ArtistController@detail')->name('artistDetail');

  Route::get('/event', 'EventController@index')->name('event');

  Route::get('/news', 'NewsController@index')->name('news');
  Route::get('/news/{id}-{alias?}', 'NewsController@detail')->name('newsDetail');

  Route::get('/album', 'AlbumController@index')->name('album');
  Route::get('/single/{id?}', 'AlbumController@albumDetail')->name('albumDetail');

  Route::get('/store', 'StoreController@index')->name('store');
  Route::get('/store/{id?}', 'StoreController@product')->name('storeProduct');
  Route::post('/addtocart', 'StoreController@addToCart')->name('addToCart');
  Route::post('/submitOrder', 'StoreController@submitOrder')->name('submitOrder');

  Route::get('/order', 'StoreController@order')->name('order');
  Route::get('/orderSuccess', 'StoreController@orderSuccess')->name('orderSuccess');
    
});

Route::get('404',function(){
  return view('404');
});

Route::get('access_denied',function(){
  return view('pageError');
})->name('access_denied');

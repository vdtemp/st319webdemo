<?
Route::name('admin.')->group(function () {
  Route::get('/', 'Admin\AuthController@index')->name('login_fr');
  Route::post('/login', 'Admin\AuthController@login')->name('do_login');
  Route::get('/logout', 'Admin\AuthController@logout')->name('do_logout');

  Route::post('/getCity', 'Admin\AdminLocationController@getCity')->name('getCity');
  Route::post('/getPosition', 'Admin\AdminBannerController@getPosition')->name('getBannerPosition');
});

Route::name('admin.')->middleware('auth.admin')->group(function () { 
	Route::get('/dashboard', 'Admin\AdminHomeController@index')->name('dashboard');
	Route::resource('user', 'Admin\AdminUserController');

	Route::resource('category', 'Admin\AdminCategoryController');
	Route::post('category/ajax-load-parent', 'Admin\AdminCategoryController@loadParent');

	Route::name('staff.')->prefix('staff')->group(function () {
		Route::get('/', 'Admin\AdminUserController@index')->name('index');
		Route::any('/create', 'Admin\AdminUserController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminUserController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminUserController@update')->name('edit.update')->where('id', '[0-9]+');
		Route::post('/{id?}/delete', 'Admin\AdminUserController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::post('/upload', 'Admin\AdminUserController@upload')->name('create.upload');
  }); 

	Route::name('configuration.')->prefix('configuration')->group(function () {
		Route::get('/', 'Admin\AdminConfigController@index')->name('index');
		Route::post('/update', 'Admin\AdminConfigController@update')->name('update');
	});

	Route::name('positionBanner.')->prefix('positionBanner')->group(function () {
		Route::get('/', 'Admin\AdminBannerPositionController@index')->name('index');
		Route::any('/create', 'Admin\AdminBannerPositionController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminBannerPositionController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminBannerPositionController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::any('/{id?}/delete', 'Admin\AdminBannerPositionController@destroy')->name('delete')->where('id', '[0-9]+');
	}); 

	Route::name('banner.')->prefix('banner')->group(function () {
		Route::get('/', 'Admin\AdminBannerController@index')->name('index');
		Route::any('/create', 'Admin\AdminBannerController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminBannerController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminBannerController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::get('/{id?}/delete', 'Admin\AdminBannerController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::post('/upload', 'Admin\AdminBannerController@upload')->name('create.upload');
	}); 

	Route::name('news.')->prefix('news')->group(function () {
		Route::get('/', 'Admin\AdminNewsController@index')->name('index');
		Route::any('/create', 'Admin\AdminNewsController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminNewsController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminNewsController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::get('/{id?}/delete/{page?}', 'Admin\AdminNewsController@destroy')->name('delete')->where('id', '[0-9]+')->where('page', '[0-9]+');
		Route::post('/upload', 'Admin\AdminNewsController@upload')->name('create.upload');
	});

	Route::name('campaign.')->prefix('campaign')->group(function () {
		Route::get('/', 'Admin\AdminCampaignController@index')->name('index');
		Route::any('/create', 'Admin\AdminCampaignController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminCampaignController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminCampaignController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::get('/{id?}/delete', 'Admin\AdminCampaignController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::post('/upload', 'Admin\AdminCampaignController@upload')->name('create.upload');
	}); 

	Route::name('event.')->prefix('event')->group(function () {
		Route::get('/', 'Admin\AdminEventController@index')->name('index');
		Route::any('/create', 'Admin\AdminEventController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminEventController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminEventController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::get('/{id?}/delete', 'Admin\AdminEventController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::post('/upload', 'Admin\AdminEventController@upload')->name('create.upload');
	}); 

	Route::name('artist.')->prefix('artist')->group(function () {
		Route::get('/', 'Admin\AdminArtistController@index')->name('index');
		Route::any('/create', 'Admin\AdminArtistController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminArtistController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminArtistController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::get('/{id?}/delete', 'Admin\AdminArtistController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::post('/upload', 'Admin\AdminArtistController@upload')->name('create.upload');
	});

	Route::name('product.')->prefix('product')->group(function () {
		Route::get('/', 'Admin\AdminProductController@index')->name('index');
		Route::any('/create', 'Admin\AdminProductController@create')->name('create');
		Route::get('/{id?}/edit', 'Admin\AdminProductController@edit')->name('edit')->where('id', '[0-9]+');
		Route::post('/{id?}/edit', 'Admin\AdminProductController@edit')->name('edit.update')->where('id', '[0-9]+');
		Route::post('/{id?}/delete', 'Admin\AdminProductController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::post('/upload', 'Admin\AdminProductController@upload')->name('create.upload');

		Route::any('/addMusic', 'Admin\AdminProductController@addMusic')->name('addMusic');
	});

	Route::name('order.')->prefix('order')->group(function () {
		Route::get('/', 'Admin\AdminOrdersController@index')->name('index'); 
		Route::get('/{id?}/delete', 'Admin\AdminOrdersController@destroy')->name('delete')->where('id', '[0-9]+');
		Route::get('/{id?}/confirm', 'Admin\AdminOrdersController@confirm')->name('confirm')->where('id', '[0-9]+');
	});

});
?>
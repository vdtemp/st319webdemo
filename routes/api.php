<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::name('api.')->group(function () {
	Route::name('banners.')->prefix('banners')->group(function () {    
		Route::post('/changestatus', 'Admin\AdminBannerController@changestatus')->name('changestatus'); 
	});

	Route::name('set_hot.')->prefix('set_hot')->group(function () {    
		Route::post('/hotNews', 'Admin\AdminNewsController@setHotNews')->name('hotNews');
		Route::post('/hotCampaign', 'Admin\AdminCampaignController@setHotCampaign')->name('HotCampaign');
		Route::post('/hotArtist', 'Admin\AdminArtistController@setHotArtist')->name('hotArtist');
	});

	Route::name('change_order.')->prefix('change_order')->group(function () {    
		Route::post('/news_change_order', 'Admin\AdminNewsController@changeOrder')->name('newscategoryChangeOrder');		
		Route::post('/banner_change_order', 'Admin\AdminBannerController@changeOrder')->name('bannerChangeOrder');
		Route::post('/event_change_order', 'Admin\AdminEventController@changeOrder')->name('eventChangeOrder');
		Route::post('/artist_change_order', 'Admin\AdminArtistController@changeOrder')->name('artistChangeOrder');
	});
})	;

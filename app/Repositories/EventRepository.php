<?php

namespace App\Repositories;
use App\Models\Event; 
use Request;

class EventRepository
{
	function __construct()
	{
		
	}

	public function getEvents($data = [])
	{
		$Event = Event::where('eve_status', 1);
		
		if(isset($data['hot']) && $data['hot'] == 1){
			$Event = $Event->where('eve_hot', $data['hot']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$Event = $Event->offset($data['page'] * $data['perPage'])->limit($data['perPage']);
		}

		$result = $Event->get()->toArray();
		return $result;
	}

	public function paging($data=[])
	{
		$Event = Event::where('eve_status', 1); 

		if(isset($data['hot']) && $data['hot'] == 1){
			$Event = $Event->where('eve_hot', $data['hot']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$totalRecord = $Event->count();	 
			$totalPage   = ($totalRecord % $data['perPage'] == 0) ? floor($totalRecord / $data['perPage']) : floor($totalRecord / $data['perPage']) + 1; 
		}

		$result = [
			'totalRecord' => $totalRecord,
			'totalPage'   => intval($totalPage) , 
			'perPage'     => $data['perPage'],
			'page'        => $data['page'],
		]; 
		return $result;
	}

	public function getEventDetail($id, $other = true){
		$Event = Event::where('eve_status', 1)
						->where('eve_id', intval($id))
						->first();

		$otherEvent = [];
		if($other){
			$otherEvent = Event::where('eve_status', 1)
								->where('eve_id', '<>', $id)
								->limit(5)
								->get()->toArray();
		}

		$return = [
			'EventInfo'  => $Event,
			'otherEvent' => $otherEvent,
		];

		if($Event){
			return $return;
		}else{
			return false;
		}
	}
}
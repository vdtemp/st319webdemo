<?php

namespace App\Repositories;
use App\Models\NewsMulti; 
use Request;

class NewsRepository
{
	function __construct()
	{
		
	}

	public function getNews($data = [], $with = '')
	{
		$NewsMulti = NewsMulti::where('new_active', 1);
		
		if($with != ''){
			$NewsMulti = $NewsMulti->with($with);
		}

		if(isset($data['type']) && $data['type'] > 0){
			$NewsMulti = $NewsMulti->where('new_type', intval($data['type']));
		}

		if(isset($data['hot']) && $data['hot'] == 1){
			$NewsMulti = $NewsMulti->where('new_hot', $data['hot']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$NewsMulti = $NewsMulti->offset($data['page'] * $data['perPage'])->limit($data['perPage']);
		}

		$result = $NewsMulti->get()->toArray();
		return $result;
	}

	public function paging($data=[])
	{
		$NewsMulti = NewsMulti::where('new_active', 1); 

		if(isset($data['type']) && $data['type'] > 0){
			$NewsMulti = $NewsMulti->where('new_type', intval($data['type']));
		}

		if(isset($data['hot']) && $data['hot'] == 1){
			$NewsMulti = $NewsMulti->where('new_hot', $data['hot']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$totalRecord = $NewsMulti->count();	 
			$totalPage   = ($totalRecord % $data['perPage'] == 0) ? floor($totalRecord / $data['perPage']) : floor($totalRecord / $data['perPage']) + 1; 
		}

		$result = [
			'totalRecord' => $totalRecord,
			'totalPage'   => $totalPage, 
			'perPage'     => $data['perPage'],
			'page'        => $data['page'],
		]; 
		return $result;
	}

	public function getNewsDetail($id, $other = true){
		$NewsMulti = NewsMulti::where('new_active', 1)
						->where('new_id', intval($id))
						->with('images')
						->first();

		$otherNews = [];
		if($other){
			$otherNews = NewsMulti::where('new_active', 1)
							->where('new_id', '<>', $id)
							->limit(5)
							->get()->toArray();
		}

		$return = [
			'NewsInfo' 	=> $NewsMulti,
			'otherNews' => $otherNews,
		];

		if($NewsMulti){
			return $return;
		}else{
			return false;
		}
	}
}
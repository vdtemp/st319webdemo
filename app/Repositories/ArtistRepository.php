<?php

namespace App\Repositories;
use App\Models\Artist;
use App\Models\ArtistGallery;
use Request;

class ArtistRepository
{	
	protected $Artist;
	protected $ArtistGallery;

	function __construct()
	{
		
	}

	public function getArtists($data = [])
	{
		$Artist = Artist::where('art_active', 1); 
		
		if(isset($data['hot']) && $data['hot'] == 1){
			$Artist = $Artist->where('art_hot', $data['hot']);
		}

		if(isset($data['job'])){
			$Artist = $Artist->where('art_job', $data['job']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$Artist = $Artist->offset($data['page'] * $data['perPage'])->limit($data['perPage']);
		}
		$result = $Artist->orderBy('art_order', 'desc')->get()->toArray();
		return $result;
	}

	public function getArtistDetail($id)
	{
		$Artist = Artist::where('art_active', 1)->with(['images', 'products'])->where('art_id', $id)->first();
		return $Artist;
	}
}
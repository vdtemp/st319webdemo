<?php

namespace App\Repositories;
use App\Models\Banner;
use App\Models\BannerPosition;
use Request;

class BannerRepository
{
	function __construct()
	{
		
	}

	public function getBanners($data = [])
	{
		$banners = Banner::where('ban_active', 1);
		
		if(isset($data['pos']) && $data['pos'] != ''){
			$banners = $banners->where('ban_position', $data['pos']);
		}

		if(isset($data['limit']) && $data['limit'] > 0){
			$banners = $banners->limit($data['limit']);
		}

		$result = $banners->get()->toArray();
		return $result;
	}
}
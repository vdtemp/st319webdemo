<?php

namespace App\Repositories;
use App\Models\AdminUser;
use Request;

class AdminUserRepository
{
    protected $adminUser;

    public static $adminNotEdit = ['admin', 'administrator'];
    public function __construct(AdminUser $adminUser)
    {
        $this->adminUser = $adminUser;
    }

    public function getByUserName($user_name){
		$info	= $this->adminUser->where('adm_loginname', $user_name)->where('adm_active', 1)->first();
		if($info){
			return $info;
		}
		return false;
    }

    public function checkInfoLogin($user_name, $password){
    	$dataUser = $this->getByUserName($user_name);
    	if($dataUser){
    		if($dataUser->adm_id > 0 && (md5($password) == $dataUser->adm_password)){
    			self::setLogged($user_name, $password);
    			return true;
    		}else{
    			return false;
    		}
    	}
    }

    public static function isLogged(){
    	// Not logged redirect login
  		$value 		= Request::session()->get('ADMIN_LOGGED');
  		$username	= Request::session()->get('ADMIN_USER_NAME');
  		$token 		= Request::session()->get('ADMIN_TOKEN');
  		if($value !== null && $username !== null && $token !== null){
  			return true;
  		}
  		return false;
    }

    public static function setLogged($user_name, $password){
    	Request::session()->put('ADMIN_LOGGED', true);
	    Request::session()->put('ADMIN_USER_NAME', $user_name);
	    Request::session()->put('ADMIN_USER_PASSWORD', md5($password));
	    Request::session()->put('ADMIN_TOKEN', $password);
    }

    public function getInfoLogged(){

		$username  = Request::session()->get('ADMIN_USER_NAME');
		$info      = $this->getByUserName($username);
		if($info){
			// Kiểm tra password có đúng không
			$password 	= Request::session()->get('ADMIN_USER_PASSWORD');
			if($password == $info->adm_password) return $info;
		}

		return false;
    }

    public function setLogout(){
    	Request::session()->forget(['ADMIN_LOGGED', 'ADMIN_USER_NAME', 'ADMIN_USER_PASSWORD', 'ADMIN_TOKEN']);
    }

    /**
     * [getMenuLogged description]
     * @return [type] [description]
     */
    public static function getMenuLogged(){
		$dataReturn		= [];
		$infoStaff		= app()->ADMIN_INFO;
		$allMenuRole	= app()->ADMIN_ROLE;

		foreach($allMenuRole as $key => $value){
			if(isset($value['info']['is_menu']) && $value['info']['is_menu'] == 1){
				$dataReturn[$key] 	= $value;
				foreach($value['sub'] as $key_sub => $value_sub){
					// Xóa bỏ những item không phải là menu
					if(!isset($value_sub['is_menu']) || $value_sub['is_menu'] != 1){
						unset($dataReturn[$key]['sub'][$key_sub]);
					}
				}
			}
		}

    	return $dataReturn;
    }

    public static function routeNotCheckPermission(){
    	return ['admin.dashboard.index', 'admin.profile.changepass'];
    }

    public static function getAllMenuRoleAdmin(){
		$arrayMenu 	= array(
							'configuration'	=> array( 
								'info' => ['label' 	=> "Cấu hình chung", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "configuration", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Cấu hình', 'route' => 'index'),
										'update'  => array('is_menu' => 0, 'check_pers' => 0, 'label' => 'Update', 'route' => 'update'),										
										)
							),	
							'staff'	=> array( 
								'info' => ['label' 	=> "Quản lý người dùng", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "staff", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'positionBanner'	=> array( 
								'info' => ['label' 	=> "Vị trí banner", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "positionBanner", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'banner'	=> array( 
								'info' => ['label' 	=> "Quản lý banner", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "banner", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'news'	=> array( 
								'info' => ['label' 	=> "Quản lý Tin tức", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "news", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'campaign'	=> array( 
								'info' => ['label' 	=> "Quản lý Chiến dịch", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "campaign", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'event'	=> array( 
								'info' => ['label' 	=> "Quản lý Sự kiện", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "event", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'artist'	=> array( 
								'info' => ['label' 	=> "Quản lý Nghệ sĩ", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "artist", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete')
										)
							),
							'product'	=> array( 
								'info' => ['label' 	=> "Quản lý Sản phẩm", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "product", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'),
										'create' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới', 'route' => 'create'),
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Xóa', 'route' => 'delete'),
										'addMusic' => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Thêm mới bài hát', 'route' => 'addMusic'),
										)
							),
							'order'	=> array( 
								'info' => ['label' 	=> "Quản lý Đơn hàng", 'is_menu'	=> 1, 'check_pers'=> 1, 'prefix' => "order", "icon" => "ion-ios-photos-outline"],
								'sub'	=> array(
										'index'  => array('is_menu' => 1, 'check_pers' => 1, 'label' => 'Danh sách', 'route' => 'index'), 
										'edit'   => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Chỉnh sửa', 'route' => 'add'),
										'delete' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Hủy', 'route' => 'delete'),
										'confirm' => array('is_menu' => 0, 'check_pers' => 1, 'label' => 'Duyệt', 'route' => 'confirm')
										)
							),
							'dashboard'	=> array(
								'info' => ['label' => "Dashboard", 'is_menu' => 0, 'check_pers'=> 0, 'prefix' => "dashboard", "icon" => "ion-ios-photos-outline"],
								'sub' 		=> array(
													'dashboard'	=> array('key' => '', 'is_menu' => 0, 'check_pers' => 0, 'label' => 'Dashboard', 'route' => 'dashboard')
													)
												), 
							);

		return $arrayMenu;
	}

	public static function getAllMenuAdmin(){
		$allMenuRole 	= self::getAllMenuRoleAdmin();
		$arrayReturn 	= array();
		foreach($allMenuRole as $key => $value){
			if(isset($value['is_menu']) && $value['is_menu'] == 1){
				$arrayReturn[$key] 	= $value;
				foreach($value['sub'] as $key_sub => $value_sub){
					// Xóa bỏ những item không phải là menu
					if(!isset($value_sub['is_menu']) || $value_sub['is_menu'] != 1){
						unset($arrayReturn[$key]['sub'][$key_sub]);
					}
				}
			}
		}

		return $arrayReturn;
	}

	public static function getPermission(){

		$staffRole 	= [];
		$infoStaff	= app()->ADMIN_INFO;

		$roleSso 	= (isset($infoStaff->adm_role) && $infoStaff->adm_role) ? json_decode($infoStaff->adm_role, 1) : [];

		// Lấy quyền từ api
		// $roleSso = [
		// 			"mtype"				=> ["view" => "view", "add" => "add", "edit" => "edit", "delete" => "delete", "censorship" => "censorship"],
		// 			"mlevel"			=> ["view" => "view", "add" => "add", "edit" => "edit", "delete" => "delete", "censorship" => "censorship"],
		// 			"question_sms"		=> ["view" => "view", "add" => "add", "edit" => "edit", "delete" => "delete", "censorship" => "censorship"],
		// 			"question_practice"	=> ["view" => "view", "add" => "add", "edit" => "edit", "delete" => "delete", "censorship" => "censorship"],
		// 			"question_compete"	=> ["view" => "view", "add" => "add", "edit" => "edit", "delete" => "delete", "censorship" => "censorship"],
		// 			];

		// Lấy toàn bộ quyền có thể có để so sánh
		$allRole	= AdminUserRepository::getAllMenuRoleAdmin();

		if($infoStaff->adm_isadmin == 1) return $allRole;

		$dataRole = [];
		// Lặp trả kết quả phù hợp
		if($roleSso){
			foreach ($roleSso as $key => $value) {
				if(isset($allRole[$key])){
					$dataRole[$key]['info']	= $allRole[$key]['info'];
					$dataRole[$key]['sub']	= [];
					// Bóc tiếp để lấy list quyền của module này
					if($value){
						foreach ($value as $key_sub => $value_sub) {
							$key_sub 	= trim($key_sub);
							if(isset($allRole[$key]['sub'][$key_sub])){
								$dataRole[$key]['sub'][$key_sub] = $allRole[$key]['sub'][$key_sub];
							}
	    				}
					}
				}
			}
		}

		// Lặp tiếp để lấy các quyền không check quyền
		foreach ($allRole as $key => $value) {
			if(isset($dataRole[$key])){
				$staffRole[$key] 	= $dataRole[$key];
			}else{
				$staffRole[$key] = [];
				if($value['info']['check_pers'] != 1){
					// Lặp để lấy các quyền con không check quyền
					if(isset($value['sub'])){
						foreach ($value['sub'] as $key_sub => $value_sub) {
							if($value_sub['check_pers'] != 1) $staffRole[$key][$key_sub] = $value_sub;
						}
					}
				}
			}
		}

		return $staffRole;
	}

	public static function checkPermission($module, $per = ''){

    	if(empty($module)) return false;
    	$infoStaff		= app()->ADMIN_INFO;
    	$staffRole 		= app()->ADMIN_ROLE;

    	// Là nhân viên thường thì lặp để check
    	// Check quyền module
		if(isset($staffRole[$module])){
			// Check quyền action
			if(!empty($per)){
				// Không thuộc mục nào thì phắn
				if(isset($staffRole[$module]['sub'][$per])) return true;
			}else{
				return true;
			}
		}

    	return false;
    }
}

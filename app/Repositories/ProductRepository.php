<?php

namespace App\Repositories;
use App\Models\Product; 
use App\Models\ProductImage; 
use App\Models\AlbumVideos; 
use Request;

class ProductRepository
{
	function __construct()
	{
		
	}

	public function getProducts($data = [], $with='')
	{
		$Product = Product::where('pro_status', 1);
		
		if(isset($data['hot']) && $data['hot'] == 1){
			$Product = $Product->where('pro_hot', $data['hot']);
		}

		if(isset($data['artist_id']) && $data['artist_id'] > 0){
			$Product = $Product->where('pro_artist_id', $data['artist_id']);
		}

		if(isset($data['type'])  && $data['type'] > 0){
			$Product = $Product->where('pro_type', $data['type']);
		}

		if(isset($data['sale'])  && $data['sale'] >= 0){
			$Product = $Product->where('pro_sale', $data['sale']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$Product = $Product->offset($data['page'] * $data['perPage'])->limit($data['perPage']);
		}

		if($with != ''){
			$result = $Product->with($with);	
		}
		
		$result = $Product->get()->toArray();
		return $result;
	}

	public function productDetail($id){
		$Product = Product::where('pro_status', 1)->where('pro_id',intval($id))->with(['images'])->first();
		return $Product;
	}

	public function getAlbumVideos($albumId){
		$result = AlbumVideos::where('albv_album_id', $albumId)->where('albv_active', 1)->select(['albv_title', 'albv_itunes', 'albv_spotify'])->get()->toArray();
		return $result;
	}
}
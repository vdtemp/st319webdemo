<?php

namespace App\Repositories;
use App\Models\Campaign; 
use Request;

class CampaignRepository
{
	function __construct()
	{
		
	}

	public function getCampaigns($data = [])
	{
		$Campaign = Campaign::where('camp_status', 1);
		
		if(isset($data['hot']) && $data['hot'] == 1){
			$Campaign = $Campaign->where('camp_hot', $data['hot']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$Campaign = $Campaign->offset($data['page'] * $data['perPage'])->limit($data['perPage']);
		}

		$result = $Campaign->get()->toArray();
		return $result;
	}

	public function paging($data=[])
	{
		$Campaign = Campaign::where('camp_status', 1); 

		if(isset($data['hot']) && $data['hot'] == 1){
			$Campaign = $Campaign->where('camp_hot', $data['hot']);
		}

		if(isset($data['page']) && isset($data['perPage'])){
			$totalRecord = $Campaign->count();	 
			$totalPage   = ($totalRecord % $data['perPage'] == 0) ? floor($totalRecord / $data['perPage']) : floor($totalRecord / $data['perPage']) + 1; 
		}

		$result = [
			'totalRecord' => $totalRecord,
			'totalPage'   => intval($totalPage) , 
			'perPage'     => $data['perPage'],
			'page'        => $data['page'],
		]; 
		return $result;
	}

	public function getCampaignDetail($id, $other = true){
		$Campaign = Campaign::where('camp_status', 1)
						->where('camp_id', intval($id))
						->first();

		$otherCampaign = [];
		if($other){
			$otherCampaign = Campaign::where('camp_status', 1)
								->where('camp_id', '<>', $id)
								->limit(5)
								->get()->toArray();
		}

		$return = [
			'CampaignInfo'  => $Campaign,
			'otherCampaign' => $otherCampaign,
		];

		if($Campaign){
			return $return;
		}else{
			return false;
		}
	}
}
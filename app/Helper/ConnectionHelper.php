<?php
namespace App\Helper;

class ConnectionHelper
{

	const DB_SERVER 	= 'slaves';

    public static function isAvailableConnection($connection)
    {

        $host = $connection['host'] ?? $connection['read']['host'] ?? null;
        if (!$host) {
            return false;
        }

        return is_array($connection)
            && isset($connection['driver'])
            && (
                (isset($connection['host']) && $connection['host'])
                || (
                    isset($connection['read']['host'])
                    && $connection['read']['host']
                    && isset($connection['write']['host'])
                    && $connection['write']['host']
                )
            );
        
    }

    public static function getAvailableConnections($connections)
    {
        $result = [];
        foreach ($connections as $key => $connection) {
            if (!static::isAvailableConnection($connection)) {
                continue;
            }
            $result[$key] = $connection;
        }

        return array_filter($result);
    }

    public static function getRandomConnection($connections)
    {
        if (empty($connections)) {
            return '';
        }

        $connections	=static::getAvailableConnections($connections);

        if (empty($connections)) {
            return '';
        }

        // Lấy random ra 1 cái
		$rand		= array_rand($connections);

      return $connections[$rand];
    }

	public static function scopeSelectFieldListing($query)
	{
		if(is_array(static::FIELD_FOR_LISTING) && !empty(static::FIELD_FOR_LISTING))
		{
			$query->select(static::FIELD_FOR_LISTING);
		}
		return $query;
	}
}
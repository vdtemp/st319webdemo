<?php
namespace App\Http\View;

use Illuminate\View\View;

class AdminInfoComposer{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(){

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $infoAdmin  = app()->ADMIN_INFO;
        $menuAdmin  = app()->ADMIN_MENU;
        $view->with(['infoAdmin' => $infoAdmin, 'menuAdmin' => $menuAdmin]);
    }
}
<?php
namespace App\Http\View;

use Illuminate\View\View;
use Illuminate\Support\Facades\Route;
use App\Models\Configuration;
use App\Models\Artist;

class MenuComposer{
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $webInfo = Configuration::where('con_id', 1)->first();
        $arrLang = [ 1 => 'VIE', 2 => 'ENG'];

        $Artists =  Artist::where('art_active', 1)->where('art_hot', 1)->orderBy('art_order', 'desc')->get();
        $arrArtist = [];
        foreach ($Artists as $artist) {
            $arrArtist[$artist->art_id] = $artist->art_name; 
        }
        $view->with(['webInfo' => $webInfo, 'arrLang' => $arrLang, 'arrArtist' => $arrArtist]);
    }
}
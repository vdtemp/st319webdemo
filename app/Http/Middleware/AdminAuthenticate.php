<?php

namespace App\Http\Middleware;
use Closure;
use Route;
use Lang;
use Illuminate\Http\Request;
use App\Repositories\AdminUserRepository;

class AdminAuthenticate
{
    public $admin_user_repository;

    public function __construct(AdminUserRepository $admin_user_repository){
        $this->admin_user_repository  = $admin_user_repository;
    }

    public function handle($request, Closure $next){

        $isLogged = $this->admin_user_repository::isLogged();
        if(!$isLogged){     
            $urlLogin = route('admin.login_fr');       
            return redirect($urlLogin);
        }else{
            $info = $this->admin_user_repository->getInfoLogged();
            if($info){
                 // Lưu thông tin
                app()->singleton('ADMIN_INFO', function() use ($info){
                    return $info;
                });
              
                // Gọi api lấy tất cả các quyền
                $staffRole      =  $this->admin_user_repository::getPermission();

                // Lưu thông tin quyền vào đây để tranh phải gọi api nhiều lần
                app()->singleton('ADMIN_ROLE', function() use ($staffRole){
                    return $staffRole;
                });

                // Lấy menu
                $menuAdmin  = $this->admin_user_repository->getMenuLogged();
                // Lưu thông tin
                app()->singleton('ADMIN_MENU', function() use ($menuAdmin){
                    return $menuAdmin;
                });

                // Check quyền vào module
                $nameRouteCurrent = Route::currentRouteName();
           
                //dd($nameRouteCurrent);
                $routeNotCheckPermission = $this->admin_user_repository::routeNotCheckPermission();
                if(!in_array($nameRouteCurrent, $routeNotCheckPermission)) {

                    $temp   = explode(".", $nameRouteCurrent);
                    $module = isset($temp[1]) ? $temp[1] : '';
                    $per    = isset($temp[2]) ? $temp[2] : '';

                    if(!$this->admin_user_repository::checkPermission($module, $per)){
                        return redirect()->route('access_denied');
                    }
                }
            }else{
                $nameRouteCurrent = Route::currentRouteName();
                if($nameRouteCurrent != 'admin.login_fr'){
                    return redirect()->route('admin.login_fr')->with('error', Lang::get('auth.timeout'));
                }
                return redirect()->route('access_denied');
            }
        }
            
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;
use Closure;
use Route;
use Lang;
use Illuminate\Http\Request;
use Cookie;
use App\Models\UserApi;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use URL;
class UserAuthenticate
{
    public $UserRepository;
    
    public function __construct(){
    }

    // check quyền user sử dụng CMS
    public function handle($request, Closure $next){
        $token = $request->cookie('st319Userkey');
        if(!$token) return redirect(route('userLogin'));
        try {
            $checkStr = md5('st319' . 'st3192022' . 'testversion');
            if($checkStr === $token){                
                $currentURL = URL::current();
                switch ($currentURL) {
                    case 'http://greyd.st319.vn':                        
                        return redirect()->route('artistDetail', ['id' => 1, 'alias' => 'greyd']);
                        break;
                    case 'http://monstar.st319.vn':
                        return redirect()->route('artistDetail', ['id' => 2, 'alias' => 'monstar']);
                        break;
                    case 'http://amee.st319.vn':
                        return redirect()->route('artistDetail', ['id' => 3, 'alias' => 'amee']);
                        break;
                }
                return $next($request);
            }else{
                return redirect('userLogin');
            }             
        } catch(ExpiredException $e) {
            return response()->json([
               'error' => 'Provided token is expired.'
            ], 400);
        } catch(\Exception $e) {
            return response()->json([
               'error' => 'An error while decoding token.'
            ], 400);
        }
        
    }
}

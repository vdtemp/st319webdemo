<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Repositories\ArtistRepository;
use App\Repositories\NewsRepository;
use Cookie;
use DB;

class AlbumController extends FrontEndController
{
    protected $ProductRepository;
    protected $ArtistRepository;
    public function __construct(ProductRepository $ProductRepository, ArtistRepository $ArtistRepository,NewsRepository $NewsRepository){
        $this->ProductRepository = $ProductRepository;
        $this->ArtistRepository = $ArtistRepository;
        $this->NewsRepository = $NewsRepository;
    }

    public function getType()
    {
        $type = [
            1 => 'Album',
            2 => 'Singles & EPs ',
            3 => 'Fimography'
        ];

        return $type;
    }
    public function index(Request $request, $id = null)
    {   
        $artId = $request->get('artId', 0);
        $id    = $request->get('id', 0);

        // Ds Nghệ sĩ
        $arrArtist = $this->ArtistRepository->getArtists([
            'job' => 1,       
        ]);        
        $listArtist = [];
        if($arrArtist){
            foreach ($arrArtist as $key => $value) {
                $listArtist[$value['art_id']] = $value['art_name'];
            }
        }

        // Danh sách sản phẩm       
        $albums = [];
        $products = $this->ProductRepository->getProducts([
            'artist_id' => $artId,
        ]);

        $listAlbum    = [];
        $hotAlbum     = [];
        $sellingAlbum = [];

        if($products){
            foreach ($products as $key => $pro) {
                // Hot|selected album
                if($pro['pro_type'] == 1 && $hotAlbum == []){
                    if($id > 0){
                        if($id == $pro['pro_id']){
                            $hotAlbum = $pro; 
                        }    
                    }else{
                        if ($pro['pro_hot'] == 1) {
                            $hotAlbum = $pro;   
                        }    
                    }
                }

                if($pro['pro_sale']){
                    $sellingAlbum[] = $pro;
                }

                $listAlbum[$pro['pro_type']][] = $pro;
            }
        }

        if($hotAlbum){
            $albumMusics = $this->ProductRepository->getAlbumVideos($hotAlbum['pro_id']);
            $hotAlbum['musics'] = $albumMusics;
        }

        // Loại sp
        $arrType = $this->gettype();

        // Tin phụ
        $listNews = $this->NewsRepository->getNews([
            'page'    => 0,
            'perPage' => 2,
        ]);

        $data = [
            'listArtist'   => $listArtist,
            'listAlbum'    => $listAlbum,
            'arrType'      => $arrType,
            'artId'        => $artId,
            'hotAlbum'     => $hotAlbum,
            'listNews'     => $listNews,
            'sellingAlbum' => $sellingAlbum,
            'id'           => $id
        ];

        return view('albumAll')->with($data);        
    }

    public function albumDetail(Request $request, $id)
    {
        // Ds Nghệ sĩ
        $arrArtist = $this->ArtistRepository->getArtists([
            'job' => 1,       
        ]);        
        $listArtist = [];
        if($arrArtist){
            foreach ($arrArtist as $key => $value) {
                $listArtist[$value['art_id']] = $value['art_name'];
            }
        }

        // Danh sách sản phẩm       
        $albums = [];
        $artId = $request->get('artId');
        $products = $this->ProductRepository->getProducts([
            'artist_id' => $artId,
        ]);

        $listAlbum    = [];
        $hotAlbum     = [];
        $sellingAlbum = [];
        $single       = [];
        if($products){
            foreach ($products as $key => $pro) {
                // Hot|selected album
                if($pro['pro_type'] == 1 && $hotAlbum == []){                     
                    if ($pro['pro_hot'] == 1) {
                        $hotAlbum = $pro;   
                    }     
                }

                if($pro['pro_id'] == $id){
                    $single =  $pro;    
                }                

                if($pro['pro_sale']){
                    $sellingAlbum[] = $pro;
                }

                $listAlbum[$pro['pro_type']][] = $pro;
            }
        }

        if($hotAlbum){
            $albumMusics = $this->ProductRepository->getAlbumVideos($hotAlbum['pro_id']);
            $hotAlbum['musics'] = $albumMusics;
        }

        // Loại sp
        $arrType = $this->gettype();

        // Tin phụ
        $listNews = $this->NewsRepository->getNews([
            'page'    => 0,
            'perPage' => 2,
        ]);
        
        $data = [
            'listArtist'   => $listArtist,
            'listAlbum'    => $listAlbum,
            'arrType'      => $arrType,
            'artId'        => $artId,
            'hotAlbum'     => $hotAlbum,
            'listNews'     => $listNews,
            'sellingAlbum' => $sellingAlbum,
            'single'       => $single,
        ];

        return view('album')->with($data);
    }
}

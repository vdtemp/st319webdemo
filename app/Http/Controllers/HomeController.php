<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BannerRepository;
use App\Repositories\NewsRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\ArtistRepository;
use App\Repositories\ProductRepository;
use Cookie;
use DB;

class HomeController extends FrontEndController
{
    public function __construct(){

    }

    public function index(Request $request){ 
        /*Banner*/
        $BannerRepository = new BannerRepository;
        $banners = $BannerRepository->getBanners(['pos'=>'main_banner']);
        
        /*News*/
        $NewsRepository = new NewsRepository;
        $hotNews = $NewsRepository->getNews([
            'hot' => 1,
            'page' => 0,
            'perPage' => 1,
            'type' => 1,
        ]);

        /*Campaign*/
        $CampaignRepository  = new CampaignRepository;
        $hotCampaign = $CampaignRepository->getCampaigns([
            'hot' => 1,
            'page' => 0,
            'perPage' => 1,
        ]);

        /*Article*/ 
        $hotArticle = $NewsRepository->getNews([
            'hot' => 1,
            'page' => 0,
            'perPage' => 1,
            'type' => 2,
        ]);
        
        /*music*/
        $ProductRepository = new ProductRepository;
        $hotProduct = $ProductRepository->getProducts([
            'hot' => 1,
            'page' => 0,
            'perPage' =>8, 
        ]);
        
        $data = [
            'banners' => $banners,
            'hotNews' => $hotNews,
            'hotCampaign' => $hotCampaign,
            'hotArticle' => $hotArticle,
            'hotProduct' => $hotProduct,
        ];

        return view('home')->with($data);
    }
    
    public function introduction(Request $request){
        /*Banner*/
        $BannerRepository = new BannerRepository;
        $banners = $BannerRepository->getBanners(['pos'=>'intro_banner']);

        $data = [
            'banners' => $banners,
        ];
        return view('gioithieu')->with($data);
    }
}

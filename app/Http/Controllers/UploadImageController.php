<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\ImageManager;
use Validator;
use App\Models\City;
use App\Models\CityTranslation;

class UploadImageController extends FrontEndController
{
	public function image(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'files.*'   => ['mimes:jpeg,jpg,png,gif','max:2000']
        ],[
            'files.*.mimes'      => 'Ảnh tải lên không đúng định dạng',
            'files.*.max'        => 'Ảnh tải lên kích thước tối đa :max KB'
        ]);
        if($validator->fails()){
        	return response($validator->errors(),500);
        }
		$files = $request->file('files');
		if(!empty($files[0])){
			return ['/uploads/hotel/'.ImageManager::upload($files[0], 'hotel')];
		}
	}

	public function city()
	{
		$data = [
			'Bắc Kạn',
			'Bạc Liêu',
			'Bắc Ninh',
			'Bến Tre',
			'Bình Định',
			'Bình Dương',
			'Bình Phước',
			'Bình Thuận',
			'Cà Mau',
			'Cao Bằng',
			'Đắk Lắk',
			'Đắk Nông',
			'Điện Biên',
			'Đồng Nai',
			'Đồng Tháp',
			'Gia Lai',
			'Hà Giang',
			'Hà Nam',
			'Hà Tĩnh',
			'Hải Dương',
			'Hậu Giang',
			'Hòa Bình',
			'Hưng Yên',
			'Khánh Hòa',
			'Kiên Giang',
			'Kon Tum',
			'Lai Châu',
			'Lâm Đồng',
			'Lạng Sơn',
			'Lào Cai',
			'Long An',
			'Nam Định',
			'Nghệ An',
			'Ninh Bình',
			'Ninh Thuận',
			'Phú Thọ',
			'Quảng Bình',
			'Quảng Nam',
			'Quảng Ngãi',
			'Quảng Ninh',
			'Quảng Trị',
			'Sóc Trăng',
			'Sơn La',
			'Tây Ninh',
			'Thái Bình',
			'Thái Nguyên',
			'Thanh Hóa',
			'Thừa Thiên Huế',
			'Tiền Giang',
			'Trà Vinh',
			'Tuyên Quang',
			'Vĩnh Long',
			'Vĩnh Phúc',
			'Yên Bái',
			'Phú Yên',
			'Cần Thơ',
			'Đà Nẵng',
			'Hải Phòng',
			'Hà Nội',
			'TP HCM',
		];
		foreach ($data as $key => $value) {
			$city = City::insert([
				"CITY_ID" => $key + 4,
				"TIMEZONE" => 55,
				"COUNTRY_ID" => 1
			]);
			if($city){
				CityTranslation::insert([
					"NAME" => $value,
					"LANGUAGE_ID" => 1,
					"CITY_ID" => $key + 4,
				]);
			}
		}
	}
}
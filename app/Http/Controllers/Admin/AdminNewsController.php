<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser; 
use App\Models\NewsMulti; 
use App\Models\NewsImage;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;
use App\Helper\HtmlCleanup;


class AdminNewsController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new NewsMulti;        
      $news_title = $request->get('news_title', '');
      $page = intval($request->input('page',1));
      if($news_title != ''){
         $searchTitle = removeAccent(strtolower(replaceMQ($news_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         $query = $query->where('new_alias',$searchTitle);
      }

      $news_hot = $request->get('news_hot', -1);
      $params['news_hot'] = $news_hot;
      if($news_hot > 0){ 
         $query = $query->where('news_hot',$news_hot);
      }

      $news_status = $request->get('news_status', -1);
      $params['news_status'] = $news_status;
      if($news_status > 0){ 
         $query = $query->where('news_status',$news_status);
      }

      $new_type = $request->get('new_type', -1);
      $params['new_type'] = $new_type;
      if($new_type > 0){ 
         $query = $query->where('new_type',$new_type);
      }

      $NewsMulti  = $query->orderBy('new_id', 'desc')->paginate(15);
      
      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];
      $arrHot    = [0 => 'no Hot', 1 => 'Hot']; 

      $data['params']          = $params;
      $data['arrLang']         =  $this->getLang();
      $data['arrStatus']       = $arrStatus; 
      $data['arrHot']          = $arrHot; 
      $data['arrType']         = $this->getType(); 
      
      $data['page_filter_url'] = '/admin/news';
      $data['page']            = $page;
      $data['news']            = $NewsMulti;

      return view('admin.module.news.list')->with($data);
   }

   public function create(Request $request)
   {
      $data            = [];
      $admin_id        = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role            = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck       = 0;
      $data['accept']  = 0;
      $data['arrType'] = $this->getType(); 

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['news']) && (isset($role['news']['accept']) && $role['news']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  
      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [
             // 'type_id'      => 'required|numeric|not_in:0',
             // 'cate_id'      => 'required|numeric|not_in:0',
             'new_title'        => 'max:255|required',
             'new_teaser'  => 'required',
             'new_picture' => 'max:255|required',
             'new_description'      => 'required'
         ],[
             // 'cate_id.not_in'        => 'Bạn chưa chọn danh mục.',
             // 'type_id.not_in'        => 'Bạn chưa chọn loại bài viết.',
             'new_title.required'        => 'Bạn nhập tiêu đề.',
             'new_teaser.required'  => 'Bạn nhập mô tả.',
             'new_picture.required' => 'Bạn chưa chọn ảnh đại diện.',
             'new_description.required'      => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/news/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         // news
         $dataInsert['new_active']      = intval($request->new_active);
         $dataInsert['new_type']      = intval($request->new_type); 
         $dataInsert['new_hot']         = intval($request->new_hot); 
         $dataInsert['new_order']       = intval($request->new_order);
         $dataInsert['new_date']        = time();
         $dataInsert['new_last_modify'] = time();  
         
          
         $new_content        = $request->get('new_description', '');
         $new_content        = stripslashes($new_content);
         $my_HtmlCleanup     = new HtmlCleanup($new_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $new_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->new_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['new_title']        = $request->get('new_title', '');
         $dataInsert['new_alias']        = $searchTitle;
         $dataInsert['new_picture']      = $request->get('new_picture', '');
         $dataInsert['new_link_video']   = $request->get('new_link_video', '');
         $dataInsert['new_teaser']       = $request->get('new_teaser', '');
         $dataInsert['new_description']  = $new_description;
         $dataInsert['new_meta_title']   = $request->get('new_meta_title', '');
         $dataInsert['new_meta_desc']    = $request->get('new_meta_desc', '');
         $dataInsert['new_meta_keyword'] = $request->get('new_meta_keyword', ''); 

         $dataInsert['admin_id']    = $admin_id;

         DB::beginTransaction();
         $newID = NewsMulti::create($dataInsert);
         if(isset($newID) && $newID->new_id > 0){
                              
             // new_image
             $arrImage = $request->picture_data;
             try {
                 if(isset($arrImage) && $arrImage != null){
                     foreach ($arrImage as $key => $value) {
                         $dataInsertImg           = [];
                         $dataInsertImg['newi_url']    = replaceMQ($value);
                         $dataInsertImg['newi_thumps'] = replaceMQ($value);
                         $dataInsertImg['newi_new_id'] = intval($newID->new_id);
                         NewsImage::create($dataInsertImg);
                     }
                 }
                 DB::commit();
                 Session::flash('success','Thêm mới bài viết thành công!');
             } catch (\Throwable $th) {
                 DB::rollBack();
                 Session::error('Lỗi','Thêm mới bài viết không thành công!');
             }
         }
      }
      return view("admin.module.news.add", $data);
   }

   public function edit($id, Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['news']) && (isset($role['news']['accept']) && $role['news']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  

      $data['arrType'] = $this->getType();

      $news = NewsMulti::where('new_id', $id)->first()->toArray();
      $data['news'] = $news;

      if($request->action == "update"){
         $validator = Validator::make($request->all(), [
             // 'type_id'      => 'required|numeric|not_in:0',
             // 'cate_id'      => 'required|numeric|not_in:0',
             'new_title'        => 'max:255|required',
             'new_teaser'  => 'required',
             'new_picture' => 'max:255|required',
             'new_description'      => 'required'
         ],[
             // 'cate_id.not_in'        => 'Bạn chưa chọn danh mục.',
             // 'type_id.not_in'        => 'Bạn chưa chọn loại bài viết.',
             'new_title.required'        => 'Bạn nhập tiêu đề.',
             'new_teaser.required'  => 'Bạn nhập mô tả.',
             'new_picture.required' => 'Bạn chưa chọn ảnh đại diện.',
             'new_description.required'      => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/news/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         // news
         $dataInsert['new_active']      = intval($request->new_active); 
         $dataInsert['new_type']        = intval($request->new_type); 
         $dataInsert['new_hot']         = intval($request->new_hot); 
         $dataInsert['new_order']       = intval($request->new_order);
         $dataInsert['new_date']        = time();
         $dataInsert['new_last_modify'] = time();          
          
         $new_content        = $request->get('new_description', '');
         $new_content        = stripslashes($new_content);
         $my_HtmlCleanup     = new HtmlCleanup($new_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $new_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->new_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['new_title']        = $request->get('new_title', '');
         $dataInsert['new_alias']        = $searchTitle;
         $dataInsert['new_picture']      = $request->get('new_picture', '');
         $dataInsert['new_link_video']   = $request->get('new_link_video', '');
         $dataInsert['new_teaser']       = $request->get('new_teaser', '');
         $dataInsert['new_description']  = $new_description;
         $dataInsert['new_meta_title']   = $request->get('new_meta_title', '');
         $dataInsert['new_meta_desc']    = $request->get('new_meta_desc', '');
         $dataInsert['new_meta_keyword'] = $request->get('new_meta_keyword', ''); 

         $dataInsert['edit_admin']    = $admin_id;

         DB::beginTransaction();
         try {
            $newID = NewsMulti::where("new_id","=",$id)->update($dataInsert);

            if($newID){
                // new_image
                NewsImage::where("newi_new_id","=",$id)->delete(); 
                $arrImage = $request->picture_data;
                if($arrImage){
                    try {
                       if(isset($arrImage) && $arrImage != null){
                           foreach ($arrImage as $key => $value) {
                               $dataInsertImg                = [];
                               $dataInsertImg['newi_url']    = replaceMQ($value);
                               $dataInsertImg['newi_thumps'] = replaceMQ($value);
                               $dataInsertImg['newi_new_id'] = intval($id);                               
                               NewsImage::create($dataInsertImg);
                           }
                       }
                       DB::commit();
                       Session::flash('success','Sửa bài viết thành công!');                       
                   } catch (\Throwable $th) {
                       DB::rollBack();
                       Session::flash('error','Sửa bài viết không thành công!');
                   }  
                }               
            }  
            return redirect('/admin/news/'); 
         } catch (Exception $e) {
            return redirect('/admin/news/');
         }
         
      } 

      return view('admin.module.news.edit', ['id' => $id])->with($data);
   }

   public function destroy($id)
   {
      $NewsMulti = NewsMulti::where("new_id","=",$id);

      if ($NewsMulti)
      {
         $NewsMulti->delete();
         Session::flash('success', 'Xóa tin thành công!');
      }

      return redirect('/admin/news/');

   }

   public function upload(Request $request){
      $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0]; 

      if ($request->hasFile('Filedata')) {
         $image                  = $request->file('Filedata');

         $validator = Validator::make($request->all(), [
             'Filedata'   => 'mimes:jpeg,jpg,png,gif'
         ],[
             'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
         ]);

         if ($validator->fails()) {
             $dataReturn['msg'] = 'File upload không tồn tại!';
         }
         //upload images
         $dataReturn['filename'] = env("CDN_IMAGE") . 'news/'.ImageManager::upload($image, 'news');
         list($width, $height)   = getimagesize($image->getRealPath());
         $dataReturn['width']    = $width;
         $dataReturn['height']   = $height;
         $dataReturn['status']   = true;
         $dataReturn['filesize'] = $image->getClientSize();
         $dataReturn['url'] = $dataReturn['filename'];
      }else{
          $dataReturn['msg'] = 'File upload không tồn tại!';
      }

      return json_encode($dataReturn);
    }
 
   public function changeOrder(Request $request){
     $id     = $request->get('id');
     $value  = $request->get('value', 0);
     $return = ['status'=> 0, 'msg' => ''];

     if($id){
         try {
             $update = NewsMulti::where('new_id', $id)->update([
                 'new_order' => intval($value),
             ]);
             $return['msg'] = 'Update Thành Công';
         } catch (Exception $e) {
             $return['msg'] = 'Update Thất bại';    
             $return['status'] = 1;
         }            
     }
     return $return;
   }
   public function changestatus(Request $request)
   {
      $id     = intval($request->get('id'));
      $status = intval($request->get('status'));
      $news    = NewsMulti::where("new_id","=",$id);

      if ($news)
      {
         $news->update(['new_active' => $status] ); 
      }
      $return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
      return $return;
    }

   public function setHotNews(Request $request){

      $id = $request->get('idata', 0);

      if($id > 0 ){
         $NewsMulti = NewsMulti::where('new_id', $id)->first(); 
         if($NewsMulti->new_hot == 1){
             NewsMulti::WHERE('new_id', $id)->update(['new_hot' => 0, 'new_last_modify' => time()]);
         }else{
             NewsMulti::WHERE('new_id', $id)->update(['new_hot' => 1, 'new_last_modify' => time()]);
         }
         return ['status' => 0, 'msg' => 'Thành công'];
      }else{
         return ['status' => 1, 'msg' => 'Thất bại'];
      }
   }
  
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }

   public function getType()
   {
     $type = [
         1 => 'News',
         2 => 'Article'
     ];

     return $type;
   }
}    

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser;  
use App\Models\Event;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;
use App\Helper\HtmlCleanup;


class AdminEventController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new Event;      
      $page = intval($request->input('page',1));

      $eve_title = $request->get('eve_title', '');
      if($eve_title != ''){
         $searchTitle = removeAccent(strtolower(replaceMQ($eve_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         $query = $query->where('eve_title_search',$searchTitle);
      }

      $eve_status = $request->get('eve_status', -1);
      if($eve_status > 0){ 
         $query = $query->where('eve_status',$eve_status);
      }

      $lang_id = $request->get('lang_id', '');
      if($lang_id > 0){ 
         $query = $query->where('lang_id',$lang_id);
      }

      $Event  = $query->orderBy('eve_id', 'desc')->paginate(15);
      
      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];

      $params = [
         'lang_id'     => $lang_id,
         'eve_title'   => $eve_title,
         'eve_status'  => $eve_status,
      ];

      $data['params']          = $params;
      $data['arrLang']         =  $this->getLang();
      $data['arrStatus']       = $arrStatus; 
      $data['arrUser']         = []; 
      
      $data['page_filter_url'] = '/admin/event';
      $data['page']            = $page;
      $data['Events']          = $Event;

      return view('admin.module.event.list')->with($data);
   }

   public function create(Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['event']) && (isset($role['event']['accept']) && $role['event']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  
      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [ 
             'eve_title'       => 'max:255|required',
             'eve_intro'       => 'required',
             'eve_banner'     => 'max:255|required',
             'eve_description' => 'required'
         ],[ 
             'eve_title.required'       => 'Bạn chưa nhập tiêu đề.',
             'eve_intro.required'       => 'Bạn chưa nhập mô tả.',
             'eve_banner.required'     => 'Bạn chưa chọn ảnh đại diện.',
             'eve_description.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/campaign/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         $dataInsert['eve_status']      = intval($request->eve_status);  
         $dataInsert['eve_order']       = intval($request->eve_order);    
         $dataInsert['eve_created_date'] = time();
          
         $eve_content    = $request->get('eve_description', '');
         $eve_content    = stripslashes($eve_content);
         $my_HtmlCleanup = new HtmlCleanup($eve_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $eve_description  = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->eve_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['eve_title']        = $request->get('eve_title', '');
         $dataInsert['eve_title_search'] = $searchTitle;
         $dataInsert['eve_banner']       = $request->get('eve_banner', '');
         $dataInsert['eve_video_url']    = $request->get('eve_video_url', '');
         $dataInsert['eve_tags']         = $request->get('eve_tags', '');
         $dataInsert['eve_intro']        = $request->get('eve_intro', '');
         $dataInsert['eve_form_apply']   = $request->get('eve_form_apply', '');
         $dataInsert['eve_description']  = $eve_description; 

         $dataInsert['admin_id']    = $admin_id;
         $dataInsert['lang_id']     = intval($request->get('lang_id', 1));  
         try {
            $Event = Event::create($dataInsert);
            if(isset($Event) && $Event->eve_id > 0){
                 Session::flash('success','Thêm mới thành công!'); 
                 return redirect(route('admin.event.index'));         
            }else{
               Session::flash('error','Thêm mới không thành công!'); 
            }   
         } catch (Exception $e) {
            Session::flash('error','Thêm mới không thành công!'); 
         }
         
      }
      return view("admin.module.event.add", $data);
   }

   public function edit($id, Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['event']) && (isset($role['event']['accept']) && $role['event']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  

      $Event = Event::where('eve_id', $id)->first();
      $data['Event'] = $Event;

      if($request->action == "update"){
         $validator = Validator::make($request->all(), [ 
             'eve_title'       => 'max:255|required',
             'eve_intro'       => 'required',
             'eve_banner'     => 'max:255|required',
             'eve_description' => 'required'
         ],[ 
             'eve_title.required'       => 'Bạn nhập tiêu đề.',
             'eve_intro.required'       => 'Bạn nhập mô tả.',
             'eve_banner.required'     => 'Bạn chưa chọn ảnh đại diện.',
             'eve_description.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect(route('admin.event.edit', ['id' => $id]))->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         $dataInsert['lang_id']    = intval($request->lang_id); 
         $dataInsert['eve_status'] = intval($request->eve_status);  
         $dataInsert['eve_order']  = intval($request->eve_order);  
          
         $eve_content        = $request->get('eve_description', '');
         $eve_content        = stripslashes($eve_content);
         $my_HtmlCleanup     = new HtmlCleanup($eve_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $eve_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->eve_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);         
        
         $dataInsert['eve_title']        = $request->get('eve_title', '');
         $dataInsert['eve_title_search'] = $searchTitle;
         $dataInsert['eve_banner']      = $request->get('eve_banner', '');
         $dataInsert['eve_video_url']    = $request->get('eve_video_url', '');
         $dataInsert['eve_tags']         = $request->get('eve_tags', '');
         $dataInsert['eve_intro']        = $request->get('eve_intro', '');
         $dataInsert['eve_form_apply']   = $request->get('eve_form_apply', '');
         $dataInsert['eve_description']  = $eve_description; 
  
         try {
            $EventID = Event::where("eve_id","=",$id)->update($dataInsert);
            Session::flash('success','Sửa thành công!');       
            return redirect('/admin/event/'); 
         } catch (Exception $e) {
            Session::error('Lỗi','Sửa không thành công!');
            return redirect('/admin/event/');
         }
         
      } 

      return view('admin.module.event.edit', ['id' => $id])->with($data);
   }

   public function destroy($id)
   {
      $Event = Event::where("eve_id","=",$id);

      if ($Event)
      {
         $Event->delete();
         Session::flash('success', 'Xóa thành công!');
      }

      return redirect('/admin/event/');

   }

   public function upload(Request $request){
      $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0]; 

      if ($request->hasFile('Filedata')) {
         $image                  = $request->file('Filedata');

         $validator = Validator::make($request->all(), [
             'Filedata'   => 'mimes:jpeg,jpg,png,gif'
         ],[
             'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
         ]);

         if ($validator->fails()) {
             $dataReturn['msg'] = 'File upload không tồn tại!';
         }
         //upload images
         $dataReturn['filename'] = env("CDN_IMAGE") . 'event/'.ImageManager::upload($image, 'event');
         list($width, $height)   = getimagesize($image->getRealPath());
         $dataReturn['width']    = $width;
         $dataReturn['height']   = $height;
         $dataReturn['status']   = true;
         $dataReturn['filesize'] = $image->getClientSize();
         $dataReturn['url'] = $dataReturn['filename'];
      }else{
          $dataReturn['msg'] = 'File upload không tồn tại!';
      }

      return json_encode($dataReturn);
    }
 
   public function changeOrder(Request $request){
     $id     = $request->get('id');
     $value  = $request->get('value', 0);
     $return = ['status'=> 0, 'msg' => ''];

     if($id){
         try {
             $update = Event::where('eve_id', $id)->update([
                 'eve_order' => intval($value),
             ]);
             $return['msg'] = 'Update Thành Công';
         } catch (Exception $e) {
             $return['msg'] = 'Update Thất bại';    
             $return['status'] = 1;
         }            
     }
     return $return;
   }
   public function changestatus(Request $request)
   {
      $id     = intval($request->get('id'));
      $status = intval($request->get('status'));
      $Event    = Event::where("eve_id","=",$id);

      if ($Event)
      {
         $Event->update(['eve_status' => $status] ); 
      }
      $return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
      return $return;
    } 
  
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }
}    

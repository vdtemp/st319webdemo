<?php

namespace App\Http\Controllers\Admin;

use App\Models\AdminUser;
use Illuminate\Http\Request;
use Session;
use Validator;
use Lang;
use App\Helper\ImageManager;
use App\Repositories\AdminUserRepository;
use DB;

class AdminUserController extends AdminController
{
	public $admin_user_repository;
	public function __construct(AdminUserRepository $admin_user_repository){
		$this->admin_user_repository = $admin_user_repository;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = [];
        $query = AdminUser::onWriteConnection();
        // $query = AdminUser::query();
        
        $username = replaceMQ($request->input('username'));
        // Không show các admin tổng
        $adminNotEdit 	= $this->admin_user_repository::$adminNotEdit;
        foreach ($adminNotEdit as $key => $value) {
        	$query->where('adm_loginname', '!=', $value);
        }

        if ($username)
        {
            $query->where('adm_loginname', 'like', '%' . $username . '%');
            $params['username'] = $username;
        }

        $data['admin_user']      = $query->orderBy('adm_id', 'desc')->paginate(20);
        $data['params']          = $params;
        $data['page_filter_url'] = '/admin/staff';

        return view("admin.module.admin_user.list", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     // dd(app()->ADMIN_INFO->adm_id);
    //     // dd(config('cache.default'));
    //     $allRole 	= $this->admin_user_repository::getAllMenuRoleAdmin();

    //     return view("admin.module.admin_user.add", ['allRole' => $allRole]);
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id = 0)
    {
        $allRole         = $this->admin_user_repository::getAllMenuRoleAdmin();
        $arrData         = array();
        
        $adm_create_time = time();
        $adm_udate_time  = time();
        $adm_admin_id    = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
        $adm_loginname   = "";
        $adm_password    = "";
        $adm_email       = "";
        $adm_phone       = "";
        $adm_department  = 0;
        $adm_active      = 0;
        $adm_avatar      = "";
        $dataRoleUser    = array();

        if($id > 0){
            $admin_user = AdminUser::findOrFail($id);
            if (!$admin_user)
            {
                echo 'User không tồn tại!';
                die();
            }

            $adm_loginname  = $admin_user->adm_loginname;
            $adm_email      = $admin_user->adm_email;
            $adm_phone      = $admin_user->adm_phone;
            $adm_department = $admin_user->adm_department;
            $adm_active     = $admin_user->adm_active;
            $dataRoleUser   = $admin_user->adm_role != "" ? json_decode($admin_user->adm_role,1) : array();

        }

        // Lặp để lấy các quyền đc post lên
        $dataRole   = [];
        foreach ($allRole as $key => $value) {
            foreach ($value['sub'] as $key_sub => $value_sub) {
                $field = $key . "_" . $key_sub;
                $check = $request->input($field);
                if($check == 1){
                    if(!isset($dataRole[$key])) $dataRole[$key] = [];
                    $dataRole[$key][$key_sub] = 1;
                    if(isset($value_sub['keys'])){
                        $actions_sub    = explode(",", $value_sub['keys']);
                        foreach ($actions_sub as $action_sub) {
                            $dataRole[$key][$action_sub] = 1;
                        }
                    }
                }
            }
        }

        $adm_role       = $request->input("adm_role", $dataRole ? json_encode($dataRole) : '');
        $adm_loginname  = $request->input("adm_loginname", $adm_loginname);
        $adm_password   = $request->input("adm_password", $adm_password);
        $adm_email      = $request->input("adm_email", $adm_email);
        $adm_phone      = $request->input("adm_phone", $adm_phone);
        $adm_department = $request->input("adm_department", $adm_department);
        $adm_active     = $request->input("adm_active", $adm_active);

        if(isset($request->picture_data) && count($request->picture_data) > 0){
            $picture_data = $request->picture_data; 
            $adm_avatar   = $picture_data[0];
        }

        $action         = $request->input("action");

        $arrData = [
                'adm_role'        => $adm_role,
                'adm_loginname'   => $adm_loginname,
                'adm_password'    => md5($adm_password),
                'adm_email'       => $adm_email,
                'adm_phone'       => $adm_phone,
                'adm_department'  => $adm_department,
                'adm_active'      => $adm_active,
                'adm_create_time' => $adm_create_time,
                'adm_udate_time'  => $adm_udate_time,
                'adm_admin_id'    => $adm_admin_id,
                'adm_avatar'      => $adm_avatar
            ];

        if($action == 'execute'){
       
            if($id <= 0){
                $validator  = Validator::make($request->all(), [
                    'adm_loginname' => 'required|unique:admin_user',
                    'adm_password'  => 'min:6|required_with:repassword|same:repassword|required',
                    'repassword'    => 'min:6|required',
                    // 'adm_avatar'   => 'mimes:jpeg,jpg,png,gif'
                ],[
                    'adm_loginname.required' => 'Username là trường bắt buộc',
                    'adm_loginname.unique'   => 'Username đã tồn tại',
                    'adm_password.same'      => 'Mật khẩu không trùng khớp',
                    'adm_password.required'  => 'Password là trường bắt buộc',
                    // 'adm_avatar.mimes'      => 'Ảnh tải lên không đúng định dạng'
                ]);

                if ($validator->fails()) {
                    return redirect('/admin/staff/create')->withErrors($validator->messages())->withInput();
                }
                // Lưu thông tin
                if ($adminuser = AdminUser::create($arrData)) {
                    Session::flash('success', 'Thêm mới user thành công!');
                } else {
                    Session::flash('danger', 'Lỗi: vui lòng thao tác lại');
                }

            }else{

                $admin_user->update($arrData);
                Session::flash('success', 'Sửa thông tin user thành công!');
                return redirect('/admin/staff/create');
            }
        }

        return view("admin.module.admin_user.add", ['allRole' => $allRole, 'data' => $arrData, 'dataRoleUser' => $dataRoleUser, 'id' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$data 			= [];
		$admin_user		= AdminUser::find($id);

		if(!$admin_user) exit(Lang::get('auth.record_notfound'));

		// Không được edit các admin tổng
		$adminNotEdit 	= $this->admin_user_repository::$adminNotEdit;
        if(in_array($admin_user->adm_loginname, $adminNotEdit))  return redirect()->route('access_denied');

		$data['admin_user']	= $admin_user;
		// Nếu đã được phân quyền thì định dạng lại để show
		if(isset($admin_user->adm_role) && $admin_user->adm_role){
			$data['admin_user']->adm_role 	= json_decode($admin_user->adm_role, 1);
		}

		// Lấy toàn bộ quyền
		$allRole			= $this->admin_user_repository::getAllMenuRoleAdmin();
		$data['allRole']	= $allRole;

        return view('admin.module.admin_user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {

    	$allRole 	= $this->admin_user_repository::getAllMenuRoleAdmin();
        $arrData   = array();

        $AdminUser = AdminUser::where(['adm_id' => $id]);

        if($AdminUser){
            $validator = Validator::make($request->all(), [
                'adm_avatar'   => 'mimes:jpeg,jpg,png,gif'
            ],[
                'adm_avatar.mimes'      => 'Ảnh tải lên không đúng định dạng'
            ]);

            if ($validator->fails()) {
                return redirect('/admin/staff/'.$id.'/edit')->withErrors($validator)->withInput();
            }

            $arrData['adm_email']       = $request->adm_email;
            $arrData['adm_phone']       = $request->adm_phone;
            $arrData['adm_department']  = $request->adm_department;
            $arrData['adm_active']      = $request->adm_active;
            $arrData['adm_udate_time']  = time();
            $arrData['adm_admin_id']    = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;

            // Lặp để lấy các quyền đc post lên
			$dataRole 	= [];
			foreach ($allRole as $key => $value) {
				foreach ($value['sub'] as $key_sub => $value_sub) {
					$field = $key . "_" . $key_sub;
					$check = $request->input($field);
					if($check == 1){
						if(!isset($dataRole[$key])) $dataRole[$key] = [];
						$dataRole[$key][$key_sub] = 1;
						if(isset($value_sub['keys'])){
							$actions_sub 	= explode(",", $value_sub['keys']);
							foreach ($actions_sub as $action_sub) {
								$dataRole[$key][$action_sub] = 1;
							}
						}
					}
				}
			}
			$arrData['adm_role'] 	= $dataRole ? json_encode($dataRole) : '';

            
            $AdminUser->update($arrData);

            // Update thông tin ảnh
            if ($request->hasFile('adm_avatar')) {
                $image = $request->file('adm_avatar');
                //upload images
                $fileName = ImageManager::upload($image, 'user_admin');
                //insert to database
                $AdminUser->update(['adm_avatar' => $fileName]);

            }
            Session::flash('success', 'Sửa thông tin user thành công!');

        }else{
            return redirect()->route('admin.staff')->with('error','User không tồn tại!');
        }
        return redirect('/admin/staff');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $AdminUser = AdminUser::where(['adm_id' => $id]);

        if ($AdminUser)
        {   
            try {
                $AdminUser->delete();
                Session::flash('success', 'Xóa thành công!');
            } catch (\Throwable $th) {
                Session::flash('error', 'Bản ghi này đang có dữ liệu liên quan!');
            }
            
        }

        return redirect('/admin/staff/');
    }

    public function upload(Request $request){
        $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0];
        // Update thông tin ảnh
       
        if ($request->hasFile('Filedata')) {
            $image                  = $request->file('Filedata');

            $validator = Validator::make($request->all(), [
                'Filedata'   => 'mimes:jpeg,jpg,png,gif'
            ],[
                'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
            ]);

            if ($validator->fails()) {
                $dataReturn['msg'] = 'File upload không tồn tại!';
                // return redirect('/admin/staff/'.$id.'/edit')->withErrors($validator)->withInput();
            }
            $actual_link = env('DATA_IMAGE_URL');

            //upload images
            $dataReturn['filename'] = ImageManager::upload($image, 'user_admin');
            list($width, $height)   = getimagesize($image->getRealPath());
            $dataReturn['width']    = $width;
            $dataReturn['height']   = $height;
            $dataReturn['status']   = true;
            $dataReturn['filesize'] = $image->getClientSize();
            $dataReturn['url'] = $actual_link.'user_admin/'.$dataReturn['filename'];
        }else{
             $dataReturn['msg'] = 'File upload không tồn tại!';
        }

        return json_encode($dataReturn);

    }

}

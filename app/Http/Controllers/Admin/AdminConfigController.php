<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Models\Language; 
use App\Models\AdminUser; 
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;

class AdminConfigController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      $configuration = Configuration::first()->toArray();
      
      $data  = [
         'configuration' => $configuration,
      ];
      return view('admin.module.configuration.index')->with($data);
   }

   public function update(Request $request){
      $configuration = Configuration::where('con_id', 1)->first()->toArray();
       
      $dataUpdate = [
         "con_admin_email"         => $request->get('con_admin_email', $configuration['con_admin_email']),
         "con_site_title"          => $request->get('con_site_title', $configuration['con_site_title']),
         "con_meta_description"    => $request->get('con_meta_description', $configuration['con_meta_description']),
         "con_meta_keywords"       => $request->get('con_meta_keywords', $configuration['con_meta_keywords']),
         "con_currency"            => $request->get('con_currency', $configuration['con_currency']), 
         
         "con_contact"             => $request->get('con_contact', $configuration['con_contact']),
         "con_hotline"             => $request->get('con_hotline', $configuration['con_hotline']),
         "con_background_img"      => $request->get('con_background_img', $configuration['con_background_img']),
         "con_background_color"    => $request->get('con_background_color', $configuration['con_background_color']),
         "con_address"             => $request->get('con_address', $configuration['con_address']),
         "con_background_homepage" => $request->get('con_background_homepage', $configuration['con_background_homepage']),
         "con_facebook"            => $request->get('con_facebook', $configuration['con_facebook']),
         "con_map"                 => $request->get('con_map', $configuration['con_map']),
         "con_pinterest"           => $request->get('con_pinterest', $configuration['con_pinterest']),
         "con_youtube"             => $request->get('con_youtube', $configuration['con_youtube']),
         "con_twitter"             => $request->get('con_twitter', $configuration['con_twitter']),
         "con_company"             => $request->get('con_company', $configuration['con_company']),
         "con_ceo_name"            => $request->get('con_ceo_name', $configuration['con_ceo_name']),
         "con_business_field"      => $request->get('con_business_field', $configuration['con_business_field']),
         "con_date_establishment"  => $request->get('con_date_establishment', $configuration['con_date_establishment']),
      ];

      try {
         $update = Configuration::where('con_id', 1)->update($dataUpdate);
      } catch (Exception $e) {
         
      }

      return redirect(route('admin.configuration.index'));
      
   }
}    
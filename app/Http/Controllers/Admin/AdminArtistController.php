<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser;  
use App\Models\Artist;
use App\Models\ArtistGallery;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;
use App\Helper\HtmlCleanup;


class AdminArtistController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new Artist;        
      $art_name = $request->get('art_name', '');
      $page = intval($request->input('page',1));
      if($art_name != ''){
         $searchTitle = removeAccent(strtolower(replaceMQ($art_name)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         $query = $query->where('art_namesearch',$searchTitle);
      }

      $art_hot = $request->get('art_hot', -1);
      if($art_hot > 0){ 
         $query = $query->where('art_hot',$art_hot);
      }

      $art_status = $request->get('art_status', -1);
      if($art_status > 0){ 
         $query = $query->where('art_status',$art_status);
      }

      $art_job = $request->get('art_job', -1);
      if($art_job > 0){ 
         $query = $query->where('art_job',$art_job);
      }

      $Artist  = $query->orderBy('art_id', 'desc')->paginate(15);
      
      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];
      $arrHot    = [0 => 'no Hot', 1 => 'Hot']; 

      $data['params']          = $params;
      $data['arrLang']         =  $this->getLang();
      $data['arrJob']         =  $this->getJobs();
      $data['arrStatus']       = $arrStatus; 
      $data['arrHot']          = $arrHot; 
      
      $data['page_filter_url'] = '/admin/artist';
      $data['page']            = $page;
      $data['Artists']            = $Artist;

      return view('admin.module.artist.list')->with($data);
   }

   public function create(Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['artist']) && (isset($role['artist']['accept']) && $role['artist']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  
      $data['arrJob']         =  $this->getJobs();

      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [ 
             'art_name'        => 'max:255|required',
             'art_intro'       => 'required',
             'art_avatar'      => 'max:255|required',
             'art_description' => 'required'
         ],[ 
             'art_name.required'        => 'Bạn nhập tên.',
             'art_intro.required'       => 'Bạn nhập mô tả.',
             'art_avatar.required'      => 'Bạn chưa chọn ảnh đại diện.',
             'art_description.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/artist/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         // news
         $dataInsert['art_active']      = intval($request->art_active); 
         $dataInsert['art_hot']         = intval($request->art_hot);  
         $dataInsert['art_create_date'] = time();
         $dataInsert['art_last_modify'] = time();  
         
          
         $art_content        = $request->get('art_description', '');
         $art_content        = stripslashes($art_content);
         $my_HtmlCleanup     = new HtmlCleanup($art_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $art_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->art_name)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['art_name']        = $request->get('art_name', '');
         $dataInsert['art_namesearch']  = $searchTitle;
         $dataInsert['art_avatar']      = $request->get('art_avatar', '');
         $dataInsert['art_link_video']  = $request->get('art_link_video', '');
         $dataInsert['art_intro']       = $request->get('art_intro', '');
         $dataInsert['art_description'] = $art_description;
         $dataInsert['art_birthday']    = $request->get('art_birthday', '');
         $dataInsert['art_year_debut']  = $request->get('art_year_debut', '');
         $dataInsert['art_background']  = $request->get('art_background', '');
         $dataInsert['art_small_pic']   = $request->get('art_small_pic', ''); 
         $dataInsert['art_awards']      = $request->get('art_awards', '');
         $dataInsert['admin_id']        = $admin_id;
         $dataInsert['lang_id']         = $request->lang_id;

         $dataInsert['art_spotify_url'] = $request->get('art_spotify_url', '');
         $dataInsert['art_itunes_url']  = $request->get('art_itunes_url', '');

         $dataInsert['art_facebook_link']  = $request->get('art_facebook_link', '');
         $dataInsert['art_instagram_link'] = $request->get('art_instagram_link', '');
         $dataInsert['art_youtube_link']   = $request->get('art_youtube_link', '');
         $dataInsert['art_twitter_link']   = $request->get('art_twitter_link', '');
         $dataInsert['art_tiktok_link']    = $request->get('art_tiktok_link', '');
         
         DB::beginTransaction();
         $Artist = Artist::create($dataInsert);
         if(isset($Artist) && $Artist->art_id > 0){                              
             // art_image
             $arrImage = $request->picture_data;
             try {
                 if(isset($arrImage) && $arrImage != null){
                     foreach ($arrImage as $key => $value) {
                        $dataInsertImg                     = [];
                        $dataInsertImg['arg_image']        = replaceMQ($value);
                        $dataInsertImg['arg_image_thumps'] = replaceMQ($value);                         
                        $dataInsertImg['arg_artist_id']    = intval($Artist->art_id);
                        $dataInsertImg['arg_create_date']  = time();
                        ArtistGallery::create($dataInsertImg);
                     }
                 }
                 DB::commit();
                 Session::flash('success','Thêm mới thành công!');
             } catch (\Throwable $th) {
               dd($th);
                 DB::rollBack();
                 Session::flash('error','Thêm mới không thành công!');
             }
         }
      }
      return view("admin.module.artist.add", $data);
   }

   public function edit($id, Request $request)
   {
      $data           = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;
      $data['arrJob'] =  $this->getJobs();

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['artist']) && (isset($role['artist']['accept']) && $role['artist']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  

      $artist = Artist::where('art_id', $id)->first()->toArray();
      $data['artist'] = $artist;
      
      if($request->action == "update"){
         $validator = Validator::make($request->all(), [
             'art_name'        => 'max:255|required', 
             'art_avatar'      => 'max:255|required',
             'art_description' => 'required'
         ],[
             'art_name.required'        => 'Bạn nhập tiêu đề.', 
             'art_avatar.required'      => 'Bạn chưa chọn ảnh đại diện.',
             'art_description.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/artist/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         // artist
         $dataInsert['art_active']      = intval($request->art_active ?? $artist['art_active']); 
         $dataInsert['art_hot']         = intval($request->art_hot ?? $artist['art_hot']);  
          
         $art_content        = $request->get('art_description', $artist['art_description']);
         $art_content        = stripslashes($art_content);
         $my_HtmlCleanup     = new HtmlCleanup($art_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $art_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->art_name ?? $artist['art_name'])));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['art_name']           = $request->get('art_name', $artist['art_name']);
         $dataInsert['art_namesearch']     = $searchTitle;
         $dataInsert['art_avatar']         = $request->get('art_avatar', $artist['art_avatar']);
         $dataInsert['art_background']     = $request->get('art_background', $artist['art_background']);         
         $dataInsert['art_small_pic']      = $request->get('art_small_pic', $artist['art_small_pic']); 
         $dataInsert['art_link_video']     = $request->get('art_link_video', $artist['art_link_video']);
         $dataInsert['art_intro']          = $request->get('art_intro', $artist['art_intro']);
         $dataInsert['art_description']    = $art_description;
         $dataInsert['art_job']            = $request->get('art_job', $artist['art_job']); 
         $dataInsert['art_year_debut']     = $request->get('art_year_debut', $artist['art_year_debut']); 
         $dataInsert['art_birthday']       = $request->get('art_birthday', $artist['art_birthday']); 
         $dataInsert['art_awards']         = $request->get('art_awards', $artist['art_awards']);
         $dataInsert['edit_admin']         = $admin_id;
         $dataInsert['lang_id']            = $request->get('lang_id', $artist['lang_id']); 
         $dataInsert['art_spotify_url']    = $request->get('art_spotify_url', $artist['art_spotify_url']);
         $dataInsert['art_itunes_url']     = $request->get('art_itunes_url', $artist['art_itunes_url']);
         $dataInsert['art_facebook_link']  = $request->get('art_facebook_link', $artist['art_facebook_link']);
         $dataInsert['art_instagram_link'] = $request->get('art_instagram_link', $artist['art_instagram_link']);
         $dataInsert['art_youtube_link']   = $request->get('art_youtube_link', $artist['art_youtube_link']);
         $dataInsert['art_twitter_link']   = $request->get('art_twitter_link', $artist['art_twitter_link']);
         $dataInsert['art_tiktok_link']    = $request->get('art_tiktok_link', $artist['art_tiktok_link']);
         $dataInsert['art_last_modify'] = time();
         // dd($dataInsert);
         DB::beginTransaction();
         try {
            $ArtistID = Artist::where("art_id","=",$id)->update($dataInsert);

            if($ArtistID){
                $arrImage = $request->picture_data;
                if($arrImage){
                    try {
                       if(isset($arrImage) && $arrImage != null){
                           foreach ($arrImage as $key => $value) {
                              $dataInsertImg           = [];
                              $dataInsertImg['arg_image']        = replaceMQ($value);
                              $dataInsertImg['arg_image_thumps'] = replaceMQ($value);                         
                              $dataInsertImg['arg_artist_id']    = intval($id);
                              $dataInsertImg['arg_create_date']  = time();
                              ArtistGallery::create($dataInsertImg);
                           }
                       }
                       DB::commit();
                       Session::flash('success','Sửa thành công!');                       
                   } catch (\Throwable $th) {
                       DB::rollBack();
                       Session::flash('error','Sửa không thành công!');
                   }  
               }
            }  
            DB::commit();
            Session::flash('success','Sửa thành công!'); 
            return redirect('/admin/artist/'); 
         } catch (Exception $e) {
            DB::rollBack();
            Session::flash('error','Sửa không thành công!');
            return redirect('/admin/artist/');
         }
         
      } 

      return view('admin.module.artist.edit', ['id' => $id])->with($data);
   }

   public function destroy($id)
   {
      $Artist = Artist::where("art_id","=",$id);

      if ($Artist)
      {
         $Artist->delete();
         Session::flash('success', 'Xóa tin thành công!');
      }

      return redirect('/admin/artist/');

   }

   public function upload(Request $request){
      $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0]; 

      if ($request->hasFile('Filedata')) {
         $image                  = $request->file('Filedata');

         $validator = Validator::make($request->all(), [
             'Filedata'   => 'mimes:jpeg,jpg,png,gif'
         ],[
             'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
         ]);

         if ($validator->fails()) {
             $dataReturn['msg'] = 'File upload không tồn tại!';
         }
         //upload images
         $dataReturn['filename'] = env("CDN_IMAGE") . 'artist/'.ImageManager::upload($image, 'artist');
         list($width, $height)   = getimagesize($image->getRealPath());
         $dataReturn['width']    = $width;
         $dataReturn['height']   = $height;
         $dataReturn['status']   = true;
         $dataReturn['filesize'] = $image->getClientSize();
         $dataReturn['url'] = $dataReturn['filename'];
      }else{
          $dataReturn['msg'] = 'File upload không tồn tại!';
      }

      return json_encode($dataReturn);
    }
 
   public function changeOrder(Request $request){
     $id     = $request->get('id');
     $value  = $request->get('value', 0);
     $return = ['status'=> 0, 'msg' => ''];

     if($id){
         try {
             $update = Artist::where('art_id', $id)->update([
                 'art_order' => intval($value),
             ]);
             $return['msg'] = 'Update Thành Công';
         } catch (Exception $e) {
             $return['msg'] = 'Update Thất bại';    
             $return['status'] = 1;
         }            
     }
     return $return;
   }
   public function changestatus(Request $request)
   {
      $id     = intval($request->get('id'));
      $status = intval($request->get('status'));
      $news    = Artist::where("art_id","=",$id);

      if ($news)
      {
         $news->update(['art_active' => $status] ); 
      }
      $return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
      return $return;
    }

   public function setHotArtist(Request $request){

      $id = $request->get('idata', 0);

      if($id > 0 ){
         $Artist = Artist::where('art_id', $id)->first(); 
         if($Artist->art_hot == 1){
             Artist::WHERE('art_id', $id)->update(['art_hot' => 0, 'art_last_modify' => time()]);
         }else{
             Artist::WHERE('art_id', $id)->update(['art_hot' => 1, 'art_last_modify' => time()]);
         }
         return ['status' => 0, 'msg' => 'Thành công'];
      }else{
         return ['status' => 1, 'msg' => 'Thất bại'];
      }
   }
  
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }
   public function getJobs()
   {
     $lang = [
         1 => 'Ca sĩ',
         2 => 'Diễn Viên'
     ];

     return $lang;
   }
}    

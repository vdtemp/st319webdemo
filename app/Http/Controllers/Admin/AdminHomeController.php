<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class AdminHomeController extends Controller
{
	public function __construct(){

	}

   	public function index(Request $request){
   		// $this->createSequence();
		return view('admin.home');
    }
}

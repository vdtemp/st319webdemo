<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser;
use App\Models\BannerPosition;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;

class AdminBannerPositionController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new BannerPosition;  
      $params     = [];
      $name     = $request->get('name' ,'');
      if($name != ''){
        $query  = $query->where('banp_name', $name);
      }
      $position = $request->get('position', '');
      if($position != ''){
        $query  = $query->where('banp_position', $position);
      }
      $status   = $request->get('status', -1);
      if($status > 0){
        $query  = $query->where('banp_status', $status);
      }
      $Positions = $query->get()->toArray();

      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];
      
      $data['params']            = $params;
      $data['arrStatus']         = $arrStatus; 
      $data['page_filter_url']   = '/admin/positionBanner';

      $data['params']            = $params; 
      $data['Positions']         = $Positions;

      return view('admin.module.banner.positionList')->with($data);
   }

   public function create(Request $request)
   {
      $params     = [];
      $where      = array();
      $data       = [];   
   
      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [ 
             'banp_position'  => 'required',
         ],[  
             'banp_position.required'       => 'Bạn nhập mã vị trí.',  
         ]);

         if ($validator->fails()) {
            return redirect(route('admin.positionBanner.create'))->withErrors($validator)->withInput()->with($data);
         }

         $check = BannerPosition::where('banp_position', $request->banp_position)->first();

         if($check){
            Session::flash('error', 'Mã vị trí đã tồn tại!');
            return redirect(route('admin.positionBanner.create'))->withInput()->with($data);
         }

         $dataInsert = [];  
         // news
         $dataInsert['banp_name']        = $request->banp_name; 
         $dataInsert['banp_position']    = $request->banp_position; 
         $dataInsert['banp_description'] = $request->banp_description ;
         $dataInsert['banp_active']      = 1; 
         $dataInsert['banp_date_create'] = time(); 

         $newID = BannerPosition::create($dataInsert);
         if(isset($newID) && $newID->banp_id > 0){ 
             Session::flash('success', 'Thêm mới thành công!');
             return redirect(route('admin.positionBanner.index'));
         }
      }
      return view('admin.module.banner.positionAdd')->with($data);
   }

   public function edit($id, Request $request)
   {
      $params     = [];
      $where      = array();
      $data       = [];   
      
      $banPos = BannerPosition::where("banp_id", $id)->first()->toArray();
      $data['banPos'] = $banPos;

      if($request->action == "update"){
         $validator = Validator::make($request->all(), [ 
             'banp_position'  => 'required',
         ],[  
             'banp_position.required'       => 'Bạn nhập mã vị trí.',  
         ]);

         if ($validator->fails()) {
            return redirect(route('admin.positionBanner.edit', ['id' => $id]))->withErrors($validator)->withInput()->with($data);
         }

         if($banPos['banp_position'] != $request->banp_position){
             $check = BannerPosition::where('banp_position', $request->banp_position)->first();

            if($check){
               Session::flash('error', 'Mã vị trí đã tồn tại!');
               return redirect(route('admin.positionBanner.edit', ['id' => $id]))->withInput()->with($data);
            }   
         }        

         $updateData = [];  
         // news
         $updateData['banp_name']        = $request->banp_name; 
         $updateData['banp_position']    = $request->banp_position;
         $updateData['banp_description'] = $request->banp_description ; 

         try{
             $BannerPosition = BannerPosition::where('banp_id', $id)->update($updateData);
             Session::flash('success', 'Success');     
             return redirect(route('admin.positionBanner.index'));
         } catch (Exception $e) {
             Session::flash('error', 'False'); 
         }
      }

      return view('admin.module.banner.positionEdit')->with($data);
   }

   public function destroy($id)
   {  
      $BannerPosition = BannerPosition::where("banp_id","=",$id)->first();
      if ($BannerPosition)
      {
         $checkBanner = Banner::where('ban_position', $BannerPosition->banp_position)->get();
         if($checkBanner){
            Session::flash('error', 'Vị trí đang có banner hiển thị, không được xóa!');
            return redirect(route('admin.positionBanner.index'));
         }

         BannerPosition::where("banp_id","=",$id)->delete();
         Session::flash('success', 'Xóa vị trí thành công!');
      }

      return redirect(route('admin.positionBanner.index'));

   }
}    
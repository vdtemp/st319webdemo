<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser; 
use App\Models\UserOrder; 
use App\Models\UserOrderDetail;
use App\Models\Product;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;
use App\Helper\HtmlCleanup;


class AdminOrdersController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new UserOrder;         
      $page       = intval($request->input('page',1));
  
 
      $uso_status = $request->get('uso_status', -1);
      $params['uso_status'] = $uso_status;
      if($uso_status > 0){ 
         $query = $query->where('uso_status',$uso_status);
      }

      $uso_user_name = $request->get('name', '');
      $params['name'] = $uso_user_name;
      if($uso_user_name != ''){ 
         $query = $query->where('uso_user_name', 'LIKE', "%{$uso_user_name}%");
      }

      $uso_user_phone = $request->get('phone', '');
      $params['phone'] = $uso_user_phone;
      if($uso_user_phone != ''){ 
         $query = $query->where('uso_user_phone', 'LIKE', "%{$uso_user_phone}%");
      }

      $uso_user_email = $request->get('email', '');
      $params['email'] = $uso_user_email;
      if($uso_user_email != ''){ 
         $query = $query->where('uso_user_email', 'LIKE', "%{$uso_user_email}%");
      }

      $UserOrder  = $query->with(['detail'])->orderBy('uso_id', 'desc')->paginate(50);

      /*Sản phẩm*/
      $arrProduct = [];
      foreach($UserOrder as $order){
         foreach ($order['detail'] as $detail) {
            if(!in_array($detail->uod_product_id, $arrProduct)){
               $arrProduct[] = $detail->uod_product_id;   
            }           
         }
      }
      $products = Product::whereIn('pro_id', $arrProduct)->select(['pro_id', 'pro_name', 'pro_image', 'pro_price'])->get()->keyBy('pro_id')->toArray();

      $arrStatus               = [0 => 'Chưa xác nhận', 1 => 'Xác nhận', 100 => 'Hủy']; 
      $data['params']          = $params;
      $data['arrLang']         =  $this->getLang();
      $data['arrStatus']       = $arrStatus;  
      
      $data['page_filter_url'] = '/admin/order';
      $data['page']            = $page;
      $data['UserOrder']       = $UserOrder;
      $data['products']        = $products;

      return view('admin.module.orders.list')->with($data);
   }

   public function confirm($id)
   {
      $UserOrder = UserOrder::where("uso_id","=",$id);

      if ($UserOrder)
      {
         $UserOrder->update(['uso_status' => 1] );
         Session::flash('success', 'Xác nhận đơn thành công!');
      }

      return redirect('/admin/order/');

   }
   public function destroy($id)
   {
      $UserOrder = UserOrder::where("uso_id","=",$id);

      if ($UserOrder)
      {
         $UserOrder->update(['uso_status' => 100] );
         Session::flash('success', 'Hủy đơn thành công!');
      }

      return redirect('/admin/order/');

   }   
  
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }
 
}    

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser;  
use App\Models\Campaign;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;
use App\Helper\HtmlCleanup;


class AdminCampaignController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new Campaign;      
      $page = intval($request->input('page',1));

      $camp_title = $request->get('camp_title', '');
      if($camp_title != ''){
         $searchTitle = removeAccent(strtolower(replaceMQ($camp_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         $query = $query->where('camp_search',$searchTitle);
      }

      $camp_hot = $request->get('camp_hot', -1);
      if($camp_hot > 0){ 
         $query = $query->where('camp_hot',$camp_hot);
      }

      $camp_status = $request->get('camp_status', -1);
      if($camp_status > 0){ 
         $query = $query->where('camp_status',$camp_status);
      }

      $lang_id = $request->get('lang_id', '');
      if($lang_id > 0){ 
         $query = $query->where('lang_id',$camp_status);
      }
      $campaign  = $query->orderBy('camp_id', 'desc')->paginate(15);
      
      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];
      $arrHot    = [0 => 'no Hot', 1 => 'Hot']; 

      $params = [
         'lang_id'     => $lang_id,
         'camp_title'  => $camp_title,
         'camp_hot'    => $camp_hot,
         'camp_status' => $camp_status,
      ];

      $data['params']          = $params;
      $data['arrLang']         =  $this->getLang();
      $data['arrStatus']       = $arrStatus; 
      $data['arrHot']          = $arrHot; 
      $data['arrUser']         = []; 
      
      $data['page_filter_url'] = '/admin/campaign';
      $data['page']            = $page;
      $data['campaign']            = $campaign;

      return view('admin.module.campaign.list')->with($data);
   }

   public function create(Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['campaign']) && (isset($role['campaign']['accept']) && $role['campaign']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  
      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [ 
             'camp_title'       => 'max:255|required',
             'camp_intro'       => 'required',
             'camp_picture'     => 'max:255|required',
             'camp_description' => 'required'
         ],[ 
             'camp_title.required'       => 'Bạn chưa nhập tiêu đề.',
             'camp_intro.required'       => 'Bạn chưa nhập mô tả.',
             'camp_picture.required'     => 'Bạn chưa chọn ảnh đại diện.',
             'camp_description.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/campaign/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         $dataInsert['camp_status']      = intval($request->camp_status); 
         $dataInsert['camp_hot']         = intval($request->camp_hot); 
         $dataInsert['camp_order']       = intval($request->camp_order); 
         $dataInsert['camp_modified_date'] = time();          
         $dataInsert['camp_created_date'] = time();
          
         $camp_content        = $request->get('camp_description', '');
         $camp_content        = stripslashes($camp_content);
         $my_HtmlCleanup      = new HtmlCleanup($camp_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $camp_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->camp_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['camp_title']        = $request->get('camp_title', '');
         $dataInsert['camp_search']       = $searchTitle;
         $dataInsert['camp_picture']      = $request->get('camp_picture', '');
         $dataInsert['camp_link_video']   = $request->get('camp_link_video', '');
         $dataInsert['camp_intro']        = $request->get('camp_intro', '');
         $dataInsert['camp_description']  = $camp_description;
         $dataInsert['camp_meta_title']   = $request->get('camp_meta_title', '');
         $dataInsert['camp_meta_desc']    = $request->get('camp_meta_desc', '');
         $dataInsert['camp_meta_keyword'] = $request->get('camp_meta_keyword', '');  

         $dataInsert['admin_id']    = $admin_id;
         $dataInsert['lang_id']     = intval($request->get('lang_id', 1));  
         try {
            $Campaign = Campaign::create($dataInsert);
            if(isset($Campaign) && $Campaign->camp_id > 0){
                 Session::flash('success','Thêm mới thành công!'); 
                 return redirect(route('admin.campaign.index'));         
            }else{
               Session::flash('error','Thêm mới không thành công!'); 
            }   
         } catch (Exception $e) {
            Session::flash('error','Thêm mới không thành công!'); 
         }
         
      }
      return view("admin.module.campaign.add", $data);
   }

   public function edit($id, Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['campaign']) && (isset($role['campaign']['accept']) && $role['campaign']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      $data['arrLang'][0] = "- Chọn ngôn ngữ -";
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  

      $campaign = Campaign::where('camp_id', $id)->first();
      $data['campaign'] = $campaign;

      if($request->action == "update"){
         $validator = Validator::make($request->all(), [ 
             'camp_title'       => 'max:255|required',
             'camp_intro'       => 'required',
             'camp_picture'     => 'max:255|required',
             'camp_description' => 'required'
         ],[ 
             'camp_title.required'       => 'Bạn nhập tiêu đề.',
             'camp_intro.required'       => 'Bạn nhập mô tả.',
             'camp_picture.required'     => 'Bạn chưa chọn ảnh đại diện.',
             'camp_description.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect(route('admin.campaign.edit', ['id' => $id]))->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         $dataInsert['lang_id']          = intval($request->lang_id); 
         $dataInsert['camp_status']      = intval($request->camp_status); 
         $dataInsert['camp_hot']         = intval($request->camp_hot); 
         $dataInsert['camp_order']       = intval($request->camp_order); 
         $dataInsert['camp_modified_date'] = time();          
          
         $camp_content        = $request->get('camp_description', '');
         $camp_content        = stripslashes($camp_content);
         $my_HtmlCleanup     = new HtmlCleanup($camp_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $camp_description        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->camp_title)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $dataInsert['camp_title']        = $request->get('camp_title', '');
         $dataInsert['camp_search']       = $searchTitle;
         $dataInsert['camp_picture']      = $request->get('camp_picture', '');
         $dataInsert['camp_link_video']   = $request->get('camp_link_video', '');
         $dataInsert['camp_intro']        = $request->get('camp_intro', '');
         $dataInsert['camp_description']  = $camp_description;
         $dataInsert['camp_meta_title']   = $request->get('camp_meta_title', '');
         $dataInsert['camp_meta_desc']    = $request->get('camp_meta_desc', '');
         $dataInsert['camp_meta_keyword'] = $request->get('camp_meta_keyword', ''); 
  
         try {
            $CampID = Campaign::where("camp_id","=",$id)->update($dataInsert);
            Session::flash('success','Sửa thành công!');       
            return redirect('/admin/campaign/'); 
         } catch (Exception $e) {
            Session::error('Lỗi','Sửa không thành công!');
            return redirect('/admin/campaign/');
         }
         
      } 

      return view('admin.module.campaign.edit', ['id' => $id])->with($data);
   }

   public function destroy($id)
   {
      $Campaign = Campaign::where("camp_id","=",$id);

      if ($Campaign)
      {
         $Campaign->delete();
         Session::flash('success', 'Xóa tin thành công!');
      }

      return redirect('/admin/campaign/');

   }

   public function upload(Request $request){
      $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0]; 

      if ($request->hasFile('Filedata')) {
         $image                  = $request->file('Filedata');

         $validator = Validator::make($request->all(), [
             'Filedata'   => 'mimes:jpeg,jpg,png,gif'
         ],[
             'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
         ]);

         if ($validator->fails()) {
             $dataReturn['msg'] = 'File upload không tồn tại!';
         }
         //upload images
         $dataReturn['filename'] = env("CDN_IMAGE") . 'campaign/'.ImageManager::upload($image, 'campaign');
         list($width, $height)   = getimagesize($image->getRealPath());
         $dataReturn['width']    = $width;
         $dataReturn['height']   = $height;
         $dataReturn['status']   = true;
         $dataReturn['filesize'] = $image->getClientSize();
         $dataReturn['url'] = $dataReturn['filename'];
      }else{
          $dataReturn['msg'] = 'File upload không tồn tại!';
      }

      return json_encode($dataReturn);
    }
 
   public function changeOrder(Request $request){
     $id     = $request->get('id');
     $value  = $request->get('value', 0);
     $return = ['status'=> 0, 'msg' => ''];

     if($id){
         try {
             $update = Campaign::where('camp_id', $id)->update([
                 'camp_order' => intval($value),
             ]);
             $return['msg'] = 'Update Thành Công';
         } catch (Exception $e) {
             $return['msg'] = 'Update Thất bại';    
             $return['status'] = 1;
         }            
     }
     return $return;
   }
   public function changestatus(Request $request)
   {
      $id     = intval($request->get('id'));
      $status = intval($request->get('status'));
      $Campaign    = Campaign::where("camp_id","=",$id);

      if ($Campaign)
      {
         $Campaign->update(['camp_status' => $status] ); 
      }
      $return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
      return $return;
    }

   public function setHotCampaign(Request $request){

      $id = $request->get('idata', 0);

      if($id > 0 ){
         $Campaign = Campaign::where('camp_id', $id)->first(); 
         if($Campaign->camp_hot == 1){
             Campaign::WHERE('camp_id', $id)->update(['camp_hot' => 0]);
         }else{
             Campaign::WHERE('camp_id', $id)->update(['camp_hot' => 1]);
         }
         return ['status' => 0, 'msg' => 'Thành công'];
      }else{
         return ['status' => 1, 'msg' => 'Thất bại'];
      }
   }
  
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }
}    

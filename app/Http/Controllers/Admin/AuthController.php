<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminUser;
use App\Repositories\AdminUserRepository;

class AuthController extends Controller
{
	protected $admin_user_repository = "";

	public function __construct(AdminUserRepository $admin_user_repository)
    {
        $this->admin_user_repository = $admin_user_repository;
    }

	public function index()
	{
		$logged = $this->admin_user_repository::isLogged();
		if($logged){
			return redirect()->route('admin.dashboard');
		}

		return view('admin.login');
	}

	public function login(Request $request)
	{
		$this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);
		$username = replaceMQ($request->get('username'));
		$password = replaceMQ($request->get('password'));

		if($username != "" && $password != ""){
			$info = $this->admin_user_repository->checkInfoLogin($username, $password);
			if($info){
				return redirect()->route('admin.dashboard')->with('success','Đăng nhập thành công!');
			}else{
				return redirect()->route('admin.login_fr')->with('error','Thông tin tài khoản không chính xác!');
			}
		}

	}

	public function logout(){
		$this->admin_user_repository->setLogout();
		return redirect()->route('admin.login_fr')->with('success','Đăng xuất thành công!');
	}
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser;  
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\AlbumVideos;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;
use App\Helper\HtmlCleanup;
use App\Repositories\ArtistRepository;


class AdminProductController extends Controller
{  
   protected $ArtistRepository;
   public function __construct(ArtistRepository $ArtistRepository){
      $this->ArtistRepository = $ArtistRepository;
   }

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new Product;        
      $pro_name = $request->get('pro_name', '');
      $page = intval($request->input('page',1));
      if($pro_name != ''){
         $searchTitle = removeAccent(strtolower(replaceMQ($pro_name)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         $query = $query->where('pro_namesearch',$searchTitle);
      }

      $pro_hot = $request->get('pro_hot', -1);
      if($pro_hot > 0){ 
         $query = $query->where('pro_hot',$pro_hot);
      }

      $pro_status = $request->get('pro_status', -1);
      if($pro_status > 0){ 
         $query = $query->where('pro_status',$pro_status);
      }



      $product  = $query->orderBy('pro_id', 'desc')->paginate(15);
      
      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];
      $arrHot    = [0 => 'Không chọn', 1 => 'Chọn']; 

      $data['params']    = $params;
      $data['arrLang']   =  $this->getLang();
      $data['arrType']   =  $this->getType();
      $data['arrStatus'] = $arrStatus; 
      $data['arrHot']    = $arrHot;
      $data['arrArtist'] = $this->getArtist(); 
      
      $data['page_filter_url'] = '/admin/product';
      $data['page']            = $page;
      $data['products']        = $product;

      return view('admin.module.product.list')->with($data);
   }

   public function create(Request $request)
   {
      $data            = [];
      $admin_id       = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role           = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck      = 0;
      $data['accept'] = 0;

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['product']) && (isset($role['product']['accept']) && $role['product']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  

      $data['arrType']   = $this->getType();
      $data['arrArtist'] = $this->getArtist(); 

      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [ 
             'pro_name'    => 'max:255|required',
             'pro_content' => 'required',
             'pro_detail'  => 'required',
             'pro_image'   => 'max:255|required',
             'pro_price'   => 'required'
         ],[ 
             'pro_name.required'    => 'Bạn nhập tên.',
             'pro_content.required' => 'Bạn nhập mô tả.',
             'pro_detail.required'  => 'Bạn nhập chi tiết.',
             'pro_image.required'   => 'Bạn chưa chọn ảnh đại diện.',
             'pro_price.required'   => 'Bạn nhập giá sản phẩm.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/product/create')->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         // news
         $dataInsert['pro_status']       = intval($request->pro_status); 
         $dataInsert['pro_sale']         = $request->get('pro_sale');
         $dataInsert['pro_hot']          = intval($request->pro_hot);  
         $dataInsert['pro_created_date'] = time();
                   
         $pro_content        = $request->get('pro_content', '');
         $pro_content        = stripslashes($pro_content);
         $my_HtmlCleanup     = new HtmlCleanup($pro_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $pro_content        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->pro_name)));
         $searchTitle = cleanKeywordSearch($searchTitle);
         
         $pro_detail = $request->get('pro_detail', '');
         $pro_detail = stripslashes($pro_detail);

         $dataInsert['pro_name']       = $request->get('pro_name', '');
         $dataInsert['pro_namesearch'] = $searchTitle;
         $dataInsert['pro_image']      = $request->get('pro_image', '');
         $dataInsert['pro_link_video'] = $request->get('pro_link_video', ''); 
         $dataInsert['pro_content']    = $pro_content;
         $dataInsert['pro_detail']     = $pro_detail;
         $dataInsert['pro_artist_id']  = $request->get('pro_artist_id', '');
         $dataInsert['pro_type']       = $request->get('pro_type', ''); 
         $dataInsert['pro_price']      = $request->get('pro_price', ''); 
         $dataInsert['pro_amount']     = $request->get('pro_amount', '');

         $dataInsert['admin_id']       = $admin_id;
         $dataInsert['lang_id']        = $request->lang_id;

         DB::beginTransaction();
         $product = Product::create($dataInsert);
         if(isset($product) && $product->pro_id > 0){                              
             // pro_image
             $arrImage = $request->picture_data;
             try {
                 if(isset($arrImage) && $arrImage != null){
                     foreach ($arrImage as $key => $value) {
                        $dataInsertImg                     = [];
                        $dataInsertImg['proi_image']        = replaceMQ($value);
                        $dataInsertImg['proi_status']       = 1;                         
                        $dataInsertImg['proi_product_id']    = intval($product->pro_id);
                        $dataInsertImg['proi_created_date']  = time();
                        ProductImage::create($dataInsertImg);
                     }
                 }
                 DB::commit();
                 Session::flash('success','Thêm mới thành công!');
                 return redirect('/admin/product/'); 
             } catch (\Throwable $th) { 
                 DB::rollBack();
                 Session::flash('error','Thêm mới không thành công!');
             }
         }
      }
      return view("admin.module.product.add", $data);
   }

   public function edit($id, Request $request)
   {
      $data              = [];
      $admin_id          = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;
      $role              = isset(app()->ADMIN_INFO->adm_role) ? app()->ADMIN_INFO->adm_role : '';
      $roleCheck         = 0;
      $data['accept']    = 0;

      $data['arrType']   = $this->getType();
      $data['arrArtist'] = $this->getArtist(); 

      if($role != ""){
         $role = json_decode($role,1);
         if(count($role) > 0 && isset($role['product']) && (isset($role['product']['accept']) && $role['product']['accept'] == 1)){
             $roleCheck = 1;
         }
      }
      if(app()->ADMIN_INFO['adm_isadmin'] == 1){
         $roleCheck = 1;
      }
      $data['accept'] = $roleCheck; 

      $dataLang = $this->getLang();
      foreach ($dataLang as $key => $value) {
         $data['arrLang'][$key] = $value;
      }  

      $product = Product::where('pro_id', $id)->first()->toArray();
      $data['product'] = $product;
      
      if($request->action == "update"){
         $validator = Validator::make($request->all(), [
             'pro_name'    => 'max:255|required', 
             'pro_image'   => 'max:255|required',
             'pro_content' => 'required'
         ],[
             'pro_name.required'    => 'Bạn nhập tiêu đề.', 
             'pro_image.required'   => 'Bạn chưa chọn ảnh đại diện.',
             'pro_content.required' => 'Bạn nhập nội dung bài viết.'
         ]);

         if ($validator->fails()) {
             return redirect('/admin/product/edit', ['id' => $id])->withErrors($validator)->withInput();
         }

         $dataInsert = [];
         // product
         $dataInsert['pro_status'] = intval($request->pro_status ?? $product['pro_status']); 
         $dataInsert['pro_hot']    = intval($request->pro_hot ?? $product['pro_hot']);  
         $dataInsert['pro_sale']   = $request->pro_sale ?? $product['pro_sale'];
          
         $pro_content        = $request->get('pro_content', $product['pro_content']);
         $pro_content        = stripslashes($pro_content);
         $my_HtmlCleanup     = new HtmlCleanup($pro_content);
         $my_HtmlCleanup->setIgnoreCheckProtocol();
         $my_HtmlCleanup->clean();
         $pro_content        = $my_HtmlCleanup->output_html;

         $searchTitle = removeAccent(strtolower(replaceMQ($request->pro_name ?? $product['pro_name'])));
         $searchTitle = cleanKeywordSearch($searchTitle);

         $pro_detail = $request->get('pro_detail', '');
         $pro_detail = stripslashes($pro_detail);
         
         $dataInsert['pro_name']       = $request->get('pro_name', $product['pro_name']);
         $dataInsert['pro_namesearch'] = $searchTitle;
         $dataInsert['pro_image']      = $request->get('pro_image', $product['pro_name']);
         $dataInsert['pro_link_video'] = $request->get('pro_link_video', ''); 
         $dataInsert['pro_content']    = $pro_content;
         $dataInsert['pro_detail']     = $pro_detail;
         $dataInsert['pro_artist_id']  = $request->get('pro_artist_id', $product['pro_artist_id']);
         $dataInsert['pro_type']       = $request->get('pro_type',  $product['pro_type']);  
         $dataInsert['pro_date']       = $request->get('pro_date',  $product['pro_date']);  
         
         $dataInsert['lang_id']        = $request->lang_id;
          
         DB::beginTransaction();
         try {
            $productID = Product::where("pro_id","=",$id)->update($dataInsert);

            if($productID){
                $arrImage = $request->picture_data;
                if($arrImage){
                    try {
                       if(isset($arrImage) && $arrImage != null){
                           foreach ($arrImage as $key => $value) {
                              $dataInsertImg                     = [];
                              $dataInsertImg['proi_image']        = replaceMQ($value);
                              $dataInsertImg['proi_status']       = 1;                         
                              $dataInsertImg['proi_product_id']    = intval($id);
                              $dataInsertImg['proi_created_date']  = time();
                              ProductImage::create($dataInsertImg);
                           }
                       }
                       DB::commit();
                       Session::flash('success','Sửa thành công!');                       
                   } catch (\Throwable $th) {
                       DB::rollBack();
                       Session::flash('error','Sửa không thành công!');
                   }  
               }
            }  
            DB::commit();
            Session::flash('success','Sửa thành công!'); 
            return redirect('/admin/product/'); 
         } catch (Exception $e) {
            DB::rollBack();
            Session::flash('error','Sửa không thành công!');
            return redirect('/admin/product/');
         }
         
      } 

      return view('admin.module.product.edit', ['id' => $id])->with($data);
   }

   public function destroy($id)
   {
      $product = Product::where("pro_id","=",$id);

      if ($product)
      {
         $product->delete();
         Session::flash('success', 'Xóa tin thành công!');
      }

      return redirect('/admin/product/');

   }

   public function upload(Request $request){
      $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0]; 

      if ($request->hasFile('Filedata')) {
         $image                  = $request->file('Filedata');

         $validator = Validator::make($request->all(), [
             'Filedata'   => 'mimes:jpeg,jpg,png,gif'
         ],[
             'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
         ]);

         if ($validator->fails()) {
             $dataReturn['msg'] = 'File upload không tồn tại!';
         }
         //upload images
         $dataReturn['filename'] = env("CDN_IMAGE") . 'product/'.ImageManager::upload($image, 'product');
         list($width, $height)   = getimagesize($image->getRealPath());
         $dataReturn['width']    = $width;
         $dataReturn['height']   = $height;
         $dataReturn['status']   = true;
         $dataReturn['filesize'] = $image->getClientSize();
         $dataReturn['url'] = $dataReturn['filename'];
      }else{
          $dataReturn['msg'] = 'File upload không tồn tại!';
      }

      return json_encode($dataReturn);
    }
 
   public function changeOrder(Request $request){
     $id     = $request->get('id');
     $value  = $request->get('value', 0);
     $return = ['status'=> 0, 'msg' => ''];

     if($id){
         try {
             $update = Product::where('pro_id', $id)->update([
                 'pro_order' => intval($value),
             ]);
             $return['msg'] = 'Update Thành Công';
         } catch (Exception $e) {
             $return['msg'] = 'Update Thất bại';    
             $return['status'] = 1;
         }            
     }
     return $return;
   }
   public function changestatus(Request $request)
   {
      $id     = intval($request->get('id'));
      $status = intval($request->get('status'));
      $news    = Product::where("pro_id","=",$id);

      if ($news)
      {
         $news->update(['pro_status' => $status] ); 
      }
      $return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
      return $return;
    }

   public function setHotproduct(Request $request){

      $id = $request->get('idata', 0);

      if($id > 0 ){
         $product = Product::where('pro_id', $id)->first(); 
         if($product->pro_hot == 1){
             Product::WHERE('pro_id', $id)->update(['pro_hot' => 0, 'pro_last_modify' => time()]);
         }else{
             Product::WHERE('pro_id', $id)->update(['pro_hot' => 1, 'pro_last_modify' => time()]);
         }
         return ['status' => 0, 'msg' => 'Thành công'];
      }else{
         return ['status' => 1, 'msg' => 'Thất bại'];
      }
   }
  
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }
   public function getType()
   {
     $lang = [
         1 => 'Album',
         2 => 'Singles & EPs ',
         3 => 'Fimography'
     ];

     return $lang;
   }

   public function getArtist()
   {
      $result = $this->ArtistRepository->getArtists(false);
      $return = [];
      foreach ($result as $key => $value) {
         $return[$value['art_id']] = $value['art_name'];
      }

      return $return;
   }


   /*Thêm ds bài hát vào album*/

   public function addMusic(Request $request)
   {      
      $id = $request->get('id', 0);
      // Danh sách album 
      $product = Product::where('pro_type', 1)->select(['pro_id', 'pro_name'])->get()->toArray();
      $albums = [];
      foreach ($product as $key => $value) {
         $albums[$value['pro_id']] = $value['pro_name'];
      };

      $data = [
         'id'      => $id,
         'albums'  => $albums,
      ];

      $action = $request->get('action');
      if($action == 'addMusic'){
         $albv_album_id  = $request->get('albv_album_id', '');
         $albv_title   = $request->get('albv_title', []);
         $albv_itunes  = $request->get('albv_itunes', []);
         $albv_spotify = $request->get('albv_spotify', []);
         $i = 0;
         $importArr = [];
         foreach ($albv_title as $key => $value) {
            
            $importArr[$i] = [
               'albv_artist_id'   => 0,
               'albv_album_id'    => $albv_album_id,
               'albv_title'       => $value,
               "albv_picture"     => '',
               "albv_videoUrl"    => '',
               "albv_active"      => 1,
               "albv_date_create" => time(),
               'albv_type'        => 1,
               'albv_itunes'      => $albv_itunes[$key],
               'albv_spotify'     => $albv_spotify[$key],
            ];

            $i++;
         }

         $insertId = AlbumVideos::insert($importArr);
         if($insertId){
            Session::flash('success', 'Thêm thành công!');
            return redirect(route('admin.product.addMusic'));
         }else{
            Session::flash('error', 'thêm mới không thành công!');
            return back()->withInput();
         }
      }
      return view('admin.module.product.addMusic')->with($data);  
   }
}    

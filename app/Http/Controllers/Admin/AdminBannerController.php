<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Language;
use App\Models\Type;
use App\Models\AdminUser;
use App\Models\BannerPosition;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;

class AdminBannerController extends Controller
{
   public function __construct(){}

   public function index(Request $request){
      
      $params     = [];
      $where      = array();
      $data       = [];   
      $query      = new Banner;  
      
      $name     = $request->get('name' ,'');
      if($name != ''){
        $query  = $query->where('ban_name', $name);
      }
      $params['name'] = $name;

      $position = $request->get('ban_position', '');
      if($position != ''){
        $query  = $query->where('ban_position', $position);
      }
      $params['ban_position'] = $position;

      $status   = $request->get('ban_status', -1);
      if($status > 0){
        $query  = $query->where('ban_active', $status);
      }
      $params['ban_status'] = $status;

      $type   = $request->get('ban_type', -1);
      if($type > 0){
        $query  = $query->where('ban_type', $type);
      } 
      $params['ban_type'] = $type;
      
      $banners  = $query->get()->toArray();

      $arrStatus = [0 => 'Không duyệt', 1 => 'Duyệt'];
      
      $data['arrPosition'] = $this->getPosition();
      $data['arrType']     = $this->getType();

      $data['params']            = $params;
      $data['arrStatus']         = $arrStatus; 
      $data['page_filter_url']   = '/admin/banner';

      $data['params']            = $params; 
      $data['banners']           = $banners;

      return view('admin.module.banner.list')->with($data);
   }

   public function create(Request $request)
   {
      $params     = [];
      $where      = array();
      $data       = [];   
      $data['arrPosition'] = $this->getPosition();
      $data['arrType']     = $this->getType();
      
      if($request->action == "insert"){
         $validator = Validator::make($request->all(), [ 
             'ban_position'  => 'required',
             'ban_name'  => 'required',
             'ban_type'  => 'required',
             'ban_picture' => 'required',
         ],[  
             'ban_position.required'       => 'Bạn nhập mã vị trí.',  
             'ban_name.required'       => 'Bạn nhập tên banner.',  
             'ban_type.required'       => 'Bạn nhập loại banner.',  
                'ban_picture.required'     => 'Bạn chưa chọn ảnh', 
         ]);

         if ($validator->fails()) {
            return redirect(route('admin.banner.create'))->withErrors($validator)->withInput()->with($data);
         } 

         $dataInsert                    = [];
         $dataInsert['ban_name']        = $request->ban_name; 
         $dataInsert['ban_position']    = $request->ban_position; 
         $dataInsert['ban_type']        = $request->ban_type; 
         $dataInsert['ban_description'] = $request->ban_description ;
         $dataInsert['ban_active']      = $request->ban_active; 
         $dataInsert['ban_date_create'] = time(); 
         $dataInsert['ban_picture']     = $request->ban_picture;

         $newID = Banner::create($dataInsert);
         if(isset($newID) && $newID->banp_id > 0){ 
             Session::flash('success', 'Thêm mới thành công!');
             return redirect(route('admin.banner.index'));
         }
      }
      return view('admin.module.banner.add')->with($data);
   }

   public function edit($id, Request $request)
   {
      $actual_link = 'http://'.$_SERVER['HTTP_HOST'].'/'; 

      $data            = [];

      $dataType = $this->getType();

      $data['arrType'][0] = "- Chọn loại Banner -";
      foreach ($dataType as $key => $value) {
         $data['arrType'][$key] = $value;
      }  

      $banner = Banner::where('ban_id', $id)->first()->toArray();
      if(!$banner){
         Session::flash('success', 'Thông tin không chính xác!');
         return redirect(route('admin.banner.index'));
      }

      $data['banner'] = $banner;

      $BannerPosition  = BannerPosition::where('banp_active', 1)->get();
      $data['arrPosition'][0] = "- Chọn -";
      foreach($BannerPosition as $banpos){
         $data['arrPosition'][$banpos['banp_position']] = $banpos['banp_name'];
      }

      if($request->action == "update"){
         $validator = Validator::make($request->all(), [ 
             'ban_type'        => 'required|numeric|not_in:0',
             'ban_name'       => 'max:255|required',
         ],[ 
             'ban_type.not_in'        => 'Bạn chưa chọn loại banner.',
             'ban_name.required'       => 'Bạn nhập tiêu đề.',  
         ]);

         if ($validator->fails()) {
             return redirect(route('admin.banner.edit',['id' => $id]))->withErrors($validator->messages())->withInput();
         }

         $updateData = [];  
         // news
         $updateData['ban_name']         = $request->ban_name ?? $banner['ban_name']; 
         $updateData['ban_type']         = $request->ban_type ?? $banner['ban_type']; 
         $updateData['ban_description']  = $request->ban_description ?? $banner['ban_description']; 
         $updateData['ban_picture']      = $request->ban_picture ?? $banner['ban_picture']; 
         $updateData['ban_active']       = 1 ?? $banner['ban_active'];  
         $updateData['ban_created_date'] = time() ?? $banner['ban_created_date'];
         $updateData['ban_position']     = $request->ban_position ?? $banner['ban_position']; 
         $updateData['ban_order']        = $request->ban_order ?? $banner['ban_order'];
         // dd($updateData);
         try{
             $Banner = Banner::where('ban_id', $id)->update($updateData);

             Session::flash('success', 'Success');     
             return redirect(route('admin.banner.index'));
         } catch (Exception $e) {
             Session::flash('error', 'False'); 
         }
         
      }         

      return view('admin.module.banner.edit')->with($data);
   }

   public function destroy($id)
   {
      $Banner = Banner::where("ban_id","=",$id);

      if ($Banner)
      {
         $Banner->delete();
         Session::flash('success', 'Xóa banner thành công!');
      }

      return redirect('/admin/banner/');

   }

   public function upload(Request $request){
      $dataReturn     = ['status' => false, 'msg' => '', 'filename' => '', 'url' => '', 'filesize' => 0, 'width' => 0, 'height' => 0]; 

      if ($request->hasFile('Filedata')) {
         $image                  = $request->file('Filedata');

         $validator = Validator::make($request->all(), [
             'Filedata'   => 'mimes:jpeg,jpg,png,gif'
         ],[
             'Filedata.mimes'      => 'Ảnh tải lên không đúng định dạng'
         ]);

         if ($validator->fails()) {
             $dataReturn['msg'] = 'File upload không tồn tại!';
             // return redirect('/admin/staff/'.$id.'/edit')->withErrors($validator)->withInput();
         }
         //upload images
         $dataReturn['filename'] = env("CDN_IMAGE") . 'banners/'.ImageManager::upload($image, 'banners');
         list($width, $height)   = getimagesize($image->getRealPath());
         $dataReturn['width']    = $width;
         $dataReturn['height']   = $height;
         $dataReturn['status']   = true;
         $dataReturn['filesize'] = $image->getClientSize();
         $dataReturn['url'] = $dataReturn['filename'];
      }else{
          $dataReturn['msg'] = 'File upload không tồn tại!';
      }

      return json_encode($dataReturn);
    }

   public function getPosition(){
     $data = BannerPosition::where('banp_active', 1)->get()->toArray();
     $return = [];

     if ($data) {
        foreach ($data as $key => $value) {
           $return[$value['banp_position']] = $value['banp_name'];
        }
     }  
     return $return;
   }

   public function changeOrder(Request $request){
     $id     = $request->get('id');
     $value  = $request->get('value', 0);
     $return = ['status'=> 0, 'msg' => ''];

     if($id){
         try {
             $update = Banner::where('ban_id', $id)->update([
                 'ban_order' => intval($value),
             ]);
             $return['msg'] = 'Update Thành Công';
         } catch (Exception $e) {
             $return['msg'] = 'Update Thất bại';    
             $return['status'] = 1;
         }            
     }
     return $return;
   }
   public function changestatus(Request $request)
    {
      $id     = intval($request->get('id'));
      $status = intval($request->get('status'));
      $news = Banner::where("ban_id","=",$id);

      if ($news)
      {
         $news->update(['ban_active' => $status] ); 
      }
      $return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
      return $return;
    }
   public function getType()
   {
     $types = [
         1 => 'Banner ảnh',
         2 => 'Banner flash'
     ];

     return $types;
   } 
   public function getLang()
   {
     $lang = [
         1 => 'Tiếng việt',
         2 => 'English'
     ];

     return $lang;
   }
}    

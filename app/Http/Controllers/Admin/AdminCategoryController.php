<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryMulti;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Validator;
use App\Helper\ImageManager;

class AdminCategoryController extends Controller
{

    protected $category_multi = "";

    public function __construct(CategoryMulti $category_multi)
    {
        $this->category_multi = $category_multi;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = [];
        $where = array();

        if ($request->input('cat_name'))
        {
            $where['cat_name'] = $request->input('cat_name');
            $params['cat_name'] = $request->input('cat_name');
        }

        $query = CategoryMulti::getAllCategory($where);

        $data['category']        = $query;
        $data['arrType']         = CategoryMulti::$arrType;
        $data['params']          = $params;
        $data['page_filter_url'] = '/admin/category';
        return view("admin.module.category.list", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd(app()->ADMIN_INFO->adm_id);
        // dd(config('cache.default'));
        
        $data['arrType']         = CategoryMulti::$arrType;
        if ($request->input('cat_type')){
            $where['cat_type'] = $request->input('cat_type');
            $params['cat_type'] = $request->input('cat_type');
        }
        // $query = CategoryMulti::getAllCategory($where);

        return view("admin.module.category.add", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrData   = array();
        $validator = Validator::make($request->all(), [
            'cat_type'    => 'required|not_in:0|numeric',
            'cat_name'    => 'max:255|required',
            'cat_picture' => 'mimes:jpeg,jpg,png,gif'
        ],[
            'cat_type.required' => 'Bạn chưa chọn loại danh mục.',
            'cat_type.not_in'   => 'Bạn chưa chọn loại danh mục.',
            'cat_name.max'      => 'Tên danh mục không quá 255 kí tự',
            'cat_name.required' => 'Bạn chưa nhập tên danh mục',
            'cat_picture.mimes' => 'Ảnh tải lên không đúng định dạng'
        ]);

        if ($validator->fails()) {
            return redirect('/admin/category/create')->withErrors($validator)->withInput();
        }

        $arrData['cat_type']             = intval($request->cat_type);
        $arrData['cat_parent_id']        = intval($request->cat_parent_id);
        $arrData['cat_name']             = replaceMQ($request->cat_name);
        $arrData['cat_name_rewrite']     = removeTitle($request->cat_name);
        $arrData['cat_meta_keyword']     = replaceMQ($request->cat_meta_keyword);
        $arrData['cat_description']      = replaceMQ($request->cat_description);
        $arrData['cat_meta_description'] = $arrData['cat_description'];
        $arrData['cat_meta_title']       = $arrData['cat_name'];
        $arrData['cat_order']            = intval($request->cat_order);
        $arrData['cat_active']           = $request->cat_active;
        $arrData['cat_create_time']      = time();
        $arrData['cat_update_at']        = time();
        $arrData['admin_id']             = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;

        // lưu thông tin
        if ($adminuser = AdminUser::create($arrData)) {
            if ($request->hasFile('cat_picture')) {
                $image = $request->file('cat_picture');
                //upload images
                $fileName = ImageManager::upload($image, 'category');
                //insert to database
                $adminuser->update(['cat_picture' => $fileName]);
            }
            Session::flash('success', 'Thêm mới user thành công!');
        } else {
            Session::flash('danger', 'Lỗi: vui lòng thao tác lại');
        }
        return redirect('/admin/category/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminCategory  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function show(AdminUser $adminUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = CategoryMulti::find($id);
        return view('admin.module.category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $arrData   = array();
        $AdminUser = AdminUser::findOrFail($id);
        if($AdminUser){
            $validator = Validator::make($request->all(), [
                'adm_avatar'   => 'mimes:jpeg,jpg,png,gif'
            ],[
                'adm_avatar.mimes'      => 'Ảnh tải lên không đúng định dạng'
            ]);

            if ($validator->fails()) {
                return redirect('/admin/category/'.$id.'/edit')->withErrors($validator)->withInput();
            }

            $arrData['adm_email']       = $request->adm_email;
            $arrData['adm_phone']       = $request->adm_phone;
            $arrData['adm_department']  = $request->adm_department;
            $arrData['adm_active']      = $request->adm_active;
            $arrData['adm_udate_time']  = time();
            $arrData['adm_admin_id']    = isset(app()->ADMIN_INFO->adm_id) ? app()->ADMIN_INFO->adm_id : 0;

            $AdminUser->update($arrData);

            // Update thông tin ảnh
            if ($request->hasFile('adm_avatar')) {
                $image = $request->file('adm_avatar');
                //upload images
                $fileName = ImageManager::upload($image, 'user_admin');
                //insert to database
                $AdminUser->update(['adm_avatar' => $fileName]);
            }
            Session::flash('success', 'Sửa thông tin user thành công!');
            
        }else{
            return redirect()->route('admin.user')->with('error','User không tồn tại!');
        }
        return redirect('/admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminUser  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $CategoryMulti = CategoryMulti::findOrFail($id);

        if ($CategoryMulti)
        {
            $CategoryMulti->delete();

            Session::flash('success', 'Xóa thành công!');
        }

        return redirect('/admin/category/');
    }

    function loadParent(Request $request){
        $html = '<select class="form-control" name="cat_parent_id">';
        $html .= '<option value="0">-- Chọn --</option>';
        if($request->typeID){
            $query = CategoryMulti::getAllCategory(['cat_type' => $request->typeID],false);
            foreach ($query as $key => $value) {
                $text = "";
                for ($i=0; $i < $value->level; $i++) { 
                    $text .= "---";
                }
                $html .= '<option value="' .$value->cat_id. '">'.$text.$value->cat_name.'</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }
}

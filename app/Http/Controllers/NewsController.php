<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use Cookie;
use DB;

class NewsController extends FrontEndController
{   
    protected $NewsRepository;
    public function __construct(NewsRepository $NewsRepository){
        $this->NewsRepository = $NewsRepository;
    }

    public function index(Request $request){    

        $page = $request->get('page', 1);

        $listNews = $this->NewsRepository->getNews([
            'page'    => $page -1,
            'perPage' => 20,
        ],['images']);

        $hotNews = $listNews[0];
        unset($listNews[0]);

        //pagination
        $pagination = $this->NewsRepository->paging([
            'page'    => $page -1,
            'perPage' => 20,
        ]);

        $data = [
            'hotNews' => $hotNews,
            'listNews' => $listNews,
            'pagination' => $pagination,
        ];
        return view('news')->with($data);
    }

    public function detail(Request $request, $id, $alias = ''){ 
        $result = $this->NewsRepository->getNewsDetail($id);

        if(!$result){
            return redirect(route('home'));
        }

        $data = [
            'News' => $result['NewsInfo'],
            'otherNews' => $result['otherNews'],
            'prevNews'  => $result['otherNews'][0] ?? [],
            'nextNews'  => end($result['otherNews']) ?? [],
        ];

        return view('newsDetail')->with($data);
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cookie;
use DB;
use App\Repositories\CampaignRepository;

class CampaignController extends FrontEndController
{
    protected $CampaignRepository;
    public function __construct(CampaignRepository $CampaignRepository){
        $this->CampaignRepository = $CampaignRepository;
    }

    public function index(Request $request){ 
        $page = $request->get('page', 1);
        $listCampaign = $this->CampaignRepository->getCampaigns([
            'page' => $page -1 ,
            'perPage' => 30
        ]);

        $pagination = [];
        $pagination = $this->CampaignRepository->paging([
            'page' => $page -1 ,
            'perPage' => 30
        ]);

        $data = [
            'listCampaign' =>$listCampaign,
            'pagination'   => $pagination,
        ];
        return view('chiendich')->with($data);
    }

    public function detail(Request $request, $id, $alias = ''){ 

        $result = $this->CampaignRepository->getCampaignDetail($id);

        $data = [
            'Campaign'      => $result['CampaignInfo'],
            'otherCampaign' => $result['otherCampaign'],
            'prevCampaign'  => $result['otherCampaign'][0] ?? [],
            'nextCampaign'  => end($result['otherCampaign']) ?? [],
        ];

        return view('chiendich_chitiet')->with($data);
    }

}

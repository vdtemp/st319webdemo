<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\EventRepository;
use App\Repositories\BannerRepository;
use Cookie;
use DB;

class EventController extends FrontEndController
{   
    protected $EventRepository;
    public function __construct(EventRepository $EventRepository){
        $this->EventRepository = $EventRepository;
    }

    public function index(Request $request){ 
        $BannerRepository = new BannerRepository;
        $banners = $BannerRepository->getBanners(['pos'=>'event_page_banner', 'limit' => 1]);
        
        $page = $request->get('page', 1);

        $listEvent = $this->EventRepository->getEvents([
            'page' => $page - 1,
            'perPage' => 20,
        ]);
        
        //pagination
        $pagination = $this->EventRepository->paging([
            'page'    => $page -1,
            'perPage' => 20,
        ]);
        
        $hotEvent = $listEvent[0];
        unset($listEvent[0]);

        $data = [
            'hotEvent' => $hotEvent,
            'listEvent' => $listEvent,
            'pagination' => $pagination,
            'banners' => $banners,
        ];
        return view('event')->with($data);
    }

}

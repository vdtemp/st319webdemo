<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BannerRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ArtistRepository;
use App\Models\Product; 
use App\Models\UserOrder;
use App\Models\UserOrderDetail;
use Cookie;
use Session;
use DB;

class StoreController extends FrontEndController
{
    protected $ProductRepository ;
    public function __construct(ProductRepository $ProductRepository, ArtistRepository $ArtistRepository){
        $this->ProductRepository = $ProductRepository;
        $this->ArtistRepository = $ArtistRepository;
    }

    public function index(Request $request){ 
        /*Banner*/
        $BannerRepository = new BannerRepository;
        $banners = $BannerRepository->getBanners(['pos'=>'banner_store_page']);

        /*Product*/
        $products = $this->ProductRepository->getProducts([
            'sale' => 1,
        ]);
        $listproduct = [];
        if($products){
            foreach ($products as $pro) {
                $listproduct[$pro['pro_artist_id']][] = $pro;
            }
        }

        // Ds Nghệ sĩ
        $arrArtist = $this->ArtistRepository->getArtists([
            'job' => 1,       
        ]);        
        $listArtist = [];
        if($arrArtist){
            foreach ($arrArtist as $key => $value) {
                $listArtist[$value['art_id']] = $value['art_name'];
            }
        }

        $data = [
            'listproduct' => $listproduct,
            'banners' => $banners,
            'listArtist' => $listArtist,
        ];
        
        return view('store')->with($data);
    }
    
    public function product(Request $request, $id){
        if(intval($id) <= 0){
            return redirect('store')->with("error", "Thông tin không chính xác");
        } 

        $productInfo =  $this->ProductRepository->productDetail($id);

        // Other product
        $otherPro = $this->ProductRepository->getProducts([
            'sale' => 1,
        ]);

        $data = [
            'productInfo' => $productInfo,
            'otherPro' => $otherPro,

        ];

        return view('storeProduct')->with($data);
    }

    public function addToCart(Request $request){
        $count = $request->get('count', 1);
        $proId = $request->get('proId', 0);

        if($proId <= 0){
            return redirect(route('store'));
        }

        $order = json_decode(Cookie::get('stOrder'), 1);

        if(isset($order[$proId])){
            $order[$proId]++;
        }else{
            $order[$proId] = 1;
        }

        Cookie::queue('stOrder', json_encode($order), 30);
        return redirect('order');
    }

    public function order(Request $request){
        $order = json_decode(Cookie::get('stOrder'), 1);
        if(!$order){
            return redirect(route('store'));
        }
        $arrID = array_keys($order);
        $products  = Product::where('pro_status', 1)
                    ->whereIn('pro_id', $arrID)
                    ->select(['pro_id', 'pro_name', 'pro_image', 'pro_price'])
                    ->get()->toArray();
        $data = [
            'order' => $order,
            'products' => $products
        ];

        return view('storeOrder')->with($data);
    }

    public function submitOrder(Request $request){
        $order = json_decode(Cookie::get('stOrder'), 1);

        $pro_id = $request->get('pro_id', []);
        $numb   = $request->get('numb', []);

        $proInfo = Product::whereIn('pro_id', $pro_id)->select(['pro_id', 'pro_name', 'pro_price'])->get()->toArray();
        $arrPrice = [];
        foreach ($proInfo as $key => $value) {
            $arrPrice[$value['pro_id']] = $value;
        }

        $name    = $request->get('name', '');
        $email   = $request->get('email', '');
        $phone   = $request->get('phone', '');
        $address = $request->get('address', '');
        $note    = $request->get('note', '');

        $total_value  = 0;
        $total_pay = 0;

        $dataInsert = [];   
        $dataInsert['uso_user_name']     = $name;
        $dataInsert['uso_user_email']    = $email;
        $dataInsert['uso_user_phone']    = $phone;
        $dataInsert['uso_user_address']  = $address;
        $dataInsert['uso_customer_note'] = $note;
        $dataInsert['uso_total_value']   = $total_value;
        $dataInsert['uso_total_pay']     = $total_pay;
        $dataInsert['uso_active']        = 1;
        $dataInsert['uso_date']          = time();

        DB::beginTransaction();
        $UserOrder = UserOrder::create($dataInsert);
        if($UserOrder->uso_id > 0){
            $OrderDetail = [];
            foreach ($pro_id as $key => $id) {
                $OrderDetail[] = [
                    "uod_order_id"    => $UserOrder->uso_id,
                    "uod_product_id"  => $id,
                    "uod_quantity"    => $numb[$key] ?? 1,
                    "uod_price"       => $arrPrice[$id]['pro_price'],
                    "uod_total_money" => $arrPrice[$id]['pro_price'] * $numb[$key], 
                    "uod_last_update" => time(),
                    "uod_status"      => 1,
                ];

                $total_value += $arrPrice[$id]['pro_price'] * $numb[$key];
            }
            UserOrderDetail::insert($OrderDetail);
            
            /*Update giá*/
            UserOrder::where('uso_id','=',$UserOrder->uso_id)->update([
                'uso_total_value' => $total_value,
            ]);

            DB::commit();
            Session::flash('success','Thêm mới thành công!');

            Cookie::queue(Cookie::forget('stOrder'));
            return view('storeOrderSuccess');
        }else{
            DB::rollBack();
            Session::flash('error','đặt hàng không thành công!');
            return back()->withInput();
        }
    }
}

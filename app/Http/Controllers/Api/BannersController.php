<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banners;
use Session;
use Validator;
use App\Helper\ImageManager;
use DB;

class BannersController extends Controller
{
	public function __construct(){

	}

	public function index(Request $request)
	{			
		$bannerType       = config('banner.type');
		$bannerTargetType = config('banner.targetType'); 

		$pos      = $request->get('pos', '');
		$site     = $request->get('site', '');
		$platform = $request->header('platform');

		$return 	= ['status' => 0, 'msg' => '', 'data' => ''];
		$Banners = Banners::where("BAN_ACTIVE", 1);

		if($pos != ''){
			$Banners = $Banners->where("BAN_POSITION", $pos);
		}
		if($site != ''){
			$Banners = $Banners->where("BAN_WEBSITE", $site);
		}

		if($platform == 'ios' || $platform == 'android'){
			$Banners = $Banners->where("BAN_DEVICE", 'mob');
		}else{
			$Banners = $Banners->where("BAN_DEVICE", 'pc');
		}

		$Banners = $Banners->get();

		$info = [];
		foreach ($Banners as $key => $banner) {
			$info[$key]['id']          = $banner->ban_id;
			$info[$key]['title']       = $banner->ban_title;
			$info[$key]['description'] = $banner->ban_description;
			$info[$key]['type']        = ($banner->ban_type == 1) ? 'image' : 'video';
			$info[$key]['url']         = $banner->ban_url_data;
			$info[$key]['url_thump']   = ($banner->ban_type == 1) ? $banner->ban_url_data : $banner->ban_url_data_thump;
			$info[$key]['target_type'] = $banner->ban_target_type;
			$info[$key]['target_id']   = $banner->ban_target_id;
			$info[$key]['target_url']  = $banner->ban_target_url;
			$info[$key]['position']    = $banner->ban_position;
		}

		$return['data'] = $info;
		return $return;
	}

	public function create(Request $request)
	{
		$admin_id    = 1;
		$return      = ['status' => 0, 'msg' => '', 'data' => []];
		 
    	return $return; 
	}

	public function destroy($id, Request $request)
	{		
 		$return = ['msg' => 'Gỡ bài thành công', 'status' => 0, 'data' => []];
		return $return;
	}

	public function detail($id)
	{ 
		$return =  ['msg' => '', 'status' => 0, 'data' => []];

		 
 		return $return;
	}
	
    public function changestatus(Request $request)
    {
    	$id     = intval($request->get('id'));
		$status = intval($request->get('status'));
		$news = Banners::where("ban_id","=",$id);

		if ($news)
		{
			$news->update(['ban_active' => $status] ); 
		}
		$return = ['msg' => 'Update thành công', 'status' => 0, 'data' => []];
		return $return;
    }
}
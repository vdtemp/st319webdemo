<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cookie;
use DB;
use App\Repositories\ArtistRepository;

class ArtistController extends FrontEndController
{   
    protected $ArtistRepository;
    public function __construct(ArtistRepository $ArtistRepository){
        $this->ArtistRepository = $ArtistRepository;
    }
    public function getType()
    {
     $type = [
         1 => 'Album',
         2 => 'Singles & EPs ',
         3 => 'Fimography'
     ];

     return $type;
    }

    public function index(Request $request){ 
        $listArtist = $this->ArtistRepository->getArtists();
            
        $data = [
            'listArtist' => $listArtist,
        ];
        return view('artist')->with($data);
    }
    
    public function detail(Request $request, $id, $alias = ''){

        $artistInfo = $this->ArtistRepository->getArtistDetail($id);
        $arrType = $this->gettype();

        $images     = $artistInfo['images'] ?? [];
        $products   = $artistInfo['products'] ?? [];
        $arrInfoProduct = [];
        foreach ($products as $value) {
            $arrInfoProduct[$value['pro_type']][] = $value;
        }

        $data = [
            'artistInfo' => $artistInfo, 
            'arrType' => $arrType,
            'arrInfoProduct' => $arrInfoProduct, 
            'arrImages' => $images,
        ];
        
        return view('artistDetail')->with($data);
    }
}

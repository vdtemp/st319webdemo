<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer(['admin.header', 'admin.sidebar'], 'App\Http\View\AdminInfoComposer');
        View::composer('*', 'App\Http\View\MenuComposer');
        // View::composer('layout.header', 'App\Http\View\UserInfoComposer');
    }
}
